import React, { useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import {
    CCreateElement,
    CSidebar,
    CSidebarBrand,
    CSidebarNav,
    CSidebarNavDivider,
    CSidebarNavTitle,
    CSidebarMinimizer,
    CSidebarNavDropdown,
    CSidebarNavItem
} from '@coreui/react';

import { IntlProvider, FormattedMessage } from 'react-intl';

import CIcon from '@coreui/icons-react';

// sidebar nav config

import { navigation } from './_nav';

import { setSidebarShow } from '../store/actions/navActions';

const TheSidebar = () => {
    const dispatch = useDispatch();
    const show = useSelector(state => state.navigation.sidebarShow);

    const localizations = useSelector(state => state.locals.localizations);
    const chosenLocal = useSelector(state => state.locals.chosenLocal);
    const menuItems = useSelector(state => state.navigation.menuItems);

    console.log('menuItems', menuItems);

    // const [sidebarShow, setSidebarShow] = useState('responsive');
    console.log('side', localizations[chosenLocal.value] && localizations[chosenLocal.value].partners);

    return (
        <CSidebar
            show={show}
            onShowChange={(val) => { dispatch(setSidebarShow(val)); console.log('onShowChange', val); }}
        >
            <IntlProvider locale={chosenLocal.value} messages={localizations[chosenLocal.value]}>

                <CSidebarBrand style={{ textDecoration: 'none', paddingTop: '6px' }} className="d-md-down-none" to="/">
                    <h3 className="c-sidebar-brand-full">SMS-PORT.PRO</h3>
                    <h3 className="c-sidebar-brand-minimized">S</h3>

                    {/* <CIcon
                        className="c-sidebar-brand-full"
                        name="logo-negative"
                        height={35}
                    />
                    <CIcon
                        className="c-sidebar-brand-minimized"
                        name="sygnet"
                        height={35}
                    /> */}
                </CSidebarBrand>
                <CSidebarNav>

                    <CCreateElement
                        items={navigation(localizations[chosenLocal.value], menuItems)}
                        components={{
                            CSidebarNavDivider,
                            CSidebarNavDropdown,
                            CSidebarNavItem,
                            CSidebarNavTitle
                        }}
                    />
                </CSidebarNav>
                <CSidebarMinimizer className="c-d-md-down-none"/>
            </IntlProvider>
        </CSidebar>
    );
};

export default React.memo(TheSidebar);
