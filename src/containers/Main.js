import React, { Suspense, useState, useEffect, useRef } from 'react';
import {
    Redirect,
    Route,
    Switch
} from 'react-router-dom';
import { useHistory } from 'react-router';
import { useSelector, useDispatch } from 'react-redux';
// import { cilPencil, cilSettings } from '@coreui/icons';
import {
    CHeader,
    CToggler,
    CHeaderBrand,
    CHeaderNav,
    CHeaderNavItem,
    CHeaderNavLink,
    CSubheader,
    CBreadcrumbRouter,
    CLink,
    CContainer,
    CFade,
    CButton,
    CRow,
    CCol,
    CModal,
    CModalBody,
    CModalFooter,
    CModalHeader,
    CModalTitle,

    CCard,
    CCardBody,
    CCardFooter,
    CCardHeader,
    CCollapse,
    CDropdownItem,
    CDropdownMenu,
    CDropdownToggle,
    CForm,
    CFormGroup,
    CFormText,
    CValidFeedback,
    CInvalidFeedback,
    CTextarea,
    CInput,
    CInputFile,
    CInputCheckbox,
    CInputRadio,
    CInputGroup,
    CInputGroupAppend,
    CInputGroupPrepend,
    CDropdown,
    CInputGroupText,
    CLabel,
    CSelect
} from '@coreui/react';

import CIcon from '@coreui/icons-react';

import { IntlProvider, FormattedMessage } from 'react-intl';

// routes config

import {
    TheHeaderDropdown,
    TheHeaderDropdownMssg,
    TheHeaderDropdownNotif,
    TheHeaderDropdownTasks
} from './index';

import { register, resetRegistration, logout } from '../store/actions/authActions';
import { changeLocalization } from '../store/actions/localsActions';

const loading = (
    <div className="pt-3 text-center">
        <div className="sk-spinner sk-spinner-pulse"></div>
    </div>
);

const TheHeaderUnregistered = () => {
    // const [modal, setModal] = useState(true);
    // const [large, setLarge] = useState(false);
    // const [small, setSmall] = useState(false);
    // const [primary, setPrimary] = useState(false);
    const [success, setSuccess] = useState(false);
    // const [warning, setWarning] = useState(false);
    // const [danger, setDanger] = useState(false);
    // const [info, setInfo] = useState(false);
    const [registrationData, setRegistrationData] = useState({
        name: '',
        email: '',
        communication_type: '',
        communication: '',
        message: ''
    });

    const dispatch = useDispatch();

    const show = useSelector(state => state.navigation.sidebarShow);
    const registerSuccess = useSelector(state => state.authorization.success);
    const errorName = useSelector(state => state.authorization.errorName);
    const errorEmail = useSelector(state => state.authorization.errorEmail);
    const errorCommunicationType = useSelector(state => state.authorization.errorCommunicationType);
    const authorized = useSelector(state => state.authorization.authorized);

    const localizations = useSelector(state => state.locals.localizations);
    const chosenLocal = useSelector(state => state.locals.chosenLocal);

    useEffect(() => {
        // code to run on component mount
        console.log('authorized', authorized);
    }, [show, registerSuccess, errorName, errorEmail, errorCommunicationType, authorized, chosenLocal, localizations]);

    useEffect(() => {
        dispatch(changeLocalization(chosenLocal));
    }, []);

    const onRegister = (event) => {
        event.preventDefault();
        dispatch(register(registrationData));
    };

    if (registerSuccess) {
        console.log('registerSuccess', registerSuccess);
        setRegistrationData({
            name: '',
            email: '',
            communication_type: '',
            communication: '',
            message: ''
        });
        dispatch(resetRegistration());
        setSuccess(!success);
    }

    const onCancel = () => {
        setRegistrationData({
            name: '',
            email: '',
            communication_type: '',
            communication: '',
            message: ''
        });
        dispatch(resetRegistration());
        setSuccess(false);
    };

    const handleInputChange = ({ target: { name, value } }) => {
        setRegistrationData({ ...registrationData, [name]: value });
    };

    const onLogout = (data, e) => {
        console.log('onLogout');
        dispatch(logout());
        // e.target.reset();
        // history.push({
        //     pathname: '/'
        //     // state: {
        //     //     response: messageFromServer
        //     // }
        // });
    };

    // Websocket
    // const [isPaused, setPause] = useState(false);
    // const ws = useRef(null);

    // useEffect(() => {
    //     ws.current = new WebSocket('wss://78.110.62.195/api/service');
    //     ws.current.onopen = () => console.log('ws opened');
    //     ws.current.onclose = () => console.log('ws closed');

    //     return () => {
    //         ws.current.close();
    //     };
    // }, []);

    // useEffect(() => {
    //     if (!ws.current) return;

    //     ws.current.onmessage = e => {
    //         if (isPaused) return;
    //         const message = JSON.parse(e.data);
    //         console.log('e', message);
    //     };
    // }, [isPaused]);

    // const capitalizeStyle = {
    //     button: base => ({
    //         ...base,
    //         textTransform: 'uppercase'
    //     })

    // };

    return (
        <IntlProvider locale={chosenLocal.value} messages={localizations[chosenLocal.value]}>

            <CHeader style={{ backgroundColor: '#64af59' }} withSubheader>

                <CHeaderNav className="mr-auto">
                    <CHeaderNavItem style={{ padding: '16px', backgroundColor: '#fff', left: '20%' }} className="px-3" >
                        <CHeaderNavLink style={{ color: '#414042', fontSize: '3.1rem', fontWeight: 1000 }} to="/toCRM">SMS-PORT.PRO</CHeaderNavLink>
                    </CHeaderNavItem>
                    <CHeaderNavItem style={{ left: '20%' }} className="px-3 uppercase">
                        {/* {authorized
                            ? <CHeaderNavLink style={{ color: '#fff', fontWeight: 'bold', fontSize: '1.3rem' }} onClick={() => onLogout()}><FormattedMessage id="common.logout" /></CHeaderNavLink>
                            : <CHeaderNavLink style={{ color: '#fff', fontWeight: 'bold', fontSize: '1.3rem' }} to="/login"><FormattedMessage id="common.login" /></CHeaderNavLink>
                        } */}
                        {authorized
                            ? <CHeaderNavLink style={{ color: '#fff', fontWeight: 'bold', fontSize: '1.3rem' }} onClick={() => onLogout()}>Личный кабинет</CHeaderNavLink>
                            : <CHeaderNavLink style={{ color: '#fff', fontWeight: 'bold', fontSize: '1.3rem' }} to="/login">Личный кабинет</CHeaderNavLink>
                        }

                    </CHeaderNavItem>

                </CHeaderNav>
            </CHeader>
            <div className="c-body">
                <main className="c-main">
                    <div className="container">
                        <div className="row">
                            <div style={{ top: '200px' }} className="col">
                                {/* <CButton to='/login' block style={{ color: '#fff', fontSize: '3.1rem', backgroundColor: 'red' }}>Login</CButton> */}
                                <CButton style={{ color: '#fff', fontSize: '2.1rem' }} color="danger" block onClick={() => setSuccess(!success)} className="mr-1 uppercase"><FormattedMessage className="uppercase" id="common.register" /></CButton>

                                <CModal
                                    show={success}
                                    onClose={() => setSuccess(!success)}
                                    color="success"
                                >
                                    <CCard>
                                        <CCardHeader>
                                            Регистрация
                                        </CCardHeader>
                                        <CCardBody>
                                            <CForm onSubmit={onRegister} method="post">
                                                <CFormGroup row>
                                                    <CInputGroup>
                                                        <CCol xs="12" md="12">
                                                            {/* <CInputGroupPrepend>
                                                                <CInputGroupText><CIcon name="cil-user" /></CInputGroupText>
                                                            </CInputGroupPrepend> */}
                                                            {errorName === ''
                                                                ? <>
                                                                    <CInput
                                                                        onChange={handleInputChange}
                                                                        value={registrationData.name}
                                                                        id="name"
                                                                        name="name"
                                                                        placeholder="Username"
                                                                        autoComplete="name"
                                                                        required
                                                                    />
                                                                    <CFormText className="help-block">Please enter your name</CFormText>
                                                                </>
                                                                : errorName
                                                                    ? <>
                                                                        <CInput
                                                                            invalid
                                                                            onChange={handleInputChange}
                                                                            value={registrationData.name}
                                                                            id="name"
                                                                            name="name"
                                                                            placeholder="Username"
                                                                            autoComplete="name"
                                                                            required
                                                                        />
                                                                        <CInvalidFeedback className="help-block">{errorName.error}</CInvalidFeedback>
                                                                    </>
                                                                    : <>
                                                                        <CInput
                                                                            valid
                                                                            onChange={handleInputChange}
                                                                            value={registrationData.name}
                                                                            id="name"
                                                                            name="name"
                                                                            placeholder="Username"
                                                                            autoComplete="name"
                                                                            required
                                                                        />
                                                                        <CValidFeedback className="help-block">Ok!</CValidFeedback>
                                                                    </>
                                                            }
                                                        </CCol>
                                                    </CInputGroup>
                                                </CFormGroup>
                                                <CFormGroup row>
                                                    <CInputGroup>
                                                        <CCol xs="12" md="12">
                                                            {/* <CInputGroupPrepend>
                                                                <CInputGroupText><CIcon name="cil-envelope-closed" /></CInputGroupText>
                                                            </CInputGroupPrepend> */}
                                                            {errorEmail === ''
                                                                ? <>
                                                                    <CInput
                                                                        onChange={handleInputChange}
                                                                        value={registrationData.email}
                                                                        type="email" id="email" name="email" placeholder="Email" autoComplete="username"
                                                                        required
                                                                    />
                                                                    <CFormText className="help-block">Please enter your email</CFormText>
                                                                </>
                                                                : errorEmail
                                                                    ? <>
                                                                        <CInput
                                                                            invalid
                                                                            onChange={handleInputChange}
                                                                            value={registrationData.email}
                                                                            type="email" id="email" name="email" placeholder="Email" autoComplete="username"
                                                                            required
                                                                        />
                                                                        <CInvalidFeedback>{errorEmail.error}</CInvalidFeedback>
                                                                    </>
                                                                    : <>
                                                                        <CInput
                                                                            valid
                                                                            onChange={handleInputChange}
                                                                            value={registrationData.email}
                                                                            type="email" id="email" name="email" placeholder="Email" autoComplete="username"
                                                                            required
                                                                        />
                                                                        <CValidFeedback className="help-block">Ok!</CValidFeedback>
                                                                    </>
                                                            }
                                                        </CCol>
                                                    </CInputGroup>
                                                </CFormGroup>
                                                <CFormGroup row>
                                                    <CCol xs="12" md="12">
                                                        {errorCommunicationType === ''
                                                            ? <>
                                                                <CInput onChange={handleInputChange} value={registrationData.communication_type} id="communication_type" name="communication_type" placeholder="Telegram или Whatsapp"/>
                                                                <CFormText className="help-block">Please enter your communication type</CFormText>
                                                            </>
                                                            : errorCommunicationType
                                                                ? <>
                                                                    <CInput invalid onChange={handleInputChange} value={registrationData.communication_type} id="communication_type" name="communication_type" placeholder="Telegram или Whatsapp"/>
                                                                    <CInvalidFeedback className="help-block">{errorCommunicationType.error}</CInvalidFeedback>
                                                                </>
                                                                : <>
                                                                    <CInput onChange={handleInputChange} value={registrationData.communication_type} id="communication_type" name="communication_type" placeholder="Telegram или Whatsapp"/>
                                                                    <CFormText className="help-block">Please enter your communication type</CFormText>
                                                                </>
                                                        }
                                                    </CCol>
                                                </CFormGroup>
                                                <CFormGroup row>
                                                    <CInputGroup>
                                                        <CCol xs="12" md="12">

                                                            <CInput onChange={handleInputChange} value={registrationData.communication} type="text" id="communication" name="communication" placeholder="Ссылка..." autoComplete="current-password"/>
                                                            <CFormText className="help-block">Please enter your communication</CFormText>
                                                        </CCol>

                                                    </CInputGroup>
                                                </CFormGroup>
                                                <CFormGroup row>
                                                    <CCol xs="12" md="12">
                                                        <CTextarea
                                                            name="message"
                                                            id="message"
                                                            rows="3"
                                                            placeholder="Сообщения..."
                                                            value={registrationData.message}
                                                            onChange={handleInputChange}
                                                        />
                                                        <CFormText className="help-block">Please enter your message</CFormText>

                                                    </CCol>
                                                </CFormGroup>

                                                <CFormGroup className="form-actions">
                                                    <CButton type="submit" color="success">Sign in</CButton>{' '}
                                                    <CButton color="secondary" onClick={() => onCancel(!success)}>Cancel</CButton>
                                                </CFormGroup>
                                            </CForm>
                                        </CCardBody>
                                    </CCard>

                                    {/* <CModalHeader closeButton>
                                            <CModalTitle>Request</CModalTitle>
                                        </CModalHeader>
                                        <CModalBody>
                                            <span id="reauth-email" className="reauth-email"></span>
                                            <input type="user" id="inputUser" className="form-control" placeholder="User" required autoFocus />
                                            <input type="password" id="inputPassword" className="form-control" placeholder="Password" required />
                                        </CModalBody>
                                        <CModalFooter>
                                            <CButton color="success" onClick={() => setSuccess(!success)}>Sign in</CButton>{' '}
                                            <CButton color="secondary" onClick={() => setSuccess(!success)}>Cancel</CButton>
                                        </CModalFooter> */}

                                    {/* <img id="profile-img" className="profile-img-card" src={logo} />
                                    <p id="profile-name" className="profile-name-card"></p>
                                    <form onSubmit={this.onLogin} className="form-signin">
                                        <span id="reauth-email" className="reauth-email"></span>
                                        <input type="user" id="inputUser" className="form-control" placeholder="User" required autoFocus />
                                        <input type="password" id="inputPassword" className="form-control" placeholder="Password" required />
                                        <div className="control-group">
                                            <label className="control control-checkbox">
                                Remember me!
                                                <input type="checkbox" />
                                                <div className="control_indicator"></div>
                                            </label>
                                        </div>
                                        <button className="btn btn-lg btn-primary btn-block btn-signin" type="submit">Sign in</button>
                                    </form> */}
                                </CModal>
                            </div>
                        </div>
                    </div>

                </main>
            </div>
        </IntlProvider>
    );
};

export default TheHeaderUnregistered;
