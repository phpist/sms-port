import React, { useState, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import Select from 'react-select';

import {
    CHeader,
    CToggler,
    CHeaderBrand,
    CHeaderNav,
    CHeaderNavItem,
    CHeaderNavLink,
    CSubheader,
    CBreadcrumbRouter,
    CLink
} from '@coreui/react';
import CIcon from '@coreui/icons-react';
import { IntlProvider, FormattedMessage } from 'react-intl';

// routes config
import routes from '../routes';

import { setSidebarShow } from '../store/actions/navActions';

import { changeLocalization } from '../store/actions/localsActions';

import { logout } from '../store/actions/authActions';

import {
    TheHeaderDropdown,
    TheHeaderDropdownMssg,
    TheHeaderDropdownNotif,
    TheHeaderDropdownTasks
} from './index';
import { brandSet } from '@coreui/icons';

import { useHistory, Link } from 'react-router-dom';
import makeAnimated from 'react-select/animated';

const animatedComponents = makeAnimated();

const languages = [
    {
        label: 'Русский',
        value: 'ru'
    },
    {
        label: 'English',
        value: 'en'
    }
    // {
    //     label: 'Española',
    //     value: 'es'
    // },
    // {
    //     label: 'Française',
    //     value: 'fr'
    // },
    // {
    //     label: 'Deutsche',
    //     value: 'de'
    // }
];

const TheHeader = () => {
    const history = useHistory();

    const dispatch = useDispatch();
    const sidebarShow = useSelector(state => state.navigation.sidebarShow);
    const localizations = useSelector(state => state.locals.localizations);
    const chosenLocal = useSelector(state => state.locals.chosenLocal);
    const menuItems = useSelector(state => state.navigation.menuItems);

    useEffect(() => {
    }, [chosenLocal, localizations]);

    useEffect(() => {
        dispatch(changeLocalization(chosenLocal));
    }, []);

    const toggleSidebar = () => {
        const val = [true, 'responsive'].includes(sidebarShow) ? false : 'responsive';
        dispatch(setSidebarShow(val));
    };

    const toggleSidebarMobile = () => {
        const val = [false, 'responsive'].includes(sidebarShow) ? true : 'responsive';
        dispatch(setSidebarShow(val));
    };

    const onLogout = (data, e) => {
        console.log('onLogout');
        dispatch(logout());
        history.push('/');
    };

    console.log('chosenLocal', chosenLocal.label);

    const handleSelect = value => {
        console.log('lang', value);
        dispatch(changeLocalization(value));
    };

    console.log(':localizations', localizations, chosenLocal);

    // const dot = (color = '#ccc') => ({
    //     alignItems: 'center',
    //     display: 'flex',

    //     ':before': {
    //         backgroundColor: color,
    //         borderRadius: 10,
    //         content: '" "',
    //         display: 'block',
    //         marginRight: 8,
    //         height: 10,
    //         width: 10
    //     }
    // });

    const colourStyles = {
        // option: (styles, { data, isDisabled, isFocused, isSelected }) => {
        //     const color = chroma(data.color);
        //     return {
        //         ...styles,
        //         minWidth: '147px',
        //         width: '100%',
        //         backgroundColor: isDisabled
        //             ? null
        //             : isSelected
        //                 ? data.color
        //                 : isFocused
        //                     ? color.alpha(0.1).css()
        //                     : null,
        //         color: isDisabled
        //             ? '#ccc'
        //             : isSelected
        //                 ? chroma.contrast(color, 'white') > 2
        //                     ? 'white'
        //                     : 'black'
        //                 : data.color,
        //         cursor: isDisabled ? 'not-allowed' : 'default',

        //         ':active': {
        //             ...styles[':active'],
        //             backgroundColor: !isDisabled && (isSelected ? data.color : color.alpha(0.3).css())
        //         }
        //     };
        // },
        // input: styles => ({ ...styles, ...dot() }),
        // placeholder: styles => ({ ...styles, ...dot() }),
        // singleValue: (styles, { data }) => ({ ...styles, ...dot(data.color) })
    };

    const style = {
        control: base => ({
            ...base,
            // This line disable the blue border
            boxShadow: 'none',
            border: 0,
            borderRadius: 0,
            borderBottom: '1px solid #c5c4c4'
        })
    };

    const capitalizeStyle = {
        '&::first-letter': base => ({
            ...base,
            textRransform: 'capitalize'
        })

    };

    return (
        <CHeader withSubheader>
            <IntlProvider locale={chosenLocal.value} messages={localizations[chosenLocal.value]}>
                <CToggler
                    inHeader
                    className="ml-md-3 d-lg-none"
                    onClick={toggleSidebarMobile}
                />
                <CToggler
                    inHeader
                    className="ml-3 d-md-down-none"
                    onClick={toggleSidebar}
                />
                <CHeaderBrand style={{ textDecoration: 'none', paddingTop: '6px' }} className="mx-auto d-lg-none" to="/">
                    {/* <CIcon name="logo" height="48" alt="Logo"/> */}
                    <h3>SMS-PORT.PRO</h3>
                </CHeaderBrand>

                <CHeaderNav className="d-md-down-none mr-auto">
                    <CHeaderNavItem className="px-3" >
                        <CHeaderNavLink to="/dashboard">Dashboard</CHeaderNavLink>
                    </CHeaderNavItem>
                    {/* <CHeaderNavItem className="px-3" >
                        <CIcon content={brandSet.cifRu} size="2xl" />
                    </CHeaderNavItem> */}
                    {/* <CHeaderNavItem className="px-3" >
                        <FormattedMessage id="greeting" />
                    </CHeaderNavItem> */}
                    {/* <CHeaderNavItem className="px-3">
                    <CHeaderNavLink to="/users">Users</CHeaderNavLink>
                    </CHeaderNavItem>
                    <CHeaderNavItem className="px-3">
                    <CHeaderNavLink>Settings</CHeaderNavLink>
                </CHeaderNavItem> */}
                </CHeaderNav>
                <CHeaderNav className="px-3">
                    <Select
                        id="localsSelect"
                        value={chosenLocal}
                        closeMenuOnSelect={true}
                        components={animatedComponents}
                        options={languages}
                        onChange={handleSelect}
                        name="locals"
                        styles={style}
                    />
                </CHeaderNav>

                <CHeaderNav className="px-3">
                    <CHeaderNavLink style={{ fontSize: '1.1rem' }} onClick={() => onLogout()}>
                        <CHeaderNavItem className="px-3 uppercase" >
                            <FormattedMessage id="common.logout" />
                        </CHeaderNavItem>
                    </CHeaderNavLink>
                    {/* <TheHeaderDropdownNotif/>
                    <TheHeaderDropdownTasks/>
                    <TheHeaderDropdownMssg/>
                    <TheHeaderDropdown/> */}
                </CHeaderNav>

                <CSubheader className="px-3 justify-content-between">
                    <CBreadcrumbRouter
                        className="border-0 c-subheader-nav m-0 px-0 px-md-3"
                        routes={routes(localizations[chosenLocal.value], menuItems)}
                    />
                    {/* <div className="d-md-down-none mfe-2 c-subheader-nav">
                    <CLink className="c-subheader-nav-link"href="#">
                        <CIcon name="cil-speech" alt="Settings" />
                    </CLink>
                    <CLink
                        className="c-subheader-nav-link"
                        aria-current="page"
                        to="/dashboard"
                    >
                        <CIcon name="cil-graph" alt="Dashboard" />&nbsp;Dashboard
                    </CLink>
                    <CLink className="c-subheader-nav-link" href="#">
                        <CIcon name="cil-settings" alt="Settings" />&nbsp;Settings
                    </CLink>
                </div> */}
                </CSubheader>
            </IntlProvider>
        </CHeader>
    );
};

export default TheHeader;
