import React, { Suspense, useEffect } from 'react';
import {
    Redirect,
    Route,
    Switch
} from 'react-router-dom';
import { CContainer, CFade } from '@coreui/react';
import { useSelector, useDispatch } from 'react-redux';
import { IntlProvider, FormattedMessage } from 'react-intl';

// routes config
import routes from '../routes';

const loading = (
    <div className="pt-3 text-center">
        <div className="sk-spinner sk-spinner-pulse"></div>
    </div>
);

const TheContent = () => {
    const localizations = useSelector(state => state.locals.localizations);
    const chosenLocal = useSelector(state => state.locals.chosenLocal);
    const menuItems = useSelector(state => state.navigation.menuItems);

    useEffect(() => {
    }, [menuItems]);

    const defineRoutes = routes(localizations[chosenLocal.value], menuItems);
    console.log('defineRoutes', defineRoutes);
    console.log('defineRoutes[0]', defineRoutes[0]);

    return (
        <IntlProvider locale={chosenLocal.value} messages={localizations[chosenLocal.value]}>
            <main className="c-main">
                <CContainer fluid>
                    <Suspense fallback={loading}>
                        <Switch>
                            {defineRoutes.map((route, idx) => {
                                return route.component && (
                                    <Route
                                        key={idx}
                                        path={route.path}
                                        exact={route.exact}
                                        name={route.name}
                                        render={props => (
                                            <CFade>
                                                <route.component {...props} />
                                            </CFade>
                                        )} />
                                );
                            })}
                            <Redirect from="/" to={defineRoutes[0].path} />
                        </Switch>
                        {/* <Redirect from="/" to={defineRoutes[0].path} /> */}
                    </Suspense>
                </CContainer>
            </main>

        </IntlProvider>

    );
};

export default React.memo(TheContent);
