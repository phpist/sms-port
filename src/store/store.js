import { createStore, combineReducers, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import authReducer from './reducers/authReducer';
import navReducer from './reducers/navReducer';
import iccidReducer from './reducers/iccidReducer';
import localsReducer from './reducers/localsReducer';
import serviceReducer from './reducers/serviceReducer';

const AllReducers = combineReducers({
    authorization: authReducer,
    navigation: navReducer,
    iccid: iccidReducer,
    locals: localsReducer,
    service: serviceReducer
});

const initialState = {};
const middleware = [thunk];

// const store = createStore(changeState);
const store = createStore(AllReducers, initialState, compose(applyMiddleware(...middleware)));
// const store = createStore(AllReducers, initialState, compose(applyMiddleware(...middleware), window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()));

export default store;
