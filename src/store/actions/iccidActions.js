import {
    API__GET_ICCIDS_SUCCESS,
    API__GET_ICCIDS_FAIL,
    API__GET_COUNTRIES_SUCCESS,
    API__GET_COUNTRIES_FAIL,
    FILTER_ICCIDS,
    API__CREATE_ICCID_SUCCESS,
    API__CREATE_ICCID_FAIL,
    FORCED_LOG_OUT,
    RESET_CREATE_OPERATOR_FORM,
    API__EDIT_ICCID_SUCCESS,
    API__EDIT_ICCID_FAIL,
    API__DELETE_ICCID_SUCCESS,
    API__DELETE_ICCID_FAIL,
    API__GET_COUNTRIES_AND_ICCIDS_SUCCESS,
    ERROR_500
} from '../actionTypes';
const URL = 'https://dev.sms-port.pro/api';
// const token = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJpYXQiOjE1OTY3MDc0MDAsImV4cCI6MTU5NjcxMTAwMCwicm9sZXMiOlsiUk9MRV9TVVBFUl9BRE1JTiIsIlJPTEVfVVNFUiJdLCJ1c2VybmFtZSI6ImFkbWluQG1haWwucnUifQ.UQdsWLrhFI4ULqWyujuIb-wibCBgMj5jU3KZl9dwt6Pn28ircJzatvYG2pXpnYQWRxDD8RrTTAz5zLRLtjGepbiFNov0rufdBxlEqMRT4hPjio3YuoNCthv0Glh8NMQ09u_ddowHJLM3ttrYVgRDrJ89Q6WK6o0tiJolJ-AkdApBfxThnV3Wtyx0O8PKA2zqxhcEncsAts1W5wzW-iO-nGf0Wd3erYKjbEjugPeRFpxY_hp5wpTe8e6PVZzkJDNzDfXkT-3AirDOJcSd6Ru_eVGLPGYomLV9jVHypa8cfvIGPpxu6zJ8__HL-bZe9h63gZqs79n-ttkxxpMbZq56BXakKm2HbKsP-TUvoG3YKK6w256D8tkMjYK47T61zVu1nrdz9ZUHwd_Wa6o8R4iN-_G4OBfYFYklOtFZH7d3mfxBoJN9C_9hv4miN-4R1uofHPlV-pS24dDv7czbJhT8O7qLnD1cOLxecgsL6y35E9DBdcQjRQIBxg0J9ysLikUjZzBaE5HiUcSFnonvARYfZunPfjZGTrZ8nejAUhJ0By6Jk_6upmYhwxJlyidqIU7FN3mZcMGwKdQKDjjprR43jFysA_BFx9hexXQa8i7oPUA_CQ8VjkGzt2iqiJ80INuqIinxnL257yRvQQg6PwJQzTcLLpHfaJwZLNjThA0eleM';

// export const getIccids = () => {
//     return (dispatch, getState) => {
//         const token = getState().authorization.token;
//         console.log('getAllIccid', token);

//         fetch(`${URL}/iccid`, {
//             method: 'GET',
//             headers: {
//                 Authorization: 'Bearer ' + token,
//                 'Content-Type': 'application/json'
//             }
//         })
//             .then(response => {
//                 if (response.ok) {
//                     // console.log(response);
//                     return response.json();
//                 } else {
//                     return response.json().then(err => Promise.reject(err));
//                 }
//             })
//             .then(response => {
//                 console.log('All ok', response);
//                 dispatch({ type: API__GET_ICCIDS_SUCCESS, payload: response });
//                 return response;
//             })
//             .catch(error => {
//                 if (error.code === 401) {
//                     // dispatch({ type: FORCED_LOG_OUT, payload: error });
//                 } else {
//                     dispatch({ type: API__GET_ICCIDS_FAIL, payload: error });
//                 }
//                 console.log('error in fetch: ', error);
//             });
//     };
// };

export const getCountriesAndIccids = () => {
    return (dispatch, getState) => {
        const token = getState().authorization.token;
        console.log('getCountries', token);

        fetch(`${URL}/country`, {
            method: 'GET',
            headers: {
                Authorization: 'Bearer ' + token,
                'Content-Type': 'application/json'
            }
        })
            .then(response => {
                if (response.ok) {
                    // console.log(response);
                    return response.json();
                } else {
                    return response.json().then(err => Promise.reject(err));
                }
            })
            .then(response => {
                console.log('All ok', response);
                // dispatch({ type: API__GET_COUNTRIES_SUCCESS, payload: response });

                fetch(`${URL}/iccid`, {
                    method: 'GET',
                    headers: {
                        Authorization: 'Bearer ' + token,
                        'Content-Type': 'application/json'
                    }
                })
                    .then(response2 => {
                        if (response2.ok) {
                            // console.log(response);
                            return response2.json();
                        } else {
                            return response2.json().then(err => Promise.reject(err));
                        }
                    })
                    .then(response2 => {
                        console.log('All ok', response2);
                        dispatch({ type: API__GET_COUNTRIES_AND_ICCIDS_SUCCESS, payload: response, payload2: response2 });
                        return response2;
                    })
                    .catch(error => {
                        if (error.code === 401) {
                            // dispatch({ type: FORCED_LOG_OUT, payload: error });
                        } else {
                            dispatch({ type: API__GET_ICCIDS_FAIL, payload: error });
                        }
                        console.log('error in fetch: ', error);
                    });
                return response;
            })
            .catch(error => {
                if (error.code === 401) {
                    dispatch({ type: FORCED_LOG_OUT, payload: error });
                } else {
                    dispatch({ type: API__GET_COUNTRIES_FAIL, payload: error });
                }
                console.log('error in fetch: ', error);
            });
    };
};

export const filterICCIDS = (id) => {
    return (dispatch, getState) => {
        console.log('filterICCIDS', id);
        dispatch({ type: FILTER_ICCIDS, payload: id });
    };
};

export const createICCID = (data) => {
    console.log('createICCID', data);
    return (dispatch, getState) => {
        // const formBody = Object.keys(data).map(key => encodeURIComponent(key) + '=' + encodeURIComponent(data[key])).join('&');
        const token = getState().authorization.token;

        const formData = new FormData();

        // for (const name in data) {
        //     formData.append(name, data[name]);
        //     console.log('name', name, data[name]);
        // }

        formData.append('prefix', data.prefix);
        formData.append('country_id', data.country_id);
        formData.append('operator', data.operator);
        formData.append('getnum_ussd', data.getnum_ussd);
        formData.append('newsim_ussd', data.newsim_ussd);
        formData.append('forwardable', data.forwardable);

        console.log('data.image', data.image);
        formData.append('image', data.image);

        console.log('formData', formData);
        console.log('data', data);

        fetch(`${URL}/iccid`, {
            method: 'POST',
            headers: {
                // 'Content-Type': 'application/x-www-form-urlencoded',
                Authorization: 'Bearer ' + token
            },
            body: formData
        })
            .then(response => {
                if (response.ok) {
                    return response.json();
                } else {
                    return response.json().then(err => Promise.reject(err));
                }
            })
            .then(response => {
                console.log('All ok', response);
                dispatch({ type: API__CREATE_ICCID_SUCCESS, payload: response, payload2: data });
                return response;
            })
            .catch(error => {
                console.log('error.code', error);
                if (error.code === 401) {
                    dispatch({ type: FORCED_LOG_OUT, payload: error });
                } else {
                    dispatch({ type: API__CREATE_ICCID_FAIL, payload: error });
                }
                console.log('error in fetch: ', error);
            });
    };
};

// static checkIn = (payload) => {
//     const { workerId, objectId, lat, lng, photo, autoClose = false } = payload;
//     const body = new FormData();
//     const type = 'image/jpg';

//     let { fileName } = photo;
//     if (!fileName) {
//       const getFileName = photo.uri.split('/');
//       fileName = getFileName[getFileName.length - 1];
//     }

//     body.append('objectId', objectId);
//     body.append('workerId', workerId);
//     body.append('lat', lat.toFixed(7).toString(10));
//     body.append('lng', lng.toFixed(7).toString(10));
//     body.append('autoClose', autoClose);
//     body.append('photo', {
//       name: fileName,
//       uri: photo.uri,
//       type,
//     });

//     const request = new Request(`${Config.API_URL}/foreman/timesheet`, {
//       method: 'POST',
//       headers: {
//         ...getBasicRequestHeaders(),
//         'Content-Type': 'multipart/form-data',
//       },
//       body,
//     });

//     return fetch(request);
//   };

export const editICCID = (data, id) => {
    console.log('editICCID', data, id);
    return (dispatch, getState) => {
        const formData = new FormData();

        // for (const name in data) {
        //     formData.append(name, data[name]);
        //     console.log('name', name, data[name]);
        // }

        formData.append('prefix', data.prefix);
        formData.append('country_id', data.country_id);
        formData.append('operator', data.operator);
        formData.append('getnum_ussd', data.getnum_ussd);
        formData.append('newsim_ussd', data.newsim_ussd);
        formData.append('forwardable', data.forwardable);
        formData.append('_method', 'PUT');

        console.log('data.image', data.image);
        formData.append('image', data.image);

        console.log('formData', formData);
        console.log('formData.get("image")', formData.get('image'));

        // const formBody = Object.keys(data).map(key => encodeURIComponent(key) + '=' + encodeURIComponent(data[key])).join('&');
        const token = getState().authorization.token;

        fetch(`${URL}/iccid/${id}`, {
            method: 'POST',
            headers: {
                // 'Content-Type': 'multipart/form-data',
                Authorization: 'Bearer ' + token
            },
            body: formData
        })
            .then(response => {
                if (response.ok) {
                    return response.json();
                } else {
                    return response.json().then(err => Promise.reject(err));
                }
            })
            .then(response => {
                console.log('All ok', response);
                dispatch({ type: API__EDIT_ICCID_SUCCESS, payload: response, payload2: data });
                return response;
            })
            .catch(error => {
                if (error.code === 401) {
                    dispatch({ type: FORCED_LOG_OUT, payload: error });
                } else {
                    dispatch({ type: API__EDIT_ICCID_FAIL, payload: error });
                }
                console.log('error in fetch: ', error);
            });
    };
};

export const deleteICCID = (data, id) => {
    return (dispatch, getState) => {
        const token = getState().authorization.token;
        console.log('deleteICCID', id);

        fetch(`${URL}/iccid/${id}`, {
            method: 'DELETE',
            headers: {
                Authorization: 'Bearer ' + token
            }
        })
            .then(response => {
                if (response.ok) {
                    return response.json();
                } else {
                    return response.json().then(err => Promise.reject(err));
                }
            })
            .then(response => {
                console.log('All ok', response);
                dispatch({ type: API__DELETE_ICCID_SUCCESS, payload: response, payload2: data });
                return response;
            })
            .catch(error => {
                if (error.code === 401) {
                    dispatch({ type: FORCED_LOG_OUT, payload: error });
                } else {
                    dispatch({ type: API__DELETE_ICCID_FAIL, payload: error });
                }
                console.log('error in fetch: ', error);
            });
    };
};

export const resetCreateOperatorForm = () => {
    return (dispatch, getState) => {
        console.log('onCancel');
        dispatch({ type: RESET_CREATE_OPERATOR_FORM });
    };
};
