import {
    API__LOGIN,
    API__LOGIN_SUCCESS,
    API__LOGIN_FAIL,
    API__REGISTER,
    API__REGISTER_SUCCESS,
    API__REGISTER_FAIL,
    LOG_OUT,
    RESET_REGISTER,
    RESET_LOGIN,
    DEFINE_SIDEBAR_MENU_ITEMS
} from '../actionTypes';
const URL = 'https://dev.sms-port.pro/api';
// const token = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJpYXQiOjE1OTY3MDc0MDAsImV4cCI6MTU5NjcxMTAwMCwicm9sZXMiOlsiUk9MRV9TVVBFUl9BRE1JTiIsIlJPTEVfVVNFUiJdLCJ1c2VybmFtZSI6ImFkbWluQG1haWwucnUifQ.UQdsWLrhFI4ULqWyujuIb-wibCBgMj5jU3KZl9dwt6Pn28ircJzatvYG2pXpnYQWRxDD8RrTTAz5zLRLtjGepbiFNov0rufdBxlEqMRT4hPjio3YuoNCthv0Glh8NMQ09u_ddowHJLM3ttrYVgRDrJ89Q6WK6o0tiJolJ-AkdApBfxThnV3Wtyx0O8PKA2zqxhcEncsAts1W5wzW-iO-nGf0Wd3erYKjbEjugPeRFpxY_hp5wpTe8e6PVZzkJDNzDfXkT-3AirDOJcSd6Ru_eVGLPGYomLV9jVHypa8cfvIGPpxu6zJ8__HL-bZe9h63gZqs79n-ttkxxpMbZq56BXakKm2HbKsP-TUvoG3YKK6w256D8tkMjYK47T61zVu1nrdz9ZUHwd_Wa6o8R4iN-_G4OBfYFYklOtFZH7d3mfxBoJN9C_9hv4miN-4R1uofHPlV-pS24dDv7czbJhT8O7qLnD1cOLxecgsL6y35E9DBdcQjRQIBxg0J9ysLikUjZzBaE5HiUcSFnonvARYfZunPfjZGTrZ8nejAUhJ0By6Jk_6upmYhwxJlyidqIU7FN3mZcMGwKdQKDjjprR43jFysA_BFx9hexXQa8i7oPUA_CQ8VjkGzt2iqiJ80INuqIinxnL257yRvQQg6PwJQzTcLLpHfaJwZLNjThA0eleM';

export const register = (data) => {
    console.log('register', data);
    return (dispatch, getState) => {
        const formBody = Object.keys(data).map(key => encodeURIComponent(key) + '=' + encodeURIComponent(data[key])).join('&');

        fetch(`${URL}/user/register`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            body: formBody
        })
            .then(response => {
                if (response.ok) {
                    return response.json();
                } else {
                    return response.json().then(err => Promise.reject(err));
                }
            })
            .then(response => {
                console.log('All ok', response);
                dispatch({ type: API__REGISTER_SUCCESS, payload: response });
                return response;
            })
            .catch(error => {
                dispatch({ type: API__REGISTER_FAIL, payload: error });
                console.log('error in fetch: ', error);
            });
    };
};

export const resetRegistration = () => {
    return (dispatch, getState) => {
        console.log('onCancel');
        dispatch({ type: RESET_REGISTER });
    };
};
export const logout = () => {
    return (dispatch, getState) => {
        console.log('logout');
        // localStorage.removeItem('token');
        dispatch({ type: LOG_OUT });
    };
};

export const login = (user) => {
    console.log('login', user);
    return (dispatch, getState) => {
        fetch(`${URL}/user/login`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(user)
        })
            .then(response => {
                if (response.ok) {
                    return response.json();
                } else {
                    return response.json().then(err => Promise.reject(err));
                }
            })
            .then(response => {
                console.log('All ok', response);
                dispatch({ type: API__LOGIN_SUCCESS, payload: response });

                // const menu = [
                //     { name: 'menu.partners' },
                //     { name: 'menu.devices' },
                //     { name: 'menu.simcards' },
                //     { name: 'menu.payments' },
                //     { name: 'menu.services' },
                //     { name: 'menu.transactions' },
                //     { name: 'menu.operators' },
                //     { name: 'menu.profiles' },
                //     { name: 'menu.simfilters' },
                //     { name: 'menu.orders' }
                //     // { name: 'menu.apikeys' }

                // ];
                // dispatch({ type: DEFINE_SIDEBAR_MENU_ITEMS, menuItems: menu });

                fetch(`${URL}/menu`, {
                    method: 'GET',
                    headers: {
                        Authorization: 'Bearer ' + response.token,
                        'Content-Type': 'application/json'
                    }
                })
                    .then(response2 => {
                        if (response2.ok) {
                            // console.log(response);
                            return response2.json();
                        } else {
                            return response2.json().then(err => Promise.reject(err));
                        }
                    })
                    .then(response2 => {
                        console.log('All ok', response2);
                        dispatch({ type: DEFINE_SIDEBAR_MENU_ITEMS, menuItems: response2 });
                        // dispatch({ type: API__GET_ICCIDS_SUCCESS, payload: response });
                        return response2;
                    })
                    .catch(error => {
                        if (error.code === 401) {
                            // dispatch({ type: FORCED_LOG_OUT, payload: error });
                        } else {
                            // dispatch({ type: API__GET_ICCIDS_FAIL, payload: error });
                        }
                        console.log('error in fetch: ', error);
                    });

                return response;
            })
            .catch(error => {
                dispatch({ type: API__LOGIN_FAIL, payload: error });
                console.log('error in fetch: ', error);
            });

        // const formBody = Object.keys(data).map(key => encodeURIComponent(key) + '=' + encodeURIComponent(data[key])).join('&');

        // fetch('http://78.110.62.195/api/user/register', {
        //     method: 'POST',
        //     headers: {
        //         'Content-Type': 'application/x-www-form-urlencoded'
        //     },
        //     body: formBody
        // })
        //     .then(response => {
        //         if (response.ok) {
        //             return response.json();
        //         } else {
        //             return response.json().then(err => Promise.reject(err));
        //         }
        //     })
        //     .then(response => {
        //         console.log('All ok', response);
        //         dispatch({ type: API__REGISTER_SUCCESS, payload: response });
        //         return response;
        //     })
        //     .catch(error => {
        //         dispatch({ type: API__REGISTER_FAIL, payload: error });
        //         console.log('error in fetch: ', error);
        //     });
    };
};
