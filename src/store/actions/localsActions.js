import {
    DEFINE_LOCALIZATION
} from '../actionTypes';

const URL = 'https://dev.sms-port.pro/api';

export const changeLocalization = (val) => {
    return (dispatch, getState) => {
        const token = getState().authorization.token;
        console.log('changeLocalization', val);

        fetch(`${URL}/translation?locale=${val.value}`, {
            method: 'GET',
            headers: {
                Authorization: 'Bearer ' + token,
                'Content-Type': 'application/json'
            }
        })
            .then(response => {
                if (response.ok) {
                    // console.log(response);
                    return response.json();
                } else {
                    return response.json().then(err => Promise.reject(err));
                }
            })
            .then(response => {
                console.log('All ok', response);
                dispatch({ type: DEFINE_LOCALIZATION, payload: val, payload2: response });
                // dispatch({ type: API__GET_ICCIDS_SUCCESS, payload: response });
                return response;
            })
            .catch(error => {
                if (error.code === 401) {
                    // dispatch({ type: FORCED_LOG_OUT, payload: error });
                } else {
                    // dispatch({ type: API__GET_ICCIDS_FAIL, payload: error });
                }
                console.log('error in fetch: ', error);
            });

        // dispatch({ type: DEFINE_LOCALIZATION, payload: val, payload2: localizations });
    };
};
