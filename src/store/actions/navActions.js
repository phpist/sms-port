import {
    SET_SIDEBAR
} from '../actionTypes';

export const setSidebarShow = (sidebarShow) => {
    return (dispatch, getState) => {
        dispatch({ type: SET_SIDEBAR, sidebarShow: sidebarShow });
    };
};

// export const defineSidebarMenu = () => {
//     return (dispatch, getState) => {
//         const menu = [
//             { name: 'menu.partners' },
//             { name: 'menu.devices' },
//             { name: 'menu.simcards' }
//         ];
//         dispatch({ type: DEFINE_SIDEBAR_MENU_ITEMS, menuItems: menu });
//     };
// };
