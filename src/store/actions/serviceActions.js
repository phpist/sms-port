import {
    // API__GET_ICCIDS_SUCCESS,
    // API__GET_ICCIDS_FAIL,
    // API__GET_COUNTRIES_SUCCESS,
    // API__GET_COUNTRIES_FAIL,
    // FILTER_ICCIDS,
    API__CREATE_SERVICE_SUCCESS,
    API__CREATE_SERVICE_FAIL,
    API__EDIT_SERVICE_SUCCESS,
    API__EDIT_SERVICE_FAIL,
    API__DELETE_SERVICE_SUCCESS,
    API__DELETE_SERVICE_FAIL,
    FORCED_LOG_OUT,
    // RESET_CREATE_OPERATOR_FORM,

    API__GET_SERVICES_SUCCESS,
    API__GET_SERVICES_FAIL,
    RESET_CREATE_SERVICE_FORM

    // ERROR_500
} from '../actionTypes';
const URL = 'https://dev.sms-port.pro/api';

export const getServices = () => {
    return (dispatch, getState) => {
        const token = getState().authorization.token;
        fetch(`${URL}/service`, {
            method: 'GET',
            headers: {
                Authorization: 'Bearer ' + token,
                'Content-Type': 'application/json'
            }
        })
            .then(response => {
                if (response.ok) {
                    // console.log(response);
                    return response.json();
                } else {
                    return response.json().then(err => Promise.reject(err));
                }
            })
            .then(response => {
                console.log('All ok', response);
                dispatch({ type: API__GET_SERVICES_SUCCESS, payload: response });
                return response;
            })
            .catch(error => {
                if (error.code === 401) {
                    dispatch({ type: FORCED_LOG_OUT, payload: error });
                } else {
                    dispatch({ type: API__GET_SERVICES_FAIL, payload: error });
                }
                console.log('error in fetch: ', error);
            });
    };
};

export const resetCreateServiceForm = () => {
    return (dispatch, getState) => {
        console.log('onCancel');
        dispatch({ type: RESET_CREATE_SERVICE_FORM });
    };
};

export const createService = (data) => {
    console.log('createICCID', data);
    return (dispatch, getState) => {
        // const formBody = Object.keys(data).map(key => encodeURIComponent(key) + '=' + encodeURIComponent(data[key])).join('&');
        const token = getState().authorization.token;

        const formData = new FormData();

        // for (const name in data) {
        //     formData.append(name, data[name]);
        //     console.log('name', name, data[name]);
        // }

        formData.append('internal_name', data.internal_name.trim());
        formData.append('formats', data.formats.trim());
        formData.append('forwardable', data.forwardable);
        formData.append('display_priority', data.display_priority.trim());
        formData.append('used_flush_delay', data.used_flush_delay.trim());
        formData.append('blacklist_flush_dela', data.blacklist_flush_dela.trim());
        formData.append('max_reports', data.max_reports.trim());
        formData.append('algoritm', data.algoritm.trim());

        console.log('data.image', data.image);
        formData.append('image', data.image);

        console.log('formData', formData);
        console.log('data', data);

        fetch(`${URL}/service`, {
            method: 'POST',
            headers: {
                // 'Content-Type': 'application/x-www-form-urlencoded',
                Authorization: 'Bearer ' + token
            },
            body: formData
        })
            .then(response => {
                if (response.ok) {
                    return response.json();
                } else {
                    return response.json().then(err => Promise.reject(err));
                }
            })
            .then(response => {
                console.log('All ok', response);
                dispatch({ type: API__CREATE_SERVICE_SUCCESS, payload: response, payload2: data });
                return response;
            })
            .catch(error => {
                console.log('error.code', error);
                if (error.code === 401) {
                    dispatch({ type: FORCED_LOG_OUT, payload: error });
                } else {
                    dispatch({ type: API__CREATE_SERVICE_FAIL, payload: error });
                }
                console.log('error in fetch: ', error);
            });
    };
};

export const editServiceAction = (data, id) => {
    console.log('editService', data, id);
    return (dispatch, getState) => {
        const formData = new FormData();

        // for (const name in data) {
        //     formData.append(name, data[name]);
        //     console.log('name', name, data[name]);
        // }

        formData.append('internal_name', data.internal_name);
        formData.append('formats', data.formats);
        formData.append('forwardable', data.forwardable);
        formData.append('display_priority', data.display_priority);
        formData.append('used_flush_delay', data.used_flush_delay);
        formData.append('blacklist_flush_dela', data.blacklist_flush_dela);
        formData.append('max_reports', data.max_reports);
        formData.append('algoritm', data.algoritm);
        formData.append('_method', 'PUT');

        console.log('data.image', data.image);
        formData.append('image', data.image);

        console.log('formData', formData);
        console.log('data', data);

        // const formBody = Object.keys(data).map(key => encodeURIComponent(key) + '=' + encodeURIComponent(data[key])).join('&');
        const token = getState().authorization.token;

        fetch(`${URL}/service/${id}`, {
            method: 'POST',
            headers: {
                // 'Content-Type': 'multipart/form-data',
                // 'Content-Type': 'application/x-www-form-urlencoded',
                Authorization: 'Bearer ' + token
            },
            body: formData
        })
            .then(response => {
                if (response.ok) {
                    return response.json();
                } else {
                    return response.json().then(err => Promise.reject(err));
                }
            })
            .then(response => {
                console.log('All ok', response);
                dispatch({ type: API__EDIT_SERVICE_SUCCESS, payload: response, payload2: data });
                return response;
            })
            .catch(error => {
                if (error.code === 401) {
                    dispatch({ type: FORCED_LOG_OUT, payload: error });
                } else {
                    dispatch({ type: API__EDIT_SERVICE_FAIL, payload: error });
                }
                console.log('error in fetch: ', error);
            });
    };
};

export const deleteServiceAction = (data, id) => {
    return (dispatch, getState) => {
        const token = getState().authorization.token;
        console.log('deleteServiceAction', id);

        fetch(`${URL}/service/${id}`, {
            method: 'DELETE',
            headers: {
                Authorization: 'Bearer ' + token
            }
        })
            .then(response => {
                if (response.ok) {
                    return response.json();
                } else {
                    return response.json().then(err => Promise.reject(err));
                }
            })
            .then(response => {
                console.log('All ok', response);
                dispatch({ type: API__DELETE_SERVICE_SUCCESS, payload: response, payload2: data });
                return response;
            })
            .catch(error => {
                if (error.code === 401) {
                    dispatch({ type: FORCED_LOG_OUT, payload: error });
                } else {
                    dispatch({ type: API__DELETE_SERVICE_FAIL, payload: error });
                }
                console.log('error in fetch: ', error);
            });
    };
};

// export const getCountriesAndIccids = () => {
//     return (dispatch, getState) => {
//         const token = getState().authorization.token;
//         console.log('getCountries', token);

//         fetch(`${URL}/country`, {
//             method: 'GET',
//             headers: {
//                 Authorization: 'Bearer ' + token,
//                 'Content-Type': 'application/json'
//             }
//         })
//             .then(response => {
//                 if (response.ok) {
//                     // console.log(response);
//                     return response.json();
//                 } else {
//                     return response.json().then(err => Promise.reject(err));
//                 }
//             })
//             .then(response => {
//                 console.log('All ok', response);
//                 // dispatch({ type: API__GET_COUNTRIES_SUCCESS, payload: response });

//                 fetch(`${URL}/iccid`, {
//                     method: 'GET',
//                     headers: {
//                         Authorization: 'Bearer ' + token,
//                         'Content-Type': 'application/json'
//                     }
//                 })
//                     .then(response2 => {
//                         if (response2.ok) {
//                             // console.log(response);
//                             return response2.json();
//                         } else {
//                             return response2.json().then(err => Promise.reject(err));
//                         }
//                     })
//                     .then(response2 => {
//                         console.log('All ok', response2);
//                         dispatch({ type: API__GET_COUNTRIES_AND_ICCIDS_SUCCESS, payload: response, payload2: response2 });
//                         return response2;
//                     })
//                     .catch(error => {
//                         if (error.code === 401) {
//                             // dispatch({ type: FORCED_LOG_OUT, payload: error });
//                         } else {
//                             dispatch({ type: API__GET_ICCIDS_FAIL, payload: error });
//                         }
//                         console.log('error in fetch: ', error);
//                     });
//                 return response;
//             })
//             .catch(error => {
//                 if (error.code === 401) {
//                     dispatch({ type: FORCED_LOG_OUT, payload: error });
//                 } else {
//                     dispatch({ type: API__GET_COUNTRIES_FAIL, payload: error });
//                 }
//                 console.log('error in fetch: ', error);
//             });
//     };
// };

// export const filterICCIDS = (id) => {
//     return (dispatch, getState) => {
//         console.log('filterICCIDS', id);
//         dispatch({ type: FILTER_ICCIDS, payload: id });
//     };
// };

// export const createICCID = (data) => {
//     console.log('createICCID', data);
//     return (dispatch, getState) => {
//         // const formBody = Object.keys(data).map(key => encodeURIComponent(key) + '=' + encodeURIComponent(data[key])).join('&');
//         const token = getState().authorization.token;

//         const formData = new FormData();

//         // for (const name in data) {
//         //     formData.append(name, data[name]);
//         //     console.log('name', name, data[name]);
//         // }

//         formData.append('prefix', data.prefix);
//         formData.append('country_id', data.country_id);
//         formData.append('operator', data.operator);
//         formData.append('getnum_ussd', data.getnum_ussd);
//         formData.append('newsim_ussd', data.newsim_ussd);
//         formData.append('forwardable', data.forwardable);

//         console.log('data.image', data.image);
//         formData.append('image', data.image);

//         console.log('formData', formData);
//         console.log('data', data);

//         fetch(`${URL}/iccid`, {
//             method: 'POST',
//             headers: {
//                 // 'Content-Type': 'application/x-www-form-urlencoded',
//                 Authorization: 'Bearer ' + token
//             },
//             body: formData
//         })
//             .then(response => {
//                 if (response.ok) {
//                     return response.json();
//                 } else {
//                     return response.json().then(err => Promise.reject(err));
//                 }
//             })
//             .then(response => {
//                 console.log('All ok', response);
//                 dispatch({ type: API__CREATE_ICCID_SUCCESS, payload: response, payload2: data });
//                 return response;
//             })
//             .catch(error => {
//                 console.log('error.code', error);
//                 if (error.code === 401) {
//                     dispatch({ type: FORCED_LOG_OUT, payload: error });
//                 } else {
//                     dispatch({ type: API__CREATE_ICCID_FAIL, payload: error });
//                 }
//                 console.log('error in fetch: ', error);
//             });
//     };
// };

// export const editICCID = (data, id) => {
//     console.log('editICCID', data, id);
//     return (dispatch, getState) => {
//         const formData = new FormData();

//         // for (const name in data) {
//         //     formData.append(name, data[name]);
//         //     console.log('name', name, data[name]);
//         // }

//         formData.append('prefix', data.prefix);
//         formData.append('country_id', data.country_id);
//         formData.append('operator', data.operator);
//         formData.append('getnum_ussd', data.getnum_ussd);
//         formData.append('newsim_ussd', data.newsim_ussd);
//         formData.append('forwardable', data.forwardable);
//         formData.append('_method', 'PUT');

//         console.log('data.image', data.image);
//         formData.append('image', data.image);

//         console.log('formData', formData);
//         console.log('formData.get("image")', formData.get('image'));

//         // const formBody = Object.keys(data).map(key => encodeURIComponent(key) + '=' + encodeURIComponent(data[key])).join('&');
//         const token = getState().authorization.token;

//         fetch(`${URL}/iccid/${id}`, {
//             method: 'POST',
//             headers: {
//                 // 'Content-Type': 'multipart/form-data',
//                 Authorization: 'Bearer ' + token
//             },
//             body: formData
//         })
//             .then(response => {
//                 if (response.ok) {
//                     return response.json();
//                 } else {
//                     return response.json().then(err => Promise.reject(err));
//                 }
//             })
//             .then(response => {
//                 console.log('All ok', response);
//                 dispatch({ type: API__EDIT_ICCID_SUCCESS, payload: response, payload2: data });
//                 return response;
//             })
//             .catch(error => {
//                 if (error.code === 401) {
//                     dispatch({ type: FORCED_LOG_OUT, payload: error });
//                 } else {
//                     dispatch({ type: API__EDIT_ICCID_FAIL, payload: error });
//                 }
//                 console.log('error in fetch: ', error);
//             });
//     };
// };

// export const deleteICCID = (data, id) => {
//     return (dispatch, getState) => {
//         const token = getState().authorization.token;
//         console.log('deleteICCID', id);

//         fetch(`${URL}/iccid/${id}`, {
//             method: 'DELETE',
//             headers: {
//                 Authorization: 'Bearer ' + token
//             }
//         })
//             .then(response => {
//                 if (response.ok) {
//                     return response.json();
//                 } else {
//                     return response.json().then(err => Promise.reject(err));
//                 }
//             })
//             .then(response => {
//                 console.log('All ok', response);
//                 dispatch({ type: API__DELETE_ICCID_SUCCESS, payload: response, payload2: data });
//                 return response;
//             })
//             .catch(error => {
//                 if (error.code === 401) {
//                     dispatch({ type: FORCED_LOG_OUT, payload: error });
//                 } else {
//                     dispatch({ type: API__DELETE_ICCID_FAIL, payload: error });
//                 }
//                 console.log('error in fetch: ', error);
//             });
//     };
// };

// export const resetCreateOperatorForm = () => {
//     return (dispatch, getState) => {
//         console.log('onCancel');
//         dispatch({ type: RESET_CREATE_OPERATOR_FORM });
//     };
// };
