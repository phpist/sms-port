import { store } from 'react-notifications-component';
import {
    API__GET_ICCIDS_SUCCESS,
    API__GET_ICCIDS_FAIL,
    API__GET_COUNTRIES_SUCCESS,
    API__GET_COUNTRIES_FAIL,
    API__CREATE_ICCID_SUCCESS,
    API__CREATE_ICCID_FAIL,
    FILTER_ICCIDS,
    RESET_CREATE_OPERATOR_FORM,
    API__EDIT_ICCID_SUCCESS,
    API__EDIT_ICCID_FAIL,
    API__DELETE_ICCID_SUCCESS,
    API__DELETE_ICCID_FAIL,
    API__GET_COUNTRIES_AND_ICCIDS_SUCCESS,
    ERROR_500
} from '../actionTypes';
import { object } from 'prop-types';

const initState = {
    iccids: [],
    iccidsFiltered: [],
    countries: [],
    countriesSelect: [],
    newOperatorCreated: false,
    seccessfullyCreatedOperator: false,
    errorOperator: '',
    errorPrefix: '',
    errorCountry: '',
    errorImage: ''
};

const iccidReducer = (state = initState, action) => {
    switch (action.type) {
    case API__GET_COUNTRIES_AND_ICCIDS_SUCCESS: {
        console.log(API__GET_ICCIDS_SUCCESS);
        const iccids = action.payload2.iccid_bindings;
        let countries = action.payload.countries;

        // Transforming countries object in object required react-select {value,label}
        countries = countries.map(item => {
            return { value: item.id, label: item.name, alpha2: item.alpha2 };
        });

        // Creating rendering only countries that exists
        const countriesSelect = countries.filter(array => iccids.some(filter => filter.country_id === array.value));

        // ===============================================================================
        //  Show all
        // countriesSelect.unshift({ value: 0, label: 'Все', alpha2: 'all' });
        // ===============================================================================

        // Creating new property in iccids (operators) for avoinding bugs with displaying countries
        for (let i = 0; i < iccids.length; i++) {
            const objCountry = countries.find(filter => filter.value === iccids[i].country_id);
            iccids[i].countryName = objCountry.label;
        }

        return {
            ...state,
            iccids: iccids,
            iccidsFiltered: iccids,
            countries: countries,
            countriesSelect: countriesSelect
        };
    }
    case API__GET_ICCIDS_FAIL:
        console.log(API__GET_ICCIDS_FAIL);
        store.addNotification({
            title: '',
            message: action.payload.message,
            type: 'danger',
            insert: 'top',
            container: 'bottom-right',
            animationIn: ['animated', 'bounceIn'],
            animationOut: ['animated', 'bounceOut'],
            dismiss: {
                duration: 6000,
                onScreen: false,
                pauseOnHover: true
            }
        });
        return {
            ...state
        };

        // case API__GET_COUNTRIES_SUCCESS: {
        //     console.log(API__GET_COUNTRIES_SUCCESS, action.payload.countries);

        //     const iccids = state.iccids;
        //     let countries = action.payload.countries;
        //     countries = countries.map(item => {
        //         // console.log('item', item);
        //         return { value: item.id, label: item.name, alpha2: item.alpha2 };
        //     });

        //     const myArrayFiltered = countries.filter(array => iccids.some(filter => filter.country_id === array.value));
        //     myArrayFiltered.unshift({ label: 'Все', value: '', alpha2: 'all' });

    //     return {
    //         ...state,
    //         countries: countries,
    //         countriesSelect: myArrayFiltered
    //     };
    // }
    case API__GET_COUNTRIES_FAIL:
        console.log(API__GET_COUNTRIES_FAIL);
        // store.addNotification({
        //     title: '',
        //     message: action.payload.message,
        //     type: 'danger',
        //     insert: 'top',
        //     container: 'bottom-right',
        //     animationIn: ['animated', 'bounceIn'],
        //     animationOut: ['animated', 'bounceOut'],
        //     dismiss: {
        //         duration: 6000,
        //         onScreen: false,
        //         pauseOnHover: true
        //     }
        // });
        return {
            ...state
        };

    case FILTER_ICCIDS: {
        console.log(FILTER_ICCIDS, action.payload);
        const iccids = state.iccids;
        const myArrayFiltered = iccids.filter(array => array.country_id === action.payload);
        console.log('myArrayFiltered', myArrayFiltered);

        return {
            ...state,
            iccidsFiltered: myArrayFiltered.length > 0 ? myArrayFiltered : iccids
        };
    }

    case API__CREATE_ICCID_SUCCESS: {
        console.log(API__CREATE_ICCID_SUCCESS, action.payload2);
        const iccids = state.iccids;
        const iccidsFiltered = state.iccidsFiltered;
        const countries = state.countries;

        store.addNotification({
            title: '',
            message: 'Создан новый оператор!',
            type: 'success',
            insert: 'top',
            container: 'bottom-right',
            animationIn: ['animated', 'bounceIn'],
            animationOut: ['animated', 'bounceOut'],
            dismiss: {
                duration: 6000,
                onScreen: false,
                pauseOnHover: true
            }
        });

        const objCountry = countries.find(filter => filter.value === action.payload2.country_id);

        let nameUrl;
        if (action.payload2.image) {
            nameUrl = '/images/iccid/' + action.payload2.image.name;
        }

        const joinReturnedId = { ...action.payload2, id: action.payload.id, image_path: nameUrl || null, countryName: objCountry.label };
        // iccids.push(joinReturnedId);
        // if (action.payload2.country_id === iccidsFiltered[0].country_id) {
        iccids.push(joinReturnedId);
        // }

        return {
            ...state,
            iccids: [...iccids],
            iccidsFiltered: [...iccids],
            seccessfullyCreatedOperator: true
        };
    }

    case API__CREATE_ICCID_FAIL: {
        let errorOperator, errorPrefix, errorCountry, errorImage;
        let error500;
        try {
            errorOperator = action.payload.errors.find(o => o.property === 'operator');
            errorPrefix = action.payload.errors.find(o => o.property === 'prefix');
            errorCountry = action.payload.errors.find(o => o.property === 'countryId');
            errorImage = action.payload.errors.find(o => o.property === 'image');
        } catch (err) {
            error500 = true;
        }

        console.log(API__CREATE_ICCID_FAIL, action.payload);
        if (error500) {
            store.addNotification({
                title: '',
                message: 'Операция не может быть выполнена',
                type: 'danger',
                insert: 'top',
                container: 'bottom-right',
                animationIn: ['animated', 'bounceIn'],
                animationOut: ['animated', 'bounceOut'],
                dismiss: {
                    duration: 6000,
                    onScreen: false,
                    pauseOnHover: true
                }
            });
            return {
                ...state,
                seccessfullyCreatedOperator: false
            };
        } else {
            return {
                ...state,
                seccessfullyCreatedOperator: false,
                errorOperator: errorOperator,
                errorPrefix: errorPrefix,
                errorCountry: errorCountry,
                errorImage: errorImage
            };
        }
    }

    case API__EDIT_ICCID_SUCCESS: {
        console.log(API__EDIT_ICCID_SUCCESS, action.payload2);
        const iccids = state.iccids;
        const iccidsFiltered = state.iccidsFiltered;
        const countries = state.countries;
        const image = action.payload2.image;
        store.addNotification({
            title: '',
            message: 'Оператор отредактирован!',
            type: 'success',
            insert: 'top',
            container: 'bottom-right',
            animationIn: ['animated', 'bounceIn'],
            animationOut: ['animated', 'bounceOut'],
            dismiss: {
                duration: 6000,
                onScreen: false,
                pauseOnHover: true
            }
        });

        const objCountry = countries.find(filter => filter.value === action.payload2.country_id);

        console.log('action.payload2', typeof action.payload2);

        let nameUrl;
        if (action.payload2.image) {
            if (typeof action.payload2.image === 'object') {
                nameUrl = '/images/iccid/' + image.name;
            } else {
                console.log('image', image);
                nameUrl = image.slice(24);
            }
        }

        // const operator = iccids.find(filter => filter.id === action.payload.id);

        const joinReturnedId = { ...action.payload2, id: action.payload.id, image_path: nameUrl || null, countryName: objCountry.label };
        const indexOfIccids = iccids.findIndex(x => x.id === joinReturnedId.id);
        // const indexOfIccidsFiltered = iccidsFiltered.findIndex(x => x.id === joinReturnedId.id);

        iccids[indexOfIccids] = joinReturnedId;

        // if (iccidsFiltered.every((item) => item.country_id === action.payload2.country_id)) {
        //     iccidsFiltered[indexOfIccidsFiltered] = joinReturnedId;
        // } else {
        //     if (operator.country_id === action.payload2.country_id) {
        //         iccidsFiltered = iccids;
        //     } else {
        //         iccidsFiltered.splice(indexOfIccidsFiltered, 1);
        //     }
        // }
        console.log('new', iccids);
        console.log('new2 fil', iccidsFiltered);

        return {
            ...state,
            iccids: [...iccids],
            iccidsFiltered: [...iccids],
            seccessfullyCreatedOperator: true
        };
    }

    case API__EDIT_ICCID_FAIL: {
        let errorOperator, errorPrefix, errorCountry, errorImage;
        let error500;
        try {
            errorOperator = action.payload.errors.find(o => o.property === 'operator');
            errorPrefix = action.payload.errors.find(o => o.property === 'prefix');
            errorCountry = action.payload.errors.find(o => o.property === 'countryId');
            errorImage = action.payload.errors.find(o => o.property === 'image');
        } catch (err) {
            error500 = true;
        }

        console.log(API__EDIT_ICCID_FAIL, action.payload);
        if (error500) {
            store.addNotification({
                title: '',
                message: 'Операция не может быть выполнена',
                type: 'danger',
                insert: 'top',
                container: 'bottom-right',
                animationIn: ['animated', 'bounceIn'],
                animationOut: ['animated', 'bounceOut'],
                dismiss: {
                    duration: 6000,
                    onScreen: false,
                    pauseOnHover: true
                }
            });
            return {
                ...state,
                seccessfullyCreatedOperator: false
            };
        } else {
            return {
                ...state,
                seccessfullyCreatedOperator: false,
                errorOperator: errorOperator,
                errorPrefix: errorPrefix,
                errorCountry: errorCountry,
                errorImage: errorImage
            };
        }
    }

    case API__DELETE_ICCID_SUCCESS: {
        console.log(API__DELETE_ICCID_SUCCESS, action.payload2);
        const iccids = state.iccids;
        const iccidsFiltered = state.iccidsFiltered;

        store.addNotification({
            title: '',
            message: 'Оператор удален!',
            type: 'success',
            insert: 'top',
            container: 'bottom-right',
            animationIn: ['animated', 'bounceIn'],
            animationOut: ['animated', 'bounceOut'],
            dismiss: {
                duration: 6000,
                onScreen: false,
                pauseOnHover: true
            }
        });

        const joinReturnedId = { ...action.payload2, id: action.payload.id };
        const indexOfIccids = iccids.findIndex(x => x.id === joinReturnedId.id);
        const indexOfIccidsFiltered = iccidsFiltered.findIndex(x => x.id === joinReturnedId.id);
        iccids.splice(indexOfIccids, 1);

        // if (action.payload2.country_id === iccidsFiltered[0].country_id) {
        iccidsFiltered.splice(indexOfIccidsFiltered, 1);
        // }
        console.log('new', iccids);

        return {
            ...state,
            iccids: [...iccids],
            iccidsFiltered: [...iccidsFiltered],
            seccessfullyCreatedOperator: true
        };
    }

    case API__DELETE_ICCID_FAIL: {
        const errorOperator = action.payload.errors.find(o => o.property === 'operator');
        const errorPrefix = action.payload.errors.find(o => o.property === 'prefix');
        const errorCountry = action.payload.errors.find(o => o.property === 'countryId');

        // countryId
        // const errorCommunicationType = action.payload.errors.find(o => o.property === 'communicationType');

        console.log(API__DELETE_ICCID_FAIL, action.payload);
        store.addNotification({
            title: '',
            message: action.payload.error,
            type: 'danger',
            insert: 'top',
            container: 'bottom-right',
            animationIn: ['animated', 'bounceIn'],
            animationOut: ['animated', 'bounceOut'],
            dismiss: {
                duration: 6000,
                onScreen: false,
                pauseOnHover: true
            }
        });

        return {
            ...state,
            seccessfullyCreatedOperator: false,
            errorOperator: errorOperator,
            errorPrefix: errorPrefix,
            errorCountry: errorCountry
        };
    }

    case RESET_CREATE_OPERATOR_FORM: {
        console.log('RESET_CREATE_OPERATOR_FORM');
        return {
            ...state,
            errorOperator: '',
            errorPrefix: '',
            errorCountry: '',
            errorImage: '',
            seccessfullyCreatedOperator: false
        };
    }
    case ERROR_500: {
        console.log(ERROR_500);
        store.addNotification({
            title: '',
            message: action.payload.error,
            type: 'danger',
            insert: 'top',
            container: 'bottom-right',
            animationIn: ['animated', 'bounceIn'],
            animationOut: ['animated', 'bounceOut'],
            dismiss: {
                duration: 6000,
                onScreen: false,
                pauseOnHover: true
            }
        });
        return {
            ...state
            // errorOperator: '',
            // errorPrefix: '',
            // errorCountry: '',
            // seccessfullyCreatedOperator: false
        };
    }

    default:
        return state;
    }
};
export default iccidReducer;
