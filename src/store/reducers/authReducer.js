import { store } from 'react-notifications-component';

import {
    API__LOGIN,
    API__LOGIN_SUCCESS,
    API__LOGIN_FAIL,
    API__REGISTER,
    API__REGISTER_SUCCESS,
    API__REGISTER_FAIL,
    LOG_OUT,
    FORCED_LOG_OUT,

    RESET_REGISTER,
    RESET_LOGIN
} from '../actionTypes';

const initState = {
    isFetching: false,
    authorized: !!localStorage.getItem('token'),
    success: '',
    // errors: [],
    errorName: '',
    errorEmail: '',
    errorCommunicationType: '',
    errorCommunicationTypeResolved: false,
    errorCommunication: '',
    errorMessage: '',
    token: localStorage.getItem('token')
};

const authReducer = (state = initState, action) => {
    switch (action.type) {
    case API__LOGIN:
        console.log(API__LOGIN);
        return {
            ...state
            // isFetching: action.isFetching
        };

    case API__LOGIN_SUCCESS:
        console.log(API__LOGIN_SUCCESS, action.payload);
        window.localStorage.setItem('token', action.payload.token);
        store.addNotification({
            title: '',
            message: 'Успех!',
            type: 'success',
            insert: 'top',
            container: 'bottom-right',
            animationIn: ['animated', 'bounceIn'],
            animationOut: ['animated', 'bounceOut'],
            dismiss: {
                duration: 6000,
                onScreen: false,
                pauseOnHover: true
            }
        });
        return {
            ...state,
            // token: action.payload.token,
            authorized: true,
            token: localStorage.getItem('token')
            // isFetching: action.isFetching,
            // success: action.payload
            // token: action.token
        };

    case API__LOGIN_FAIL:
        console.log(API__LOGIN_FAIL, action.payload);
        store.addNotification({
            title: '',
            message: action.payload.message,
            type: 'danger',
            insert: 'top',
            container: 'bottom-right',
            animationIn: ['animated', 'bounceIn'],
            animationOut: ['animated', 'bounceOut'],
            dismiss: {
                duration: 6000,
                onScreen: false,
                pauseOnHover: true
            }
        });
        return {
            ...state
            // isFetching: action.isFetching,
            // data: action.payload
            // errors: action.payload
        };

    case API__REGISTER:
        console.log(API__REGISTER);
        return {
            ...state,
            isFetching: action.isFetching
        };

    case API__REGISTER_SUCCESS:
        console.log(API__REGISTER_SUCCESS, action.payload);
        store.addNotification({
            title: '',
            message: action.payload.message,
            type: 'success',
            insert: 'top',
            container: 'bottom-right',
            animationIn: ['animated', 'bounceIn'],
            animationOut: ['animated', 'bounceOut'],
            dismiss: {
                duration: 6000,
                onScreen: false,
                pauseOnHover: true
            }
        });
        return {
            ...state,
            // isFetching: action.isFetching,
            success: action.payload.message
        };

    case API__REGISTER_FAIL: {
        const errorName = action.payload.errors.find(o => o.property === 'name');
        const errorEmail = action.payload.errors.find(o => o.property === 'email');
        const errorCommunicationType = action.payload.errors.find(o => o.property === 'communicationType');

        console.log('API__REGISTER_FAIL', action.payload);

        // if (errorCommunicationType) {
        return {
            ...state,
            // isFetching: action.isFetching,
            errorName: errorName,
            errorEmail: errorEmail,
            errorCommunicationType: errorCommunicationType
        };
        // }

        // return {
        //     ...state,
        //     // isFetching: action.isFetching,
        //     errorName: errorName,
        //     errorEmail: errorEmail,
        //     errorCommunicationType: errorCommunicationType
        // };
    }

    case LOG_OUT:
        console.log(LOG_OUT);
        window.localStorage.removeItem('token');
        window.localStorage.removeItem('menuItems');
        return {
            ...state,
            authorized: false
            // isFetching: action.isFetching,
            // authorized: false
            // token: ''
        };

    case FORCED_LOG_OUT:
        console.log(FORCED_LOG_OUT);
        window.localStorage.removeItem('token');
        window.localStorage.removeItem('menuItems');
        store.addNotification({
            title: '',
            message: action.payload.message,
            type: 'danger',
            insert: 'top',
            container: 'bottom-right',
            animationIn: ['animated', 'bounceIn'],
            animationOut: ['animated', 'bounceOut'],
            dismiss: {
                duration: 6000,
                onScreen: false,
                pauseOnHover: true
            }
        });
        return {
            ...state,
            authorized: false
            // isFetching: action.isFetching,
            // authorized: false
            // token: ''
        };

    case RESET_REGISTER: {
        console.log('RESET_REGISTER');
        return {
            ...state,
            errorName: '',
            errorEmail: '',
            errorCommunication_type: '',
            errorCommunication: '',
            errorMessage: '',
            success: ''
        }; }
    default:
        return state;
    }
};
export default authReducer;
