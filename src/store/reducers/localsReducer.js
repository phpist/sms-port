import {
    DEFINE_LOCALIZATION
} from '../actionTypes';

const initState = {
    localizations: {},
    chosenLocal: JSON.parse(window.localStorage.getItem('lang')) || {
        label: 'English',
        value: 'en'
    }
};

const localsReducer = (state = initState, action) => {
    switch (action.type) {
    case DEFINE_LOCALIZATION: {
        console.log(DEFINE_LOCALIZATION, action.payload.label);
        window.localStorage.setItem('lang', JSON.stringify(action.payload));
        const locs = action.payload2.translations;

        const localizations = { [action.payload.value]: { ...locs } };

        // const localizations = {
        //     ru: {
        //         greeting: 'Привет, пользователь! Как вы?',
        //         partners: 'Партнеры',
        //         devices: 'Устройства',
        //         simcards: 'Симкарты',
        //         payouts: 'Выплаты',
        //         services: 'Сервисы',
        //         transactions: 'Транзакции',
        //         operators: 'Операторы',
        //         profiles: 'Профили',
        //         filers_sms: 'Фильтры смс',
        //         orders: 'Заказы',
        //         api_keys: 'API-ключи'
        //     },
        //     en: {
        //         greeting: 'Hello user! How are you?',
        //         partners: 'Partners',
        //         devices: 'Devices',
        //         simcards: 'Simcards',
        //         payouts: 'Payouts',
        //         services: 'Services',
        //         transactions: 'Transactions',
        //         operators: 'Operators',
        //         profiles: 'Profiles',
        //         filers_sms: 'Filters sms',
        //         orders: 'Orders',
        //         api_keys: 'API-keys'

        //     },
        //     es: {
        //         greeting: '¡Hola user! ¿Cómo estás?',
        //         partners: 'Partners',
        //         devices: 'Devices',
        //         simcards: 'Simcards',
        //         payouts: 'Payouts',
        //         services: 'Services',
        //         transactions: 'Transactions',
        //         operators: 'Operators',
        //         profiles: 'Profiles',
        //         filers_sms: 'Filters sms',
        //         orders: 'Orders',
        //         api_keys: 'API-keys'
        //     },
        //     fr: {
        //         greeting: 'Bonjour user! Comment ça va?',
        //         partners: 'Partners',
        //         devices: 'Devices',
        //         simcards: 'Simcards',
        //         payouts: 'Payouts',
        //         services: 'Services',
        //         transactions: 'Transactions',
        //         operators: 'Operators',
        //         profiles: 'Profiles',
        //         filers_sms: 'Filters sms',
        //         orders: 'Orders',
        //         api_keys: 'API-keys'
        //     },
        //     de: {
        //         greeting: 'Hallo user! Wie geht\'s?',
        //         partners: 'Partners',
        //         devices: 'Devices',
        //         simcards: 'Simcards',
        //         payouts: 'Payouts',
        //         services: 'Services',
        //         transactions: 'Transactions',
        //         operators: 'Operators',
        //         profiles: 'Profiles',
        //         filers_sms: 'Filters sms',
        //         orders: 'Orders',
        //         api_keys: 'API-keys'
        //     }
        // };

        console.log('trtr', localizations);

        return {
            ...state,
            chosenLocal: action.payload,
            localizations: localizations
        }; }
    default:
        return state;
    }
};
export default localsReducer;
