import { store } from 'react-notifications-component';
import {
    API__GET_ICCIDS_SUCCESS,
    API__GET_ICCIDS_FAIL,
    API__GET_COUNTRIES_SUCCESS,
    API__GET_COUNTRIES_FAIL,
    API__CREATE_ICCID_SUCCESS,
    API__CREATE_ICCID_FAIL,
    FILTER_ICCIDS,
    RESET_CREATE_OPERATOR_FORM,
    API__EDIT_ICCID_SUCCESS,
    API__EDIT_ICCID_FAIL,
    API__DELETE_ICCID_SUCCESS,
    API__DELETE_ICCID_FAIL,
    API__GET_COUNTRIES_AND_ICCIDS_SUCCESS,
    API__EDIT_SERVICE_SUCCESS,
    API__EDIT_SERVICE_FAIL,
    API__DELETE_SERVICE_SUCCESS,
    API__DELETE_SERVICE_FAIL,

    API__GET_SERVICES_SUCCESS,
    API__GET_SERVICES_FAIL,
    RESET_CREATE_SERVICE_FORM,
    API__CREATE_SERVICE_SUCCESS,
    API__CREATE_SERVICE_FAIL,

    ERROR_500
} from '../actionTypes';

const initState = {
    services: [],
    newServiceCreated: false,
    seccessfullyCreatedService: false,
    errorMaxReports: '',
    errorInternalName: '',
    errorFormats: '',
    errorImage: ''
    // errorPrefix: '',
    // errorCountry: '',
    // errorImage: ''
};

const serviceReducer = (state = initState, action) => {
    switch (action.type) {
    case API__GET_SERVICES_SUCCESS: {
        console.log(API__GET_SERVICES_SUCCESS);
        const services = action.payload.services;

        return {
            ...state,
            services: services
        };
    }
    case API__GET_SERVICES_FAIL:
        console.log(API__GET_SERVICES_FAIL);
        store.addNotification({
            title: '',
            message: action.payload.message,
            type: 'danger',
            insert: 'top',
            container: 'bottom-right',
            animationIn: ['animated', 'bounceIn'],
            animationOut: ['animated', 'bounceOut'],
            dismiss: {
                duration: 6000,
                onScreen: false,
                pauseOnHover: true
            }
        });
        return {
            ...state
        };

    case API__CREATE_SERVICE_SUCCESS: {
        console.log(API__CREATE_SERVICE_SUCCESS, action.payload2);
        const services = state.services;

        store.addNotification({
            title: '',
            message: 'Создан новый сервис!',
            type: 'success',
            insert: 'top',
            container: 'bottom-right',
            animationIn: ['animated', 'bounceIn'],
            animationOut: ['animated', 'bounceOut'],
            dismiss: {
                duration: 6000,
                onScreen: false,
                pauseOnHover: true
            }
        });

        let nameUrl;
        if (action.payload2.image) {
            nameUrl = '/images/iccid/' + action.payload2.image.name;
        }

        const joinReturnedId = { ...action.payload2, id: action.payload.id, image_path: nameUrl || null };

        services.push(joinReturnedId);

        return {
            ...state,
            services: [...services],
            seccessfullyCreatedService: true
        };
    }

    case API__CREATE_SERVICE_FAIL: {
        // let errorOperator, errorPrefix, errorCountry, errorImage;
        let errorMaxReports, errorInternalName, errorFormats, errorImage;
        let error500;
        try {
            errorMaxReports = action.payload.errors.find(o => o.property === 'maxReports');
            errorInternalName = action.payload.errors.find(o => o.property === 'internalName');
            errorFormats = action.payload.errors.find(o => o.property === 'formats');
            errorImage = action.payload.errors.find(o => o.property === 'image');

            // errorPrefix = action.payload.errors.find(o => o.property === 'prefix');
            // errorCountry = action.payload.errors.find(o => o.property === 'countryId');
            // errorImage = action.payload.errors.find(o => o.property === 'image');
        } catch (err) {
            // error500 = true;
        }

        console.log(API__CREATE_SERVICE_FAIL, action.payload);
        if (error500) {
            store.addNotification({
                title: '',
                message: 'Операция не может быть выполнена',
                type: 'danger',
                insert: 'top',
                container: 'bottom-right',
                animationIn: ['animated', 'bounceIn'],
                animationOut: ['animated', 'bounceOut'],
                dismiss: {
                    duration: 6000,
                    onScreen: false,
                    pauseOnHover: true
                }
            });
            return {
                ...state,
                seccessfullyCreatedService: false
            };
        } else {
            return {
                ...state,
                seccessfullyCreatedService: false,
                errorMaxReports,
                errorInternalName,
                errorFormats,
                errorImage
                // errorPrefix: errorPrefix,
                // errorCountry: errorCountry,
                // errorImage: errorImage
            };
        }
    }

    case API__EDIT_SERVICE_SUCCESS: {
        console.log(API__EDIT_SERVICE_SUCCESS, action.payload2);
        const services = state.services;

        store.addNotification({
            title: '',
            message: 'Оператор отредактирован!',
            type: 'success',
            insert: 'top',
            container: 'bottom-right',
            animationIn: ['animated', 'bounceIn'],
            animationOut: ['animated', 'bounceOut'],
            dismiss: {
                duration: 6000,
                onScreen: false,
                pauseOnHover: true
            }
        });

        let nameUrl;
        if (action.payload2.image) {
            nameUrl = '/images/iccid/' + action.payload2.image.name;
        }

        const joinReturnedId = { ...action.payload2, id: action.payload.id, image_path: nameUrl || null };
        const indexOfServices = services.findIndex(x => x.id === joinReturnedId.id);

        services[indexOfServices] = joinReturnedId;

        console.log('new', services);
        return {
            ...state,
            services: [...services],
            seccessfullyCreatedService: true
        };
    }

    case API__EDIT_SERVICE_FAIL: {
        let errorMaxReports, errorInternalName, errorFormats, errorImage;
        let error500;
        try {
            errorMaxReports = action.payload.errors.find(o => o.property === 'maxReports');
            errorInternalName = action.payload.errors.find(o => o.property === 'internalName');
            errorFormats = action.payload.errors.find(o => o.property === 'formats');
            errorImage = action.payload.errors.find(o => o.property === 'image');
        } catch (err) {
            error500 = true;
        }

        console.log(API__EDIT_SERVICE_FAIL, action.payload);
        if (error500) {
            store.addNotification({
                title: '',
                message: 'Операция не может быть выполнена',
                type: 'danger',
                insert: 'top',
                container: 'bottom-right',
                animationIn: ['animated', 'bounceIn'],
                animationOut: ['animated', 'bounceOut'],
                dismiss: {
                    duration: 6000,
                    onScreen: false,
                    pauseOnHover: true
                }
            });
            return {
                ...state,
                seccessfullyCreatedOperator: false
            };
        } else {
            return {
                ...state,
                seccessfullyCreatedOperator: false,
                errorMaxReports,
                errorInternalName,
                errorFormats,
                errorImage
            };
        }
    }

    case API__DELETE_SERVICE_SUCCESS: {
        console.log(API__DELETE_SERVICE_SUCCESS, action.payload2);
        const services = state.services;

        store.addNotification({
            title: '',
            message: 'Оператор удален!',
            type: 'success',
            insert: 'top',
            container: 'bottom-right',
            animationIn: ['animated', 'bounceIn'],
            animationOut: ['animated', 'bounceOut'],
            dismiss: {
                duration: 6000,
                onScreen: false,
                pauseOnHover: true
            }
        });

        const joinReturnedId = { ...action.payload2, id: action.payload.id };
        const indexOfServices = services.findIndex(x => x.id === joinReturnedId.id);
        services.splice(indexOfServices, 1);

        services.splice(indexOfServices, 1);
        // }
        console.log('selete services', services);

        return {
            ...state,
            services: [...services],
            seccessfullyCreatedService: true
        };
    }

    case API__DELETE_SERVICE_FAIL: {
        console.log(API__DELETE_SERVICE_FAIL, action.payload);
        store.addNotification({
            title: '',
            message: action.payload.error,
            type: 'danger',
            insert: 'top',
            container: 'bottom-right',
            animationIn: ['animated', 'bounceIn'],
            animationOut: ['animated', 'bounceOut'],
            dismiss: {
                duration: 6000,
                onScreen: false,
                pauseOnHover: true
            }
        });

        return {
            ...state,
            seccessfullyCreatedOperator: false
        };
    }

    case RESET_CREATE_SERVICE_FORM: {
        console.log('RESET_CREATE_SERVICE_FORM');
        return {
            ...state,
            errorOperator: '',
            errorPrefix: '',
            errorCountry: '',
            errorImage: '',
            seccessfullyCreatedService: false
        };
    }
    case ERROR_500: {
        console.log(ERROR_500);
        store.addNotification({
            title: '',
            message: action.payload.error,
            type: 'danger',
            insert: 'top',
            container: 'bottom-right',
            animationIn: ['animated', 'bounceIn'],
            animationOut: ['animated', 'bounceOut'],
            dismiss: {
                duration: 6000,
                onScreen: false,
                pauseOnHover: true
            }
        });
        return {
            ...state
        };
    }

    default:
        return state;
    }
};
export default serviceReducer;
// ================================================================================================
