import {
    SET_SIDEBAR,
    DEFINE_SIDEBAR_MENU_ITEMS
} from '../actionTypes';

const initState = {
    sidebarShow: 'responsive',
    menuItems: JSON.parse(window.localStorage.getItem('menuItems')) || [
        { name: 'menu.partners' },
        { name: 'menu.devices' },
        { name: 'menu.simcards' }
    ]
};

const navReducer = (state = initState, { type, menuItems, ...sidebarShow }) => {
    switch (type) {
    case SET_SIDEBAR:
        console.log(SET_SIDEBAR, sidebarShow);
        return {
            ...state,
            ...sidebarShow
            // sidebarShow: action.val
        };
    case DEFINE_SIDEBAR_MENU_ITEMS:
        console.log(DEFINE_SIDEBAR_MENU_ITEMS, menuItems);
        window.localStorage.setItem('menuItems', JSON.stringify(menuItems.items));
        return {
            ...state,
            menuItems: [...menuItems.items]
            // sidebarShow: action.val
        };
    default:
        return state;
    }
};
export default navReducer;
