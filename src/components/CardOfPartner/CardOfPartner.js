import React, { lazy } from 'react';
import { Link } from 'react-router-dom';

import {
    CBadge,
    CButton,
    CButtonGroup,
    CCard,
    CCardBody,
    CCardFooter,
    CCardHeader,
    CCol,
    CProgress,
    CRow,
    CCallout,
    CDataTable,
    CCollapse,
    CFormGroup,
    CLabel,
    CForm,
    CInputGroup,
    CInputGroupPrepend,
    CInputGroupText,
    CInput,
    CSwitch
} from '@coreui/react';
import { FormattedMessage } from 'react-intl';

import { usersData, licencesData } from '../../views/users/UsersData';

const CardOfPartner = () => {
    const licenceFields = [
        {
            key: 'key',
            label: 'Лиц. ключ',
            // _style: {
            //     width: '20%'
            // },
            filter: false
        },
        {
            key: 'gaven',
            label: 'Выдан',
            // _style: {
            //     width: '20%'
            // },
            filter: false
        },
        {
            key: 'devices',
            label: 'Устройства',
            // _style: {
            //     width: '20%'
            // },
            filter: false
        },
        {
            key: 'description',
            label: 'Описание',
            // _style: {
            //     width: '20%'
            // },
            filter: false
        }
    ];

    const onSavePartnerData = (event) => {
        event.preventDefault();
    };

    console.log('find id from url', usersData.find(item => item.id == window.location.href.split('/').reverse()[0]));
    const partner = usersData.find(item => item.id === +window.location.href.split('/').reverse()[0]);

    return (
        <>
            <CRow>
                <CCol sm="3">
                    <CCard>
                        <CCardHeader>
                            <CRow>
                                <CCol>
                                    <h6 id="traffic" className="card-title mb-0">{partner.name}</h6>
                                </CCol>
                            </CRow>
                        </CCardHeader>

                        <CCardBody>
                            <CRow className="text-left">
                                <CCol md="6">
                                    <CLabel>Id</CLabel>
                                </CCol>
                                <CCol xs="12" md="6">
                                    <p className="form-control-static">{partner.id}</p>
                                </CCol>
                            </CRow>

                            <CRow className="text-left">
                                <CCol md="6">
                                    <CLabel>Профиль</CLabel>
                                </CCol>
                                <CCol xs="12" md="6">
                                    <p className="form-control-static">{partner.profile}</p>
                                </CCol>
                            </CRow>

                            <CRow className="text-left">
                                <CCol md="6">
                                    <CLabel>Балланс</CLabel>
                                </CCol>
                                <CCol xs="12" md="6">
                                    <p className="form-control-static">{partner.balance}</p>
                                </CCol>
                            </CRow>

                            <CRow className="text-left">
                                <CCol md="6">
                                    <CLabel>Создан</CLabel>
                                </CCol>
                                <CCol xs="12" md="6">
                                    <p className="form-control-static">{partner.registered}</p>
                                </CCol>
                            </CRow>

                            <CRow className="text-left">
                                <CCol md="6">
                                    <CLabel>Последний вход</CLabel>
                                </CCol>
                                <CCol xs="12" md="6">
                                    <p className="form-control-static">{partner.lastEntry}</p>
                                </CCol>
                            </CRow>

                        </CCardBody>
                    </CCard>
                </CCol>
                <CCol sm="9">
                    <CCard>
                        <CCardHeader>
                            <CRow>
                                <CCol>
                                    <h6 id="traffic" className="card-title mb-0">ИЗМЕНИТЬ ДАННЫЕ</h6>
                                </CCol>
                            </CRow>
                        </CCardHeader>

                        <CCardBody>
                            <CForm onSubmit={onSavePartnerData} method="post" row>
                                <CRow className="text-left">
                                    <CCol className="p-1" md="2">
                                        <CFormGroup>
                                            <CLabel htmlFor="name">Имя</CLabel>
                                            <CInput defaultValue={partner.name} id="name" placeholder="" required />
                                        </CFormGroup>
                                    </CCol>
                                    <CCol className="p-1" md="2">
                                        <CFormGroup>
                                            <CLabel htmlFor="emale">E-mail</CLabel>
                                            <CInput defaultValue={partner.email} id="email" placeholder="" required />
                                        </CFormGroup>
                                    </CCol>
                                    <CCol className="p-1" md="2">
                                        <CFormGroup>
                                            <CLabel htmlFor="balance">Баланс</CLabel>
                                            <CInput defaultValue={partner.balance} id="balance" placeholder="" required />
                                        </CFormGroup>
                                    </CCol>
                                    <CCol className="p-1" md="2">
                                        <CFormGroup>
                                            <CLabel htmlFor="balance">Порог баланса</CLabel>
                                            <CInput defaultValue={partner.balanceThreshold} id="balance" placeholder="" required />
                                        </CFormGroup>
                                    </CCol>
                                    <CCol className="p-1" md="2">
                                        <CFormGroup>
                                            <CLabel htmlFor="profile">Профиль</CLabel>
                                            <CInput defaultValue={partner.profile} id="profile" placeholder="" required />
                                        </CFormGroup>
                                    </CCol>
                                    <CCol className="p-1" md="2">
                                        <CFormGroup>
                                            <CLabel htmlFor="password">Новый пароль</CLabel>
                                            <CInput defaultValue={''} id="password" placeholder="" />
                                        </CFormGroup>
                                    </CCol>

                                    <CFormGroup className="form-actions pl-1">
                                        <CButton type="submit" color="primary"><i className="cil-save"></i></CButton>{' '}
                                        <CButton color="warning" onClick={onSavePartnerData}><i className="cil-trash"></i></CButton>
                                    </CFormGroup>
                                </CRow>
                            </CForm>

                        </CCardBody>
                    </CCard>
                </CCol>
            </CRow>
            <CRow>
                <CCol sm="12">
                    <CCard>
                        <CCardHeader>
                            <CRow>
                                <CCol>
                                    <h6 id="traffic" className="card-title mb-0">РАЗРЕШЕНИЯ ПАРТНЕРА</h6>
                                </CCol>
                            </CRow>
                        </CCardHeader>

                        <CCardBody>
                            <CRow className="text-left">
                                <CCol className="p-3" md="4" xs="4">
                                    <CFormGroup>
                                        <CLabel htmlFor="simbank-connection">Подключения симбанков</CLabel>
                                        <br/>
                                        <CSwitch className={'mx-1'} id="simbank-connection" variant={'3d'} color={'primary'} defaultChecked onChange={(e) => console.log(e.target.checked)}/>
                                    </CFormGroup>
                                </CCol>
                                <CCol className="p-3" md="4" xs="4">
                                    <CFormGroup>
                                        <CLabel htmlFor="ussd-commands">USSD команды</CLabel>
                                        <br/>
                                        <CSwitch className={'mx-1'} id="ussd-commands" variant={'3d'} color={'primary'} defaultChecked onChange={(e) => console.log(e.target.checked)}/>
                                    </CFormGroup>
                                </CCol>
                                <CCol className="p-3" md="4" xs="4">
                                    <CFormGroup>
                                        <CLabel htmlFor="show-all-sms">Просмотр всех смс</CLabel>
                                        <br/>
                                        <CSwitch className={'mx-1'} id="show-all-sms" variant={'3d'} color={'primary'} onChange={(e) => console.log(e.target.checked)}/>
                                    </CFormGroup>
                                </CCol>
                            </CRow>
                        </CCardBody>
                    </CCard>
                </CCol>
            </CRow>
            <CRow>
                <CCol sm="6">
                    <CCard>
                        <CCardHeader>
                            <CRow>
                                <CCol>
                                    <h6 id="traffic" className="card-title mb-0">РЕКВИЗИТЫ ДЛЯ ВЫПЛАТ</h6>
                                </CCol>
                            </CRow>
                        </CCardHeader>

                        <CCardBody>
                            <CRow className="text-left">
                                <CCol className="pl-4" md="1">
                                    <CFormGroup>
                                        <CLabel style={{ paddingTop: '40px' }} htmlFor="name">1</CLabel>
                                    </CFormGroup>
                                </CCol>
                                <CCol className="p-1" md="3">
                                    <CFormGroup>
                                        <CLabel style={{ visibility: 'hidden' }} htmlFor="visa">Card</CLabel>
                                        <CInput defaultValue={'visa'} id="visa" placeholder="" required />
                                    </CFormGroup>
                                </CCol>
                                <CCol className="p-1" md="3">
                                    <CFormGroup>
                                        <CLabel htmlFor="cart-number">Номер карты</CLabel>
                                        <CInput defaultValue={'4444 4444 4444 4444'} id="cart-number" placeholder="" required />
                                    </CFormGroup>
                                </CCol>
                                <CCol className="p-1" md="3">
                                    <CFormGroup>
                                        <CLabel htmlFor="expire">Срок действия</CLabel>
                                        <CInput defaultValue={'03/22'} id="expire" placeholder="" required />
                                    </CFormGroup>
                                </CCol>
                                <CCol className="p-1" md="2">
                                    <CFormGroup>
                                        <CButton type="submit" color="warning"><i className="cil-trash"></i></CButton>{' '}
                                    </CFormGroup>
                                </CCol>
                            </CRow>

                            <CRow className="text-left">
                                <CCol className="pl-4" md="1">
                                    <CFormGroup>
                                        <CLabel style={{ paddingTop: '40px' }} htmlFor="name">2</CLabel>
                                    </CFormGroup>
                                </CCol>
                                <CCol className="p-1" md="3">
                                    <CFormGroup>
                                        <CLabel style={{ visibility: 'hidden' }} htmlFor="yandex">Card</CLabel>
                                        <CInput defaultValue={'yandex'} id="yandex" placeholder="" required />
                                    </CFormGroup>
                                </CCol>
                                <CCol className="p-1" md="6">
                                    <CFormGroup>
                                        <CLabel htmlFor="cart-number">Номер карты</CLabel>
                                        <CInput defaultValue={'4444 4444 4444 4444'} id="cart-number" placeholder="" required />
                                    </CFormGroup>
                                </CCol>
                                <CCol className="p-1" md="2">
                                    <CFormGroup>
                                        <CButton type="submit" color="warning"><i className="cil-trash"></i></CButton>{' '}
                                    </CFormGroup>
                                </CCol>
                            </CRow>

                            <CRow className="text-left">
                                <CCol className="pl-4" md="1">
                                    <CFormGroup>
                                        <CLabel style={{ paddingTop: '40px' }} htmlFor="name">3</CLabel>
                                    </CFormGroup>
                                </CCol>
                                <CCol className="p-1" md="3">
                                    <CFormGroup>
                                        <CLabel style={{ visibility: 'hidden' }} htmlFor="yandex">Card</CLabel>
                                        <CInput defaultValue={'yandex'} id="yandex" placeholder="" required />
                                    </CFormGroup>
                                </CCol>
                                <CCol className="p-1" md="6">
                                    <CFormGroup>
                                        <CLabel htmlFor="cart-number">Номер карты</CLabel>
                                        <CInput defaultValue={'4444 4444 4444 4444'} id="cart-number" placeholder="" required />
                                    </CFormGroup>
                                </CCol>
                                <CCol className="p-1" md="2">
                                    <CFormGroup>
                                        <CButton type="submit" color="warning"><i className="cil-trash"></i></CButton>{' '}
                                    </CFormGroup>
                                </CCol>
                            </CRow>
                            <CRow className="text-left">
                                <CCol className="pl-5" md="2">
                                    <CButton type="submit" color="primary"><i className="cil-save"></i></CButton>{' '}
                                </CCol>
                                <CCol className="pl-5" md="2">
                                    <CButton color="success" onClick={onSavePartnerData}><i className="cil-plus"></i></CButton>
                                </CCol>
                            </CRow>
                        </CCardBody>
                    </CCard>
                </CCol>
                <CCol sm="6">
                    <CCard>
                        <CCardHeader>
                            <CRow>
                                <CCol>
                                    <h6 id="traffic" className="card-title mb-0">ШАБЛОНЫ ДЛЯ ВЫПЛАТЫ</h6>
                                </CCol>
                            </CRow>
                        </CCardHeader>

                        <CCardBody>
                            <CRow className="text-left">
                                <CCol className="p-1" md="3">
                                    <CFormGroup>
                                        <CLabel htmlFor="bank-name">Название</CLabel>
                                        <CInput defaultValue={'Карта Сбербанк'} id="bank-name" placeholder="" required />
                                    </CFormGroup>
                                </CCol>
                                <CCol className="p-1" md="3">
                                    <CFormGroup>
                                        <CLabel htmlFor="reqv">Реквизиты</CLabel>
                                        <CInput defaultValue={'1'} id="reqv" placeholder="" required />
                                    </CFormGroup>
                                </CCol>
                                <CCol className="p-1" md="3">
                                    <CFormGroup>
                                        <CLabel htmlFor="sum">Сумма</CLabel>
                                        <CInput defaultValue={'14500'} id="sum" placeholder="" required />
                                    </CFormGroup>
                                </CCol>
                                <CCol className="p-1" md="3">
                                    <CFormGroup>
                                        <CButton type="submit" color="warning"><i className="cil-trash"></i></CButton>{' '}
                                    </CFormGroup>
                                </CCol>
                            </CRow>
                            <CRow className="text-left">
                                <CCol className="p-1" md="3">
                                    <CFormGroup>
                                        <CLabel htmlFor="bank-name">Название</CLabel>
                                        <CInput defaultValue={'Биткоин 1'} id="bank-name" placeholder="" required />
                                    </CFormGroup>
                                </CCol>
                                <CCol className="p-1" md="3">
                                    <CFormGroup>
                                        <CLabel htmlFor="reqv">Реквизиты</CLabel>
                                        <CInput defaultValue={'2'} id="reqv" placeholder="" required />
                                    </CFormGroup>
                                </CCol>
                                <CCol className="p-1" md="3">
                                    <CFormGroup>
                                        <CLabel htmlFor="sum">Сумма</CLabel>
                                        <CInput defaultValue={'8000'} id="sum" placeholder="" required />
                                    </CFormGroup>
                                </CCol>
                                <CCol className="p-1" md="3">
                                    <CFormGroup>
                                        <CButton type="submit" color="warning"><i className="cil-trash"></i></CButton>{' '}
                                    </CFormGroup>
                                </CCol>
                            </CRow>
                            <CRow className="text-left">
                                <CCol className="p-1" md="3">
                                    <CFormGroup>
                                        <CLabel htmlFor="bank-name">Название</CLabel>
                                        <CInput defaultValue={'Биткоин 2'} id="bank-name" placeholder="" required />
                                    </CFormGroup>
                                </CCol>
                                <CCol className="p-1" md="3">
                                    <CFormGroup>
                                        <CLabel htmlFor="reqv">Реквизиты</CLabel>
                                        <CInput defaultValue={'3'} id="reqv" placeholder="" required />
                                    </CFormGroup>
                                </CCol>
                                <CCol className="p-1" md="3">
                                    <CFormGroup>
                                        <CLabel htmlFor="sum">Сумма</CLabel>
                                        <CInput defaultValue={'14000'} id="sum" placeholder="" required />
                                    </CFormGroup>
                                </CCol>
                                <CCol className="p-1" md="3">
                                    <CFormGroup>
                                        <CButton type="submit" color="warning"><i className="cil-trash"></i></CButton>{' '}
                                    </CFormGroup>
                                </CCol>
                            </CRow>
                        </CCardBody>
                    </CCard>
                </CCol>
            </CRow>
            <CRow>
                <CCol sm="12">
                    <CCard>
                        <CCardHeader>
                            <CRow>
                                <CCol>
                                    <h6 id="traffic" className="card-title mb-0">ЛИЦЕНЗИИ</h6>
                                </CCol>
                            </CRow>
                        </CCardHeader>

                        <CCardBody>
                            <CRow className="text-left">
                                <CDataTable
                                    items={licencesData}
                                    fields={licenceFields}
                                    size="sm"
                                />
                            </CRow>
                            <CRow>
                                <CCol className="p-1" md="2">
                                    <CFormGroup>
                                        <CLabel htmlFor="limLicence">Лимит лицензий</CLabel>
                                        <CInput defaultValue={'1'} id="limLicence" placeholder="" required />
                                    </CFormGroup>
                                </CCol>
                            </CRow>
                            <CRow className="text-left">
                                <CCol className="pl-5" md="1">
                                    <CButton type="submit" color="primary"><i className="cil-save"></i></CButton>{' '}
                                </CCol>
                                <CCol className="pl-5" md="1">
                                    <CButton color="success" onClick={onSavePartnerData}><i className="cil-plus"></i></CButton>
                                </CCol>
                            </CRow>
                        </CCardBody>
                    </CCard>
                </CCol>
            </CRow>
        </>
    );
};

export default CardOfPartner;
