import React, { lazy } from 'react';
import {
    CBadge,
    CButton,
    CButtonGroup,
    CCard,
    CCardBody,
    CCardFooter,
    CCardHeader,
    CCol,
    CProgress,
    CRow,
    CCallout
} from '@coreui/react';
import { FormattedMessage } from 'react-intl';

import CIcon from '@coreui/icons-react';

import MainChartExample from '../../views/charts/MainChartExample.js';

const WidgetsBrand = lazy(() => import('../../views/widgets/WidgetsBrand.js'));

const SimFilters = () => {
    return (
        <>
            <CRow>
                <CCol sm="12">
                    <CCard>
                        <CCardHeader>
                            <CRow>
                                <CCol>
                                    <h6 id="traffic" className="card-title mb-0">ФИЛЬТРЫ СМС</h6>
                                </CCol>
                            </CRow>
                        </CCardHeader>

                        <CCardBody>
                            <CRow className="text-left">
                                <CCol xl className="mb-3 mb-xl-0">
                                    {/* <CButton color="primary">Добавить</CButton> */}
                                </CCol>
                            </CRow>
                            <br/>
                            <CRow className="text-left">
                                {/* <CCol xs="12" lg="3">
                                        <Select style={{ marginTop: '0px !important', zIndex: 9999 }}
                                            placeholder="Поиск..."
                                        />
                            </CCol> */}
                            </CRow>

                        </CCardBody>
                    </CCard>
                </CCol>
            </CRow>
        </>
    );
};

export default SimFilters;
