/* eslint-disable */
import React, { lazy } from 'react';
import { Link } from 'react-router-dom';
import {
    CBadge,
    CButton,
    CButtonGroup,
    CCard,
    CCardBody,
    CCardFooter,
    CCardHeader,
    CCol,
    CProgress,
    CRow,
    CCallout,
    CDataTable,
    CCollapse,
    CLabel,
    CSelect
} from '@coreui/react';
import './index.css';
import  { devicesData, typesDevicesData }  from '../../views/users/UsersData';

const getColor = status => {
    if(status == 0) return 'bg-danger';
    if(status < 15) return 'bg-warning';
    if(status >= 15) return 'bg-success';


    // switch (status) {
    // case 'Active': return 'success';
    // case 'Inactive': return 'secondary';
    // case 'Pending': return 'warning';
    // case 'Banned': return 'danger';
    // default: return 'primary';
};

const Devices = () => {

    const typesDevicesFields = [
        { 
            key: 'type', 
            label: 'Тип',
            filter: false
        },
        { 
            key: 'view', 
            label: 'Вид',
            filter: false
        },
        {
            key: 'amountSlots', 
            label: 'Количество слотов',
            filter: false
        },
        {
            key: 'amountLines', 
            label: 'Количество линий',
            filter: false
        }
    ];

    const devicesFields = [
        { 
            key: 'id', 
            label: 'ID',
            _style: { textAlign: 'center', left: 0 },
            filter: false
        },
        { 
            key: 'name', 
            label: 'Имя',
            _style: { textAlign: 'center', left: '80px' },
            filter: false
        },
        {
            key: 'partner', 
            label: 'Партнер',
            _style: { textAlign: 'center', left: '160px' },
            filter: false
        },
        {
            key: 'type', 
            label: 'Тип',
            _style: { textAlign: 'center', left: '240px' },
            filter: false
        },
        {
            key: 'redirection', 
            label: 'Переадресация',
            _style: { textAlign: 'center', left: '320px' },
            filter: false
        },
        {
            key: 'pu', 
            label: 'ПУ',
            _style: { textAlign: 'center', left: '400px' },
            filter: false
        },
        {
          key: 'simussd',
          label: 'SIM USSD',
          _style: { textAlign: 'center', left: '480px' },
          filter: false
        },
        {
          key: 'state',
          label: 'Состояние',
          _style: { textAlign: 'center', left: '560px' },
          filter: false
        },
        {
          key: 'currency',
          label: '$',
          _style: { textAlign: 'center', left: '640px' },
          filter: false
        },
        {
          key: 'action',
          label: 'Действие',
          _style: { textAlign: 'center', left: '720px' },
          filter: false
        },
        {
          key: 'vk',
          label: 'vk',
          _style: { textAlign: 'center' },
          filter: false
        },
        {
          key: 'od',
          label: 'od',
          _style: { textAlign: 'center' },
          filter: false
        },
        {
          key: 'wa',
          label: 'wa',
          _style: { textAlign: 'center' },
          filter: false
        },
        {
          key: 'vi',
          label: 'vi',
          _style: { textAlign: 'center' },
          filter: false
        },
        {
          key: 'tg',
          label: 'tg',
          _style: { textAlign: 'center' },
          filter: false
        },
        {
          key: 'we',
          label: 'we',
          _style: { textAlign: 'center' },
          filter: false
        },
        {
          key: 'go',
          label: 'go',
          _style: { textAlign: 'center' },
          filter: false
        },
        {
          key: 'av',
          label: 'av',
          _style: { textAlign: 'center' },
          filter: false
        },
        {
          key: 'fb',
          label: 'fb',
          _style: { textAlign: 'center' },
          filter: false
        },
        {
          key: 'tw',
          label: 'tw',
          _style: { textAlign: 'center' },
          filter: false
        },
        {
          key: 'in',
          label: 'in',
          _style: { textAlign: 'center' },
          filter: false
        },
        {
          key: 'ya',
          label: 'ya',
          _style: { textAlign: 'center' },
          filter: false
        },
        {
          key: 'ya2',
          label: 'ya2',
          _style: { textAlign: 'center' },
          filter: false
        },
        {
          key: 'ti',
          label: 'ti',
          _style: { textAlign: 'center' },
          filter: false
        },
        {
          key: '123',
          label: '123',
          _style: { textAlign: 'center' },
          filter: false
        },
        {
          key: 'Ska',
          label: 'Ska',
          _style: { textAlign: 'center' },
          filter: false
        },
        {
          key: 'ad',
          label: 'ad',
          _style: { textAlign: 'center' },
          filter: false
        },
        {
          key: 'ai',
          label: 'ai',
          _style: { textAlign: 'center' },
          filter: false
        },
        {
          key: 'all',
          label: 'all',
          _style: { textAlign: 'center' },
          filter: false
        },
        {
          key: 'aol',
          label: 'aol',
          _style: { textAlign: 'center' },
          filter: false
        },
        {
          key: 'beg',
          label: 'beg',
          _style: { textAlign: 'center' },
          filter: false
        },
        {
          key: 'bl',
          label: 'bl',
          _style: { textAlign: 'center' },
          filter: false
        },
        {
          key: 'bo',
          label: 'bo',
          _style: { textAlign: 'center' },
          filter: false
        },
        {
          key: 'burg',
          label: 'burg',
          _style: { textAlign: 'center' },
          filter: false
        },
        {
          key: 'Cd',
          label: 'Cd',
          _style: { textAlign: 'center' },
          filter: false
        },
        {
          key: 'ch',
          label: 'ch',
          _style: { textAlign: 'center' },
          filter: false
        }
      ];

    const onSaveDevices = (event) => {
        event.preventDefault();
    };

    return (
        <>
            <CRow>
                <CCol sm="12">
                    <CCard>
                        <CCardHeader>
                            <CRow>
                                <CCol>
                                    <h6 id="traffic" className="card-title mb-0">УСТРОЙСТВА</h6>
                                </CCol>
                            </CRow>
                        </CCardHeader>

                        <CCardBody>
                            <CRow className="text-left">
                            {/* <CFormGroup>
                                    <CLabel htmlFor="name">Name</CLabel>
                                    <CInput id="name" placeholder="Enter your name" required />
                                </CFormGroup> */}

                                <CCol xs="12" md="3">
                                    <CLabel htmlFor="select">Партнер</CLabel>
                                    <CSelect custom name="select" id="select">

                                        <option value="0">Please select</option>
                                        <option value="1">Option #1</option>
                                        <option value="2">Option #2</option>
                                        <option value="3">Option #3</option>
                                    </CSelect>
                                </CCol>
                                <CCol xs="12" md="3">
                                    <CLabel htmlFor="select">Лицензия</CLabel>
                                    <CSelect custom name="select" id="select">

                                        <option value="0">Все</option>
                                        <option value="1">Option #1</option>
                                        <option value="2">Option #2</option>
                                        <option value="3">Option #3</option>
                                    </CSelect>
                                </CCol>
                                <CCol xs="12" md="3">
                                    <CLabel htmlFor="select">Показывать сервисы</CLabel>
                                    <CSelect custom name="select" id="select">

                                        <option value="0">Please select</option>
                                        <option value="1">Option #1</option>
                                        <option value="2">Option #2</option>
                                        <option value="3">Option #3</option>
                                    </CSelect>
                                </CCol>
                            </CRow>
                            <br />
                            <CRow className="text-left devices-table">
                                <CDataTable
                                        items={devicesData}
                                        fields={devicesFields}
                                        itemsPerPage={50}
                                        hover
                                        sorter
                                        pagination
                                        scopedSlots = {{
                                            'id':
                                            (item)=>(
                                                <td style={{left: '0px'}} className='text-center'>
                                                        {item.id}
                                                </td>
                                            ),
                                            'name':
                                            (item)=>(
                                                <td style={{left: '80px'}} className='text-center'>
                                                        {item.name}
                                                </td>
                                            ),
                                            'partner':
                                            (item)=>(
                                                <td style={{left: '160px'}} className='text-center'>
                                                        {item.partner}
                                                </td>
                                            ),
                                            'type':
                                            (item)=>(
                                                <td style={{left: '240px'}} className='text-center'>
                                                        <Link to={ `/devices/${item.id}`}>{item.type}</Link>
                                                </td>
                                            ),
                                            'vk':
                                            (item)=>(
                                                <td className={getColor(item.vk) + ' text-center'}>
                                                        {item.vk}
                                                </td>
                                            ),
                                            'od':
                                            (item)=>(
                                                <td className={getColor(item.od) + ' text-center'}>
                                                        {item.od}
                                                </td>
                                            ),
                                            'wa':
                                            (item)=>(
                                                <td className={getColor(item.wa) + ' text-center'}>
                                                        {item.wa}
                                                </td>
                                            ),
                                            'vi':
                                            (item)=>(
                                                <td className={getColor(item.vi) + ' text-center'}>
                                                        {item.vi}
                                                </td>
                                            ),
                                            'tg':
                                            (item)=>(
                                                <td className={getColor(item.tg) + ' text-center'}>
                                                        {item.tg}
                                                </td>
                                            ),
                                            'we':
                                            (item)=>(
                                                <td className={getColor(item.we) + ' text-center'}>
                                                        {item.we}
                                                </td>
                                            ),
                                            'go':
                                            (item)=>(
                                                <td className={getColor(item.go) + ' text-center'}>
                                                        {item.go}
                                                </td>
                                            ),
                                            'av':
                                            (item)=>(
                                                <td className={getColor(item.av) + ' text-center'}>
                                                        {item.av}
                                                </td>
                                            ),


                                            'fb':
                                            (item)=>(
                                                <td className={getColor(item.fb) + ' text-center'}>
                                                        {item.fb}
                                                </td>
                                            ),
                                            'tw':
                                            (item)=>(
                                                <td className={getColor(item.tw) + ' text-center'}>
                                                        {item.tw}
                                                </td>
                                            ),
                                            'in':
                                            (item)=>(
                                                <td className={getColor(item.in) + ' text-center'}>
                                                        {item.in}
                                                </td>
                                            ),
                                            'ya':
                                            (item)=>(
                                                <td className={getColor(item.ya) + ' text-center'}>
                                                        {item.ya}
                                                </td>
                                            ),
                                            'ya2':
                                            (item)=>(
                                                <td className={getColor(item.ya2) + ' text-center'}>
                                                        {item.ya2}
                                                </td>
                                            ),
                                            'ti':
                                            (item)=>(
                                                <td className={getColor(item.ti) + ' text-center'}>
                                                        {item.ti}
                                                </td>
                                            ),
                                            '123':
                                            (item)=>(
                                                <td className={getColor(item.av) + ' text-center'}>
                                                        {item.av}
                                                </td>
                                            ),
                                            'Ska':
                                            (item)=>(
                                                <td className={getColor(item.Ska) + ' text-center'}>
                                                        {item.Ska}
                                                </td>
                                            ),


                                            'ad':
                                            (item)=>(
                                                <td className={getColor(item.ad) + ' text-center'}>
                                                        {item.ad}
                                                </td>
                                            ),
                                            'ai':
                                            (item)=>(
                                                <td className={getColor(item.ai) + ' text-center'}>
                                                        {item.ai}
                                                </td>
                                            ),
                                            'all':
                                            (item)=>(
                                                <td className={getColor(item.all) + ' text-center'}>
                                                        {item.all}
                                                </td>
                                            ),
                                            'aol':
                                            (item)=>(
                                                <td className={getColor(item.aol) + ' text-center'}>
                                                        {item.aol}
                                                </td>
                                            ),
                                            'beg':
                                            (item)=>(
                                                <td className={getColor(item.beg) + ' text-center'}>
                                                        {item.beg}
                                                </td>
                                            ),
                                            'bl':
                                            (item)=>(
                                                <td className={getColor(item.bl) + ' text-center'}>
                                                        {item.bl}
                                                </td>
                                            ),
                                            'bo':
                                            (item)=>(
                                                <td className={getColor(item.bo) + ' text-center'}>
                                                        {item.bo}
                                                </td>
                                            ),
                                            'burg':
                                            (item)=>(
                                                <td className={getColor(item.burg) + ' text-center'}>
                                                        {item.burg}
                                                </td>
                                            ),

                                            'Cd':
                                            (item)=>(
                                                <td className={getColor(item.Cd) + ' text-center'}>
                                                        {item.Cd}
                                                </td>
                                            ),
                                            'ch':
                                            (item)=>(
                                                <td className={getColor(item.ch) + ' text-center'}>
                                                        {item.ch}
                                                </td>
                                            ),
                                            'redirection':
                                            (item, index)=>{
                                                return (
                                                    <td style={{left: '320px'}} className="text-center">
                                                        <i style={{color: 'red', fontWeight: 'bold', fontSize: '1.1rem', cursor: 'pointer'}} className={item.redirection === 'off' ? 'cil-x': ''} onClick={()=>{console.log('redirection', item.id)}}></i>
                                                    </td>
                                                    )
                                            },
                                            'pu':
                                            (item, index)=>{
                                                return (
                                                    <td style={{left: '400px'}} className="text-center">
                                                        <i style={{color: 'red', fontWeight: 'bold', fontSize: '1.1rem', cursor: 'pointer'}} className={item.pu === 'off' ? 'cil-x': ''} onClick={()=>{console.log('pu', item.id)}}></i>
                                                    </td>
                                                    )
                                            },
                                            'simussd':
                                            (item, index)=>{
                                                return (
                                                    <td style={{left: '480px'}} className="text-center">
                                                        <i style={{color: 'red', fontWeight: 'bold', fontSize: '1.1rem', cursor: 'pointer'}} className={item.simussd === 'off' ? 'cil-x': ''} onClick={()=>{console.log('simussd', item.id)}}></i>
                                                    </td>
                                                    )
                                            },
                                            'action':
                                            (item, index)=>{
                                                return (
                                                    <td style={{left: '720px'}} className="text-center">
                                                        <i style={{color: item.action === 'off' ? 'red': 'green', fontWeight: 'bold', fontSize: '1.1rem', cursor: 'pointer'}} className={item.action === 'off' ? 'cil-ban': 'cil-circle'} onClick={()=>{console.log('action', item.id)}}></i>
                                                    </td>
                                                    )
                                            },
                                            'state':
                                            (item, index)=>{
                                                return (
                                                    <td style={{left: '560px'}} className="text-center">
                                                        {item.state}
                                                    </td>
                                                    )
                                            },
                                            'currency':
                                            (item, index)=>{
                                                return (
                                                    <td style={{left: '640px'}} className="text-center">
                                                        {item.currency}
                                                    </td>
                                                    )
                                            },
                                            
                                        }}
                                        
                                />
                            </CRow>

                        </CCardBody>
                    </CCard>
                </CCol>
            </CRow>
            <CRow>
                <CCol sm="12">
                    <CCard>
                        <CCardHeader>
                            <CRow>
                                <CCol>
                                    <h6 id="traffic" className="card-title mb-0">ТИПЫ УСТРОЙСТВ</h6>
                                </CCol>
                            </CRow>
                        </CCardHeader>

                        <CCardBody>
                            <CRow className="text-left">
                                <CDataTable
                                    items={typesDevicesData}
                                    fields={typesDevicesFields}
                                    hover
                                    sorter
                                    />
                            </CRow>
                            <CRow className="text-left">
                                <CCol className="pl-5" md="2">
                                    <CButton color="primary" onClick={onSaveDevices}><i className="cil-save"></i></CButton>{' '}
                                </CCol>
                                <CCol className="pl-5" md="2">
                                    <CButton color="success" onClick={onSaveDevices}><i className="cil-plus"></i></CButton>
                                </CCol>
                            </CRow>
                        </CCardBody>
                    </CCard>
                </CCol>
            </CRow>
        </>
    );
};

export default Devices;
