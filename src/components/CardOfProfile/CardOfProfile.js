
import React, { lazy } from 'react';
import { Link } from 'react-router-dom';

import {
    CBadge,
    CButton,
    CButtonGroup,
    CCard,
    CCardBody,
    CCardFooter,
    CCardHeader,
    CCol,
    CProgress,
    CRow,
    CCallout,
    CDataTable
} from '@coreui/react';

import { FormattedMessage } from 'react-intl';

import usersData from '../../views/users/UsersData';

const fields = ['id', 'Имя', 'Профиль', 'Балланс', 'Дата регистрации', 'Устройства всего/онлайн', ''];

const CardOfProfile = () => {
    return (
        <>
            <CRow>
                <CCol sm="5">
                    <CCard>
                        <CCardHeader>
                            <CRow>
                                <CCol>
                                    <h6 id="traffic" className="card-title mb-0">DEFAULT</h6>
                                </CCol>
                            </CRow>
                        </CCardHeader>

                        <CCardBody>
                            <CRow className="text-left">
                                <CCol xl className="mb-3 mb-xl-0">
                                    {/* <CButton color="primary">Добавить</CButton> */}
                                </CCol>
                            </CRow>
                            <br/>
                            <CRow className="text-left">
                                {/* <CCol xs="12" lg="3">
                                        <Select style={{ marginTop: '0px !important', zIndex: 9999 }}
                                            placeholder="Поиск..."
                                        />
                            </CCol> */}
                            </CRow>

                        </CCardBody>
                    </CCard>
                </CCol>
                <CCol sm="7">
                    <CCard>
                        <CCardHeader>
                            <CRow>
                                <CCol>
                                    <h6 id="traffic" className="card-title mb-0">СТРАНЫ</h6>
                                </CCol>
                            </CRow>
                        </CCardHeader>

                        <CCardBody>
                            <CRow className="text-left">
                                <CCol xl className="mb-3 mb-xl-0">
                                    {/* <CButton color="primary">Добавить</CButton> */}
                                </CCol>
                            </CRow>
                            <br/>
                            <CRow className="text-left">
                                {/* <CCol xs="12" lg="3">
                                        <Select style={{ marginTop: '0px !important', zIndex: 9999 }}
                                            placeholder="Поиск..."
                                        />
                            </CCol> */}
                            </CRow>

                        </CCardBody>
                    </CCard>
                </CCol>
            </CRow>
            <CRow>
                <CCol sm="12">
                    <CCard>
                        <CCardHeader>
                            <CRow>
                                <CCol>
                                    <h6 id="traffic" className="card-title mb-0">СЕРВИСЫ</h6>
                                </CCol>
                            </CRow>
                        </CCardHeader>

                        <CCardBody>
                            <CRow className="text-left">
                                <CCol xl className="mb-3 mb-xl-0">
                                </CCol>
                            </CRow>
                            <br/>
                            <CRow className="text-left">
                            </CRow>

                        </CCardBody>
                    </CCard>
                </CCol>
            </CRow>
        </>
    );
};

export default CardOfProfile;
