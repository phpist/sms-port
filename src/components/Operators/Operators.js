/* eslint-disable */
import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';
import { FormattedMessage } from 'react-intl';

import {
    CButton,
    CCard,
    CCardBody,
    CCardHeader,
    CCol,
    CRow,
    CLabel,
    CDataTable,
    CModal,
    CForm,
    CFormGroup,
    CInputGroup,
    CInput,
    CSwitch,
    CFormText,
    CInvalidFeedback,
    CModalHeader,
    CModalTitle,
    CModalBody,
    CModalFooter,
    CInputFile
} from '@coreui/react';
import BaseSelect from 'react-select';
import FixRequiredSelect from "./FixRequiredSelect";
import makeAnimated from 'react-select/animated';
import { getCountriesAndIccids, filterICCIDS, createICCID, editICCID, deleteICCID, resetCreateOperatorForm } from '../../store/actions/iccidActions';
import { operatorsFields } from './tableFields';
import './index.css';



const Select = props => (
    <FixRequiredSelect
    {...props}
    SelectComponent={BaseSelect}
    options={props.options || options}
    />
    );
    
    const initOperator = {
        prefix: '',
        country_id: '',
        operator: '',
        getnum_ussd: '',
        newsim_ussd: '',
        forwardable: false,
        image: null
    }
    
const Operators = () => {
    const dispatch = useDispatch();
    const animatedComponents = makeAnimated();

    const countries = useSelector(state => state.iccid.countries);
    const countriesSelect = useSelector(state => state.iccid.countriesSelect);
    const iccidsFiltered = useSelector(state => state.iccid.iccidsFiltered);
    const seccessfullyCreatedOperator = useSelector(state => state.iccid.seccessfullyCreatedOperator);
    const errorOperator = useSelector(state => state.iccid.errorOperator);
    const errorPrefix = useSelector(state => state.iccid.errorPrefix);
    const errorCountry = useSelector(state => state.iccid.errorCountry);
    const errorImage = useSelector(state => state.iccid.errorImage);
    const localizations = useSelector(state => state.locals.localizations);
    const chosenLocal = useSelector(state => state.locals.chosenLocal);

    const [openCreateOperator, setOpenCreateOperator] = useState(false);
    const [openEditOperator, setOpenEditOperator] = useState(false);
    const [small, setSmall] = useState(false)
    const [operatorData, setOperatorData] = useState(initOperator);
    const [selectCountry, setSelectCountry] = useState([]);
    const [selectFilterCountry, setSelectFilterCountry] = useState([]);
    const [editId, setEditId] = useState(null);

    useEffect(() => {
        dispatch(getCountriesAndIccids());
    }, []);

    useEffect(() => {
    }, [countries, countriesSelect, iccidsFiltered]);

    const handleOperator = ({ target: { name, value } }) => {
        setOperatorData({ ...operatorData, [name]: value });
        console.log('handleOperator', name, value);
    };

    const handleCheckboxOperator = (event) => {
        setOperatorData({ ...operatorData, forwardable: event });
    };
    
    const handleSelectCountry = (data) => {
        setOperatorData({ ...operatorData, country_id: data.value });
        setSelectCountry(data);
    }
    
    const onCreateOperator = (event) => {
        event.preventDefault();
        if(operatorData.newsim_ussd === ''){
            setOperatorData({...operatorData, newsim_ussd: null})
        }
        dispatch(createICCID(operatorData));
    };

    const onEditOperator = (event) => {
        event.preventDefault();
        if(operatorData.newsim_ussd === ''){
            setOperatorData({...operatorData, newsim_ussd: null})
        }
        console.log('edit', operatorData);
        dispatch(editICCID(operatorData, editId));
    };

    if(seccessfullyCreatedOperator){
        setOperatorData(initOperator);
        setSelectCountry([]);
        dispatch(resetCreateOperatorForm());
        setOpenCreateOperator(false);
        setOpenEditOperator(false);
        setSelectFilterCountry(countriesSelect[0]);
    }

    const onCancel = () => {
        setOperatorData(initOperator);
        setSelectCountry([]);
        dispatch(resetCreateOperatorForm());
        setEditId(null)
        setOpenCreateOperator(false);
        setOpenEditOperator(false);
    };

    const filterIccIds = (data) => {
        dispatch(filterICCIDS(data.value));
        setSelectFilterCountry(data);
    };

    const maxLengthCheck = (object) => {
        if (object.target.value.length > object.target.maxLength) {
            object.target.value = object.target.value.slice(0, object.target.maxLength)
        }
    }

    const editOperator = (id) => {
        const operator = iccidsFiltered.find(filter => filter.id === id);
        const country = countries.find(filter => filter.value === operator.country_id);

        setSelectCountry(country);
        setOperatorData({
            prefix: operator.prefix,
            country_id: operator.country_id,
            operator: operator.operator,
            getnum_ussd: operator.getnum_ussd,
            newsim_ussd: operator.newsim_ussd,
            forwardable: operator.forwardable,
            image: operator.image_path ? 'https://dev.sms-port.pro' + operator.image_path : null
        });
        setEditId(id);
    }
    
    const onDeleteOperator = () => {
        dispatch(deleteICCID(operatorData, editId));
        setSmall(false);
        setOpenEditOperator(false);
        onCancel();
    }


    const handleImg = (event) => {
        let file = event.target.files[0];
        console.log('handleImg', file);
        setOperatorData({ ...operatorData, image: file});
    }
        
    return (
        <>
            <CRow>
                <CCol sm="12">
                    <CCard>
                        <CCardHeader>
                            <CRow>
                                <CCol>
                                    <h6 id="traffic" className="card-title mb-0 full-uppercase"><FormattedMessage id="common.operators" /></h6>
                                </CCol>
                            </CRow>
                        </CCardHeader>

                        <CCardBody>
                            <CRow className="text-left">
                                <CCol xs="12" md="2" style={{ paddingLeft: '0.6rem', paddingRight: '0.6rem' }}>
                                    <CLabel htmlFor="select" className="uppercase">
                                        <FormattedMessage id="common.county" />
                                    </CLabel>
                                    <Select
                                        className="custom__select"
                                        name="select"
                                        closeMenuOnSelect={true}
                                        components={animatedComponents}
                                        placeholder="Все"
                                        value={selectFilterCountry}
                                        options={countriesSelect}
                                        onChange={filterIccIds}
                                    />
                                </CCol>
                                <CCol xs="12" md="2" style={{ paddingLeft: '0.6rem', paddingRight: '0.6rem' }}>
                                    <CLabel style={{ visibility: 'hidden' }} htmlFor="createOperator">Add</CLabel>
                                    <CButton style={{ display: 'block' }} name="createOperator" color="success" onClick={() => setOpenCreateOperator(!openCreateOperator)}><i className="cil-plus"></i></CButton>

                                    <CModal
                                        show={openCreateOperator}
                                        onClose={() => onCancel()}
                                        color="success"
                                    >
                                        <CCard>
                                            <CCardHeader>
                                                НОВЫЙ ОПЕРАТОР
                                            </CCardHeader>
                                            <CCardBody>
                                                <CForm id="onCreateOperator" action="/onCreateOperator" onSubmit={onCreateOperator}>
                                                    <CFormGroup row>
                                                        <CInputGroup>
                                                            <CCol xs="12" md="12">
                                                                <CLabel htmlFor="select" className="uppercase"><FormattedMessage id="common.operator" /></CLabel>
                                                                {errorOperator ? 
                                                                    <>
                                                                        <CInput pattern="^$|.*\S+.*" title="Empty field not allowed" invalid value={operatorData.operator} onChange={handleOperator} name="operator" id="input-operator-name" placeholder="" required />
                                                                        <CInvalidFeedback className="help-block">{errorOperator.error}</CInvalidFeedback>
                                                                    </> : 
                                                                    <> 
                                                                        <CInput pattern="^$|.*\S+.*" title="Empty field not allowed" value={operatorData.operator} onChange={handleOperator} name="operator" id="input-operator-name" placeholder="" required/>
                                                                    </>
                                                                }
                                                            </CCol>
                                                        </CInputGroup>
                                                    </CFormGroup>
                                                    <CFormGroup row>
                                                        <CInputGroup>
                                                            <CCol xs="12" md="4">
                                                                <CLabel htmlFor="select" className="uppercase"><FormattedMessage id="common.iccid_prefix" /></CLabel>
                                                                {errorPrefix ?
                                                                <>
                                                                    <CInput pattern="^$|.*\S+.*" title="Empty field not allowed" invalid value={operatorData.prefix} onChange={handleOperator} onInput={maxLengthCheck} name="prefix" id="input-operator-iccid-prefix" minLength="7" maxLength="7" placeholder="" type="number" required/>
                                                                    <CInvalidFeedback className="help-block"><FormattedMessage id="validation.iccid.prefix_already_exist" /></CInvalidFeedback>
                                                                </>
                                                                :
                                                                <>
                                                                    <CInput pattern="^$|.*\S+.*" title="Empty field not allowed" value={operatorData.prefix} onChange={handleOperator} onInput={maxLengthCheck} name="prefix" id="input-operator-iccid-prefix" minLength="7" maxLength="7" placeholder="" type="number" required/>
                                                                </>    
                                                            }
                                                            </CCol>
                                                            <CCol xs="12" md="4">
                                                                <CLabel htmlFor="select" className="uppercase"><FormattedMessage id="common.county" /></CLabel>
                                                                {errorCountry?
                                                                    <>
                                                                        <Select
                                                                            className="custom__select"
                                                                            name="select"
                                                                            value={selectCountry}
                                                                            closeMenuOnSelect={true}
                                                                            components={animatedComponents}
                                                                            placeholder="Все"
                                                                            options={countries}
                                                                            onChange={handleSelectCountry}
                                                                            name="country_id"
                                                                            required
                                                                            pattern="^$|.*\S+.*" title="Empty field not allowed"
                                                                        />
                                                                        <CFormText className="help-block"><span style={{color: '#f00'}}><FormattedMessage id="validation.country.not_found" /></span></CFormText>
                                                                    </>
                                                                    :
                                                                    <>
                                                                        <Select
                                                                            className="custom__select"
                                                                            name="select"
                                                                            value={selectCountry}
                                                                            closeMenuOnSelect={true}
                                                                            components={animatedComponents}
                                                                            placeholder="Все"
                                                                            options={countries}
                                                                            onChange={handleSelectCountry}
                                                                            name="country_id"
                                                                            required
                                                                            pattern="^$|.*\S+.*" title="Empty field not allowed"
                                                                        />
                                                                    </>    
                                                                }
                                                            </CCol>
                                                            <CCol xs="12" md="4">
                                                                <CLabel htmlFor="simbank-connection" className="uppercase"><FormattedMessage id="common.forwardable" /></CLabel>
                                                                <br/>
                                                                <CSwitch name="forwardable" id="input-operator-redirection" className={'mx-1'} variant={'3d'} color={'primary'} checked={operatorData.forwardable} onChange={(e) => handleCheckboxOperator(e.target.checked)}/>
                                                            </CCol>
                                                        </CInputGroup>
                                                    </CFormGroup>
                                                    <CFormGroup row style={{marginBottom: 0}}>
                                                        <CInputGroup>
                                                            <CCol xs="12" md="4">
                                                                <CLabel htmlFor="select" className="uppercase"><FormattedMessage id="common.ussd_number" /></CLabel>
                                                                <CInput pattern="^$|.*\S+.*" title="Empty field not allowed" value={operatorData.getnum_ussd} onChange={handleOperator} name="getnum_ussd" id="input-operator-getnum_ussd" placeholder="" required />
                                                            </CCol>
                                                            <CCol xs="12" md="4">
                                                                <CLabel htmlFor="select" className="uppercase"><FormattedMessage id="common.ussd_new_sim" /></CLabel>
                                                                <CInput pattern="^$|.*\S+.*" title="Empty field not allowed" value={operatorData.newsim_ussd} onChange={handleOperator} name="newsim_ussd" id="input-operator-newsim_ussd" placeholder="" />
                                                            </CCol>
                                                            <CCol xs="12" md="4" style={{display: 'flex', flexDirection: 'column', alignItems: 'center', justifyContent: 'space-around', height: '93px'}}>
                                                                {operatorData.image ?
                                                                    <img alt="Logo" width="50" height="50" src={typeof operatorData.image === 'object' ? URL.createObjectURL(operatorData.image) : operatorData.image}/>
                                                                    :
                                                                    <div style={{width: '50px', height: '50px'}}></div>
                                                                }
                                                                <div class="choose_file">
                                                                    <span>Choose File</span>
                                                                    <input name="Select File" type="file"  onChange={handleImg}/>
                                                                </div>
                                                            </CCol>
                                                        </CInputGroup>
                                                    </CFormGroup>
                                                    <CFormGroup row style={{marginBottom: '15px'}}>
                                                        <CInputGroup>
                                                            <CCol xs="12" md="8">
                                                                {errorImage ? 
                                                                    <CFormText className="help-block"><span style={{color: '#f00'}}>{errorImage.error}</span></CFormText>
                                                                    : null
                                                                }
                                                            </CCol>
                                                            <CCol xs="12" md="4">
                                                                <CFormText className="help-block"><span>Формат jpg, png. Размер до 200px</span></CFormText>
                                                            </CCol>
                                                        </CInputGroup>
                                                    </CFormGroup>
                                                    <CFormGroup className="form-actions pl-1">
                                                        <CButton type="submit" color="primary"><i className="cil-save"></i></CButton>{' '}
                                                        <CButton color="warning" onClick={() => onCancel()}><i className="cil-x"></i></CButton>
                                                    </CFormGroup>
                                                </CForm>
                                            </CCardBody>
                                        </CCard>
                                    </CModal>
                                </CCol>
                            </CRow>
                            <br/>
                            <CRow className="text-left">
                                <CDataTable
                                        items={iccidsFiltered}
                                        fields={operatorsFields(localizations[chosenLocal.value])}
                                        itemsPerPage={50}
                                        hover
                                        sorter
                                        pagination
                                        sorterValue={{ column: 'operator', asc: 'true' }}
                                        // noItemsView={{ noResults: 'no filtering results available', noItems: 'no items available' }}
                                        // https://coreui.io/vue/docs/components/table.html
                                        scopedSlots = {{
                                            'newsim_ussd':
                                            (item)=>(
                                                <td className='text-center align-middle'>
                                                    {item.newsim_ussd ? item.newsim_ussd : '-'}
                                                </td>
                                            ),
                                            'forwardable':
                                            (item)=>(
                                                <td className='text-center align-middle'>
                                                    {item.forwardable ? '+' : '-'}
                                                </td>
                                            ),
                                            // 'added':
                                            // (item)=>(
                                            //     <td className='text-center align-middle'>
                                            //         {item.added ? item.added : '-'}
                                            //     </td>
                                            // ),
                                            'country_id':
                                            (item)=>(
                                                <td className='text-center align-middle'>
                                                    {item.countryName}
                                                </td>
                                            ),
                                            'operator':
                                            (item)=>(
                                                <td className='text-center align-middle'>
                                                    <Link onClick={() => {editOperator(item.id); setOpenEditOperator(!openEditOperator)}}>{item.operator}</Link>
                                                </td>
                                            ),
                                            'image_path':
                                            (item)=>(
                                                <td className='text-center align-middle'>
                                                    <img alt="Logo" width="50" height="50" src={item.image_path ? 'https://dev.sms-port.pro' + item.image_path: 'https://via.placeholder.com/50/FFF/F00?text=N O   LOGO'}/>
                                                </td>
                                            ),
                                            'prefix':
                                            (item)=>(
                                                <td className='text-center align-middle'>
                                                    {item.prefix}
                                                </td>
                                            ),
                                            'getnum_ussd':
                                            (item)=>(
                                                <td className='text-center align-middle'>
                                                    {item.getnum_ussd}
                                                </td>
                                            )
                                        }}
                                />
                                <CModal
                                    show={openEditOperator}
                                    onClose={() => onCancel()}
                                    color="success"
                                >
                                    <CCard>
                                        <CCardHeader>
                                            РЕДАКТИРОВАТЬ ОПЕРАТОР
                                        </CCardHeader>
                                        <CCardBody>
                                            <CForm enctype="multipart/form-data" id="onEditOperator" action="/onEditOperator" onSubmit={onEditOperator}>
                                                <CFormGroup row>
                                                    <CInputGroup>
                                                        <CCol xs="12" md="12">
                                                            <CLabel htmlFor="select" className="uppercase"><FormattedMessage id="common.operator" /></CLabel>
                                                            {errorOperator ? 
                                                                <>
                                                                    <CInput pattern="^$|.*\S+.*" title="Empty field not allowed" invalid value={operatorData.operator} onChange={handleOperator} name="operator" id="input-operator-name" placeholder="" required/>
                                                                    <CInvalidFeedback className="help-block">{errorOperator.error}</CInvalidFeedback>
                                                                </> : 
                                                                <> 
                                                                    <CInput pattern="^$|.*\S+.*" title="Empty field not allowed" value={operatorData.operator} onChange={handleOperator} name="operator" id="input-operator-name" placeholder="" required/>
                                                                </>
                                                            }
                                                        </CCol>
                                                    </CInputGroup>
                                                </CFormGroup>
                                                <CFormGroup row>
                                                    <CInputGroup>
                                                        <CCol xs="12" md="4">
                                                            <CLabel htmlFor="select" className="uppercase"><FormattedMessage id="common.iccid_prefix" /></CLabel>
                                                            {errorPrefix ?
                                                            <>
                                                                <CInput pattern="^$|.*\S+.*" title="Empty field not allowed" invalid value={operatorData.prefix} onChange={handleOperator} onInput={maxLengthCheck} name="prefix" id="input-operator-iccid-prefix" minLength="7" maxLength="7" placeholder="" type="number" required/>
                                                                <CInvalidFeedback className="help-block"><FormattedMessage id="validation.iccid.prefix_already_exist" /></CInvalidFeedback>
                                                            </>
                                                            :
                                                            <>
                                                                <CInput pattern="^$|.*\S+.*" title="Empty field not allowed" value={operatorData.prefix} onChange={handleOperator} onInput={maxLengthCheck} name="prefix" id="input-operator-iccid-prefix" minLength="7" maxLength="7" placeholder="" type="number" required/>
                                                            </>    
                                                        }
                                                        </CCol>
                                                        <CCol xs="12" md="4">
                                                            <CLabel htmlFor="select" className="uppercase"><FormattedMessage id="common.county" /></CLabel>
                                                            {errorCountry?
                                                                <>
                                                                    <Select
                                                                        className="custom__select"
                                                                        name="select"
                                                                        value={selectCountry}
                                                                        closeMenuOnSelect={true}
                                                                        components={animatedComponents}
                                                                        placeholder="Все"
                                                                        options={countries}
                                                                        onChange={handleSelectCountry}
                                                                        name="country_id"
                                                                        required
                                                                        pattern="^$|.*\S+.*" title="Empty field not allowed"
                                                                    />
                                                                    <CFormText className="help-block"><span style={{color: '#f00'}}><FormattedMessage id="validation.country.not_found" /></span></CFormText>
                                                                </>
                                                                :
                                                                <>
                                                                    <Select
                                                                        className="custom__select"
                                                                        name="select"
                                                                        value={selectCountry}
                                                                        closeMenuOnSelect={true}
                                                                        components={animatedComponents}
                                                                        placeholder="Все"
                                                                        options={countries}
                                                                        onChange={handleSelectCountry}
                                                                        name="country_id"
                                                                        required
                                                                        pattern="^$|.*\S+.*" title="Empty field not allowed"
                                                                    />
                                                                </>    
                                                            }
                                                        </CCol>
                                                        <CCol xs="12" md="4">
                                                            <CLabel htmlFor="simbank-connection" className="uppercase"><FormattedMessage id="common.forwardable" /></CLabel>
                                                            <br/>
                                                            <CSwitch name="forwardable" id="input-operator-redirection" className={'mx-1'} variant={'3d'} color={'primary'} checked={operatorData.forwardable} onChange={(e) => handleCheckboxOperator(e.target.checked)}/>
                                                        </CCol>
                                                    </CInputGroup>
                                                </CFormGroup>
                                                <CFormGroup row style={{marginBottom: 0}}>
                                                    <CInputGroup>
                                                        <CCol xs="12" md="4">
                                                            <CLabel htmlFor="select" className="uppercase"><FormattedMessage id="common.ussd_number" /></CLabel>
                                                            <CInput pattern="^$|.*\S+.*" title="Empty field not allowed" value={operatorData.getnum_ussd} onChange={handleOperator} name="getnum_ussd" id="input-operator-getnum_ussd" placeholder="" required />
                                                        </CCol>
                                                        <CCol xs="12" md="4">
                                                            <CLabel htmlFor="select" className="uppercase"><FormattedMessage id="common.ussd_new_sim" /></CLabel>
                                                            <CInput pattern="^$|.*\S+.*" title="Empty field not allowed" value={operatorData.newsim_ussd} onChange={handleOperator} name="newsim_ussd" id="input-operator-newsim_ussd" placeholder="" />
                                                        </CCol>
                                                        <CCol xs="12" md="4" style={{display: 'flex', flexDirection: 'column', alignItems: 'center', justifyContent: 'space-around', height: '93px'}}>
                                                            {operatorData.image ?
                                                                <img alt="Logo" width="50" height="50" src={typeof operatorData.image === 'object' ? URL.createObjectURL(operatorData.image) : operatorData.image}/>
                                                                :
                                                                <img alt="Logo" width="50" height="50" src={'https://via.placeholder.com/50/FFF/F00?text=N O   LOGO'}/>
                                                            }
                                                            <div class="choose_file">
                                                                <span>Choose File</span>
                                                                <input name="Select File" type="file"  onChange={handleImg}/>
                                                            </div>
                                                        </CCol>
                                                    </CInputGroup>
                                                </CFormGroup>
                                                <CFormGroup row style={{marginBottom: '15px'}}>
                                                    <CInputGroup>
                                                        <CCol xs="12" md="8">
                                                            {errorImage ? 
                                                                <CFormText className="help-block"><span style={{color: '#f00'}}>{errorImage.error}</span></CFormText>
                                                                : null
                                                            }
                                                        </CCol>
                                                        <CCol xs="12" md="4">
                                                            <CFormText className="help-block"><span>Формат jpg, png. Размер до 200px</span></CFormText>
                                                        </CCol>
                                                    </CInputGroup>
                                                </CFormGroup>
                                                <CFormGroup className="form-actions pl-1">
                                                    <CButton type="submit" color="primary"><i className="cil-save"></i></CButton>{' '}
                                                    <CButton color="warning" onClick={() => setSmall(!small)}><i className="cil-trash"></i></CButton>
                                                </CFormGroup>
                                            </CForm>
                                        </CCardBody>
                                    </CCard>
                                </CModal>

                                <CModal 
                                    show={small} 
                                    onClose={() => setSmall(!small)}
                                    size="sm"
                                    color="danger"
                                    >
                                    <CModalHeader closeButton>
                                        <CModalTitle>Внимание!</CModalTitle>
                                    </CModalHeader>
                                    <CModalBody>
                                        Действительно удалить оператор?
                                    </CModalBody>
                                    <CModalFooter>
                                        <CButton color="danger" onClick={() => onDeleteOperator()}><i className="cil-check-alt"></i></CButton>{' '}
                                        <CButton color="secondary" onClick={() => setSmall(!small)}><i className="cil-x"></i></CButton>
                                    </CModalFooter>
                                    </CModal>
                            </CRow>
                        </CCardBody>
                    </CCard>
                </CCol>
            </CRow>
        </>
    );
};

export default Operators;
