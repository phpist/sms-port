const capitalizeFirstLetter = (str) => {
    return str[0].toUpperCase() + str.slice(1);
};

export const operatorsFields = (val) => {
    return [
        {
            key: 'operator',
            label: val && capitalizeFirstLetter(val['common.operator']),
            _style: { textAlign: 'center' },
            filter: false
            // sorter: {
            //     status: 'ascending'
            // },
            // sorterValue:
        },
        {
            key: 'country_id',
            label: val && capitalizeFirstLetter(val['common.county']),
            _style: { textAlign: 'center' },
            filter: false
        },
        {
            key: 'prefix',
            label: val && capitalizeFirstLetter(val['common.iccid_prefix']),
            _style: { textAlign: 'center' },
            filter: false
        },
        {
            key: 'getnum_ussd',
            label: val && capitalizeFirstLetter(val['common.ussd_number']),
            _style: { textAlign: 'center' },
            filter: false
        },
        {
            key: 'newsim_ussd',
            label: val && capitalizeFirstLetter(val['common.ussd_new_sim']),
            _style: { textAlign: 'center' },
            filter: false
        },
        {
            key: 'forwardable',
            label: val && capitalizeFirstLetter(val['common.forwardable']),
            _style: { textAlign: 'center' },
            filter: false
        },
        {
            key: 'image_path',
            label: val && capitalizeFirstLetter('logo'),
            _style: { textAlign: 'center' },
            filter: false
        }
        // {
        //     key: 'added',
        //     label: val && capitalizeFirstLetter(val['common.added']),
        //     _style: { textAlign: 'center' },
        //     filter: false
        // }
    ];
};
