/* eslint-disable */
import React, { useState } from 'react';
import { Link } from 'react-router-dom';

import {
    CBadge,
    CButton,
    CButtonGroup,
    CCard,
    CCardBody,
    CCardFooter,
    CCardHeader,
    CCol,
    CProgress,
    CRow,
    CCallout,
    CDataTable,
    CModal,
    CForm,
    CFormGroup,
    CInputGroup,
    CLabel,
    CFormText 
} from '@coreui/react';
import CIcon from '@coreui/icons-react';
import { FormattedMessage } from 'react-intl';

import { applicationsPaymentsFields, historyPaymentsFields } from './tableFields';

const applicationsPaymentsFieldsData = [
    {
        id: 1111,
        date: '01.01.2020',
        partner: 121511,
        partnerBalance: 1919,
        summ: 555,
        reqvizits: 'Альфа банк',
        status: 'Новая',
        action: ''
    },
    {
        id: 1112,
        date: '01.01.2020',
        partner: 121512,
        partnerBalance: 1920,
        summ: 556,
        reqvizits: 'Альфа банк',
        status: 'Новая',
        action: ''
    }
]

const historyPaymentsFieldsData = [
    {
        id: 1111,
        date: '01.01.2020',
        partner: 121511,
        partnerBalance: 1919,
        summ: 555,
        reqvizits: 'Альфа банк',
        status: 'Новая',
        action: ''
    },
    {
        id: 1112,
        date: '01.01.2020',
        partner: 121512,
        partnerBalance: 1920,
        summ: 556,
        reqvizits: 'Альфа банк',
        status: 'Новая',
        action: ''
    }
]

const Payments = () => {
    const [openPayment, setOpenPayment] = useState(false);
    const [paymentData, setPaymentData] = useState(false);


    const onCancel = () => {
        setOpenPayment(false);
    };

    const onSubmitPayment = (event) => {
        event.preventDefault();
    };

    const openPaymentCard = (id) => {
        const payment = applicationsPaymentsFieldsData.find(filter => filter.id === id);
        setPaymentData({
            id: payment.id,
            date: payment.date,
            partner: payment.partner,
            partnerBalance: payment.partnerBalance,
            summ: payment.summ,
            reqvizits: payment.reqvizits,
            status: payment.status,
            action: payment.action
        });
    }
    return (
        <>
            <CRow>
                <CCol sm="12">
                    <CCard>
                        <CCardHeader>
                            <CRow>
                                <CCol>
                                    <h6 id="traffic" className="card-title mb-0">ЗАЯВКИ НА ВЫПЛАТЫ</h6>
                                </CCol>
                            </CRow>
                        </CCardHeader>

                        <CCardBody>
                            <CRow className="text-left">
                                <CCol xl className="mb-3 mb-xl-0">
                                    <CDataTable
                                            items={applicationsPaymentsFieldsData}
                                            fields={applicationsPaymentsFields}
                                            itemsPerPage={50}
                                            hover
                                            sorter
                                            pagination
                                            sorterValue={{ column: 'operator', asc: 'true' }}
                                            // noItemsView={{ noResults: 'no filtering results available', noItems: 'no items available' }}
                                            // https://coreui.io/vue/docs/components/table.html
                                            scopedSlots = {{
                                                'id':
                                                (item)=>(
                                                    <td className='text-center align-middle'>
                                                        <Link onClick={() => {openPaymentCard(item.id); setOpenPayment(!openPayment)}}>{item.id}</Link>
                                                    </td>
                                                ),
                                                'date':
                                                (item)=>(
                                                    <td className='text-center align-middle'>
                                                        {item.date}
                                                    </td>
                                                ),
                                                'partner':
                                                (item)=>(
                                                    <td className='text-center align-middle'>
                                                        {item.partner}
                                                    </td>
                                                ),
                                                'partnerBalance':
                                                (item)=>(
                                                    <td className='text-center align-middle'>
                                                        {item.partnerBalance}
                                                    </td>
                                                ),
                                                'summ':
                                                (item)=>(
                                                    <td className='text-center align-middle'>
                                                        {item.summ}
                                                    </td>
                                                ),
                                                'reqvizits':
                                                (item)=>(
                                                    <td className='text-center align-middle'>
                                                        {item.reqvizits}
                                                    </td>
                                                ),
                                                'status':
                                                (item)=>(
                                                    <td className='text-center align-middle'>
                                                        {item.status}
                                                    </td>
                                                ),
                                                'action':
                                                (item)=>(
                                                    <td className='text-center align-middle'>
                                                        {item.action}
                                                    </td>
                                                )
                                            }}
                                    />
                                </CCol>
                            </CRow>
                        </CCardBody>
                    </CCard>
                </CCol>
            </CRow>
            <CRow>
                <CCol sm="12">
                    <CCard>
                        <CCardHeader>
                            <CRow>
                                <CCol>
                                    <h6 id="traffic" className="card-title mb-0">ИСТОРИЯ ВЫПЛАТ</h6>
                                </CCol>
                            </CRow>
                        </CCardHeader>

                        <CCardBody>
                            <CRow className="text-left">
                                <CDataTable
                                        items={historyPaymentsFieldsData}
                                        fields={applicationsPaymentsFields}
                                        itemsPerPage={50}
                                        hover
                                        sorter
                                        pagination
                                        sorterValue={{ column: 'operator', asc: 'true' }}
                                        // noItemsView={{ noResults: 'no filtering results available', noItems: 'no items available' }}
                                        // https://coreui.io/vue/docs/components/table.html
                                        scopedSlots = {{
                                            'id':
                                            (item)=>(
                                                <td className='text-center align-middle'>
                                                    <Link onClick={() => {openPaymentCard(item.id); setOpenPayment(!openPayment)}}>{item.id}</Link>
                                                </td>
                                            ),
                                            'date':
                                            (item)=>(
                                                <td className='text-center align-middle'>
                                                    {item.date}
                                                </td>
                                            ),
                                            'partner':
                                            (item)=>(
                                                <td className='text-center align-middle'>
                                                    {item.partner}
                                                </td>
                                            ),
                                            'partnerBalance':
                                            (item)=>(
                                                <td className='text-center align-middle'>
                                                    {item.partnerBalance}
                                                </td>
                                            ),
                                            'summ':
                                            (item)=>(
                                                <td className='text-center align-middle'>
                                                    {item.summ}
                                                </td>
                                            ),
                                            'reqvizits':
                                            (item)=>(
                                                <td className='text-center align-middle'>
                                                    {item.reqvizits}
                                                </td>
                                            ),
                                            'status':
                                            (item)=>(
                                                <td className='text-center align-middle'>
                                                    {item.status}
                                                </td>
                                            ),
                                            'action':
                                            (item)=>(
                                                <td className='text-center align-middle'>
                                                    {item.action}
                                                </td>
                                            )
                                        }}
                                    />
                                    <CModal
                                        show={openPayment}
                                        onClose={() => onCancel()}
                                        color="success"
                                    >
                                        <CCard>
                                            <CCardHeader>
                                                Заявка на выплату N{paymentData.id}
                                            </CCardHeader>
                                            <CCardBody>
                                                <CForm id="onCreateOperator" action="/onCreateOperator" onSubmit={onSubmitPayment}>
                                                    <CFormGroup row>
                                                        <CInputGroup>
                                                            <CCol xs="12" md="12">
                                                                <CLabel htmlFor="select" className="uppercase">Дата {paymentData.date}</CLabel>
                                                            </CCol>
                                                        </CInputGroup>
                                                    </CFormGroup>
                                                    <CFormGroup row>
                                                        <CInputGroup>
                                                            <CCol xs="12" md="4">
                                                                <CLabel htmlFor="select" className="uppercase">Партнер {paymentData.partner}</CLabel>
                                                                {/* <CInput invalid value={paymentsData.prefix} onChange={handleOperator} onInput={maxLengthCheck} name="prefix" id="input-operator-iccid-prefix" minLength="7" maxLength="7" placeholder="" type="number" required/> */}
                                                            </CCol>
                                                            <CCol xs="12" md="4">
                                                                <CLabel htmlFor="select" className="uppercase">Сума {paymentData.summ}</CLabel>
                                                                {/* <CInput invalid value={paymentsData.prefix} onChange={handleOperator} onInput={maxLengthCheck} name="prefix" id="input-operator-iccid-prefix" minLength="7" maxLength="7" placeholder="" type="number" required/> */}
                                                                
                                                            </CCol>
                                                            <CCol xs="12" md="4">
                                                                <CLabel htmlFor="simbank-connection">Баланс партнера{paymentData.partnerBalance}</CLabel>
                                                                <br/>
                                                            </CCol>
                                                        </CInputGroup>
                                                    </CFormGroup>
                                                    <CFormGroup row style={{marginBottom: 0}}>
                                                        <CInputGroup>
                                                            <CCol xs="12" md="4">
                                                                <CLabel htmlFor="select">Реквизиты {paymentData.reqvizits}</CLabel>
                                                                {/* <CInput value={operatorData.getnum_ussd} onChange={handleOperator} name="getnum_ussd" id="input-operator-getnum_ussd" placeholder="" required /> */}
                                                            </CCol>
                                                            <CCol xs="12" md="4">
                                                                <CLabel htmlFor="select">Статус {paymentData.status}</CLabel>
                                                                {/* <CInput value={operatorData.newsim_ussd} onChange={handleOperator} name="newsim_ussd" id="input-operator-newsim_ussd" placeholder="" /> */}
                                                            </CCol>
                                                        </CInputGroup>
                                                    </CFormGroup>
                                                    
                                                    <CFormGroup className="form-actions pl-1">
                                                        <CButton color="warning" onClick={() => onCancel()}><i className="cil-x"></i></CButton>
                                                    </CFormGroup>
                                                </CForm>
                                            </CCardBody>
                                        </CCard>
                                    </CModal>
                            </CRow>
                        </CCardBody>
                    </CCard>
                </CCol>
            </CRow>
        </>
    );
};

export default Payments;
