export const applicationsPaymentsFields = [
    {
        key: 'id',
        label: 'Id',
        _style: { textAlign: 'center' },
        filter: false
    },
    {
        key: 'date',
        label: 'Дата',
        _style: { textAlign: 'center' },
        filter: false
    },
    {
        key: 'partner',
        label: 'Партнер',
        _style: { textAlign: 'center' },
        filter: false
    },
    {
        key: 'partnerBalance',
        label: 'Баланс партнера',
        _style: { textAlign: 'center' },
        filter: false
    },
    {
        key: 'summ',
        label: 'Сумма',
        _style: { textAlign: 'center' },
        filter: false
    },
    {
        key: 'reqvizits',
        label: 'Реквизиты',
        _style: { textAlign: 'center' },
        filter: false
    },
    {
        key: 'status',
        label: 'Статус',
        _style: { textAlign: 'center' },
        filter: false
    },
    {
        key: 'action',
        label: 'Действие',
        _style: { textAlign: 'center' },
        filter: false
    }
];

export const historyPaymentsFields = [
    {
        key: 'id',
        label: 'Id',
        _style: { textAlign: 'center' },
        filter: false
    },
    {
        key: 'date',
        label: 'Дата',
        _style: { textAlign: 'center' },
        filter: false
    },
    {
        key: 'partner',
        label: 'Партнер',
        _style: { textAlign: 'center' },
        filter: false
    },
    {
        key: 'partnerBalance',
        label: 'Баланс партнера',
        _style: { textAlign: 'center' },
        filter: false
    },
    {
        key: 'summ',
        label: 'Сумма',
        _style: { textAlign: 'center' },
        filter: false
    },
    {
        key: 'reqvizits',
        label: 'Реквизиты',
        _style: { textAlign: 'center' },
        filter: false
    },
    {
        key: 'Status',
        label: 'Статус',
        _style: { textAlign: 'center' },
        filter: false
    },
    {
        key: 'action',
        label: 'Действие',
        _style: { textAlign: 'center' },
        filter: false
    }
];
