/* eslint-disable */
import React, { lazy, useState } from 'react';
import { Link } from 'react-router-dom';

import {
    CBadge,
    CButton,
    CButtonGroup,
    CCard,
    CCardBody,
    CCardFooter,
    CCardHeader,
    CCol,
    CProgress,
    CRow,
    CCallout,
    CDataTable,
    CCollapse
} from '@coreui/react';

import ReactTable, { useTable, useFilters, useGlobalFilter, useAsyncDebounce } from 'react-table';
// import 'react-table/react-table.css';
import Select from 'react-select';

import  { usersData }  from '../../views/users/UsersData';

import styled from 'styled-components';

// A great library for fuzzy filtering/sorting items
// import matchSorter from 'match-sorter';

// const getBadge = status => {
//     switch (status) {
//     case 'Active': return 'success';
//     case 'Inactive': return 'secondary';
//     case 'Pending': return 'warning';
//     case 'Banned': return 'danger';
//     default: return 'primary';
//     }
// };

const Partners = () => {
    
      const [details, setDetails] = useState([])
      // const [items, setItems] = useState(usersData)
    
      const toggleDetails = (index) => {
        const position = details.indexOf(index)
        let newDetails = details.slice()
        if (position !== -1) {
          newDetails.splice(position, 1)
        } else {
          newDetails = [...details, index]
        }
        setDetails(newDetails)
      }
    
    
      const fields = [
        { 
            key: 'id', 
            label: 'ID',
            _style: { 
                width: '5%'
            },
            filter: false
        },
        { 
            key: 'name', 
            label: 'Имя',
            // _style: { 
            //     width: '20%'
            // },
            filter: false
        },
        {
            key: 'profile', 
            label: 'Профиль',
            filter: false
        },
        {
            key: 'balance', 
            label: 'Балланс',
            filter: false
        },
        {
            key: 'registered', 
            label: 'Дата регистрации',
            filter: false
        },
        {
            key: 'devices', 
            label: 'Устройства всего/онлайн',
            filter: false
        },
        // 'devices',
        {
          key: 'show_details',
          label: '',
          _style: { width: '35%' },
          filter: false
        }
      ]
    
      const getBadge = (status)=>{
        switch (status) {
          case 'Active': return 'success'
          case 'Inactive': return 'secondary'
          case 'Pending': return 'warning'
          case 'Banned': return 'danger'
          default: return 'primary'
        }
      }
    return (
        <>
            <CCard>
                <CRow>
                    <CCol>
                        <CCardHeader>
                            <h6 id="traffic" className="card-title mb-0">Партнеры</h6>
                            {/* <h6 id="traffic" className="card-title mb-0 full-uppercase"><FormattedMessage id="menu.partners" /></h6> */}
                        </CCardHeader>
                    </CCol>
                </CRow>
                <CCardFooter>
                    <CRow className="text-left">
                        <CCol xl className="mb-3 mb-xl-0">
                            <CButton color="primary">Добавить</CButton>
                        </CCol>
                    </CRow>
                    <br />
                    <CRow className="text-left custom__table_partners">
                        <CCol xs="12" lg="12">
                            <CCard>
                                <CCardBody>
                                    <CDataTable
                                        className="custom__table"
                                        items={usersData}
                                        fields={fields}
                                        // columnFilter
                                        tableFilter
                                        footer
                                        itemsPerPageSelect={true}
                                        itemsPerPage={5}
                                        hover
                                        sorter
                                        pagination
                                        scopedSlots = {{
                                            'status':
                                            (item)=>(
                                                <td>
                                                <CBadge color={getBadge(item.status)}>
                                                    {item.status}
                                                </CBadge>
                                                </td>
                                            ),
                                            'name':
                                                (item, index)=>{
                                                    return (
                                                    <td className="py-2">
                                                        <Link to={ `/partners/${item.id}`}>{item.name}</Link>
                                                    </td>
                                                    )
                                                },
                                            'devices':
                                                (item, index)=>{
                                                    return (
                                                    <td className="py-2">
                                                        <Link to={ `/devices/${item.id}`}>{item.devices}</Link>
                                                    </td>
                                                    )
                                                },
                                            'show_details':
                                            (item, index)=>{
                                                return (
                                                <td className="py-2">
                                                    <CRow>
                                                            <CButton
                                                                color="primary"
                                                                variant="outline"
                                                                shape="square"
                                                                size="sm"
                                                                // onClick={()=>{toggleDetails(index)}}
                                                                >
                                                                {/* {details.includes(index) ? 'Hide' : 'Show'} */}
                                                                    Выплаты
                                                            </CButton>
                                                            <CButton
                                                                color="primary"
                                                                variant="outline"
                                                                shape="square"
                                                                size="sm"
                                                                // onClick={()=>{toggleDetails(index)}}
                                                                >
                                                                {/* {details.includes(index) ? 'Hide' : 'Show'} */}
                                                                    Прайс
                                                            </CButton>
                                                            <CButton
                                                                    color="primary"
                                                                    variant="outline"
                                                                    shape="square"
                                                                    size="sm"
                                                                    // onClick={()=>{toggleDetails(index)}}
                                                                    >
                                                                    {/* {details.includes(index) ? 'Hide' : 'Show'} */}
                                                                        Транзакции
                                                            </CButton>
                                                            <CButton
                                                                    color="primary"
                                                                    variant="outline"
                                                                    shape="square"
                                                                    size="sm"
                                                                    // onClick={()=>{toggleDetails(index)}}
                                                                    >
                                                                    {/* {details.includes(index) ? 'Hide' : 'Show'} */}
                                                                        Лицензии
                                                            </CButton>
                                                            <CButton
                                                                color="primary"
                                                                variant="outline"
                                                                shape="square"
                                                                size="sm"
                                                                // onClick={()=>{toggleDetails(index)}}
                                                                >
                                                                {/* {details.includes(index) ? 'Hide' : 'Show'} */}
                                                                    Статистика
                                                            </CButton>
                                                        
                                                    </CRow>
                                                </td>
                                                )
                                            },
                                            'details':
                                                (item, index)=>{
                                                return (
                                                <CCollapse show={details.includes(index)}>
                                                    <CCardBody>
                                                    <h4>
                                                        {item.username}
                                                    </h4>
                                                    <p className="text-muted">User since: {item.registered}</p>
                                                    <CButton size="sm" color="info">
                                                        User Settings
                                                    </CButton>
                                                    <CButton size="sm" color="danger" className="ml-1">
                                                        Delete
                                                    </CButton>
                                                    </CCardBody>
                                                </CCollapse>
                                                )
                                            }
                                        }}
                                        />
                                </CCardBody>
                            </CCard>
                        </CCol>
                    </CRow>
                </CCardFooter>
            </CCard>
        </>
    );
};

export default Partners;
