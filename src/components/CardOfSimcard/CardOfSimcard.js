/* eslint-disable */
import React, { lazy } from 'react';
import { Link } from 'react-router-dom';

import {
    CBadge,
    CButton,
    CButtonGroup,
    CCard,
    CCardBody,
    CCardFooter,
    CCardHeader,
    CCol,
    CProgress,
    CRow,
    CCallout,
    CDataTable,
    CModal,
    CForm,
    CFormGroup,
    CInputGroup,
    CInput,
    CSwitch,
    CFormText,
    CInvalidFeedback,
    CModalHeader,
    CModalTitle,
    CModalBody,
    CModalFooter,
    CInputFile,
} from '@coreui/react';
import CIcon from '@coreui/icons-react';
import { FormattedMessage } from 'react-intl';

import { simCardsData, ordersData, connectionsHistoryData } from '../../views/users/UsersData';



const cardOfsimcardsFields = [
    {
        key: 'earned',
        label: 'Заработано',
        _style: { textAlign: 'center' },
        filter: false
    },
    {
        key: 'country',
        label: 'Страна',
        _style: { textAlign: 'center' },
        filter: false
    },
    {
        key: 'operator',
        label: 'Оператор',
        _style: { textAlign: 'center' },
        filter: false
    },
    {
        key: 'number',
        label: 'Номер',
        _style: { textAlign: 'center' },
        filter: false
    },
    {
        key: 'partner',
        label: 'Партнер',
        _style: { textAlign: 'center' },
        filter: false
    },
    {
        key: 'device',
        label: 'Устройство',
        _style: { textAlign: 'center' },
        filter: false
    },
    {
        key: 'slot',
        label: 'Слот',
        _style: { textAlign: 'center' },
        filter: false
    },
    {
        key: 'added',
        label: 'Добавлена',
        _style: { textAlign: 'center' },
        filter: false
    },
    {
        key: 'response',
        label: 'Ответ',
        _style: { textAlign: 'center' },
        filter: false
    }
];

const ordersFields = [
    {
        key: 'date',
        label: 'Дата',
        _style: { textAlign: 'center' },
        filter: false
    },
    {
        key: 'id',
        label: 'ID',
        _style: { textAlign: 'center' },
        filter: false
    },
    {
        key: 'cost',
        label: 'Стоимость',
        _style: { textAlign: 'center' },
        filter: false
    },
    {
        key: 'reward',
        label: 'Вознаграждение',
        _style: { textAlign: 'center' },
        filter: false
    },
    {
        key: 'user',
        label: 'Пользователь',
        _style: { textAlign: 'center' },
        filter: false
    },
    {
        key: 'service',
        label: 'Сервис',
        _style: { textAlign: 'center' },
        filter: false
    },
    {
        key: 'country',
        label: 'Страна',
        _style: { textAlign: 'center' },
        filter: false
    },
    {
        key: 'slot',
        label: 'Слот',
        _style: { textAlign: 'center' },
        filter: false
    },
    {
        key: 'status',
        label: 'Статус',
        _style: { textAlign: 'center' },
        filter: false
    },
    {
        key: 'idSMS',
        label: 'ID смс',
        _style: { textAlign: 'center' },
        filter: false
    }
];

const connectionsHistoryFields = [
    {
        key: 'date',
        label: 'Дата',
        _style: { textAlign: 'center' },
        filter: false
    },
    {
        key: 'partner',
        label: 'Партнер',
        _style: { textAlign: 'center' },
        filter: false
    },
    {
        key: 'device',
        label: 'Устройство',
        _style: { textAlign: 'center' },
        filter: false
    },
    {
        key: 'slot',
        label: 'Слот',
        _style: { textAlign: 'center' },
        filter: false
    }
];


const CardOfSimcard = () => {
    console.log('find id from url', simCardsData.find(item => item.id == window.location.href.split('/').reverse()[0]));
    const simcard = simCardsData.find(item => item.id === +window.location.href.split('/').reverse()[0]);
    return (
        <>
            <CRow>
                <CCol sm="12">
                    <CCard>
                        <CCardHeader>
                            <CRow>
                                <CCol>
                                    <h6 id="traffic" className="card-title mb-0" style={{display: "inline"}}>{simcard.id}</h6> <span style={simcard.status === 'Онлайн' ? {color: '#fff', backgroundColor: '#0f0', display: "inline", borderRadius: '6px', fontSize: '0.6rem', padding: '1px'} : {color: '#fff', backgroundColor: '#f00', display: "inline", borderRadius: '6px', fontSize: '0.6rem', padding: '1px'}}>{simcard.status}</span>
                                    {/* <CBadge color="success" className="float-right">Success</CBadge> */}
                                </CCol>
                            </CRow>
                        </CCardHeader>

                        <CCardBody>
                            <CRow className="text-left">
                                <CCol>
                                    <CDataTable
                                            items={[simcard]}
                                            fields={cardOfsimcardsFields}
                                            hover
                                            // noItemsView={{ noResults: 'no filtering results available', noItems: 'no items available' }}
                                            // https://coreui.io/vue/docs/components/table.html
                                            scopedSlots = {{
                                                'earned':
                                                (item)=>(
                                                    <td className='text-center align-middle'>
                                                        {item.earned}
                                                    </td>
                                                ),
                                                'country':
                                                (item)=>(
                                                    <td className='text-center align-middle'>
                                                        {item.country}
                                                    </td>
                                                ),
                                                'operator':
                                                (item)=>(
                                                    <td className='text-center align-middle'>
                                                        {item.operator}
                                                    </td>
                                                ),
                                                'number':
                                                (item)=>(
                                                    <td className='text-center align-middle'>
                                                        {item.number}
                                                    </td>
                                                ),
                                                'partner':
                                                (item)=>(
                                                    <td className='text-center align-middle'>
                                                        {item.partner}
                                                    </td>
                                                ),
                                                'device':
                                                (item)=>(
                                                    <td className='text-center align-middle'>
                                                        <Link to={ `/devices/${item.device}`}>{item.device}</Link>
                                                    </td>
                                                ),
                                                'slot':
                                                (item)=>(
                                                    <td className='text-center align-middle'>
                                                        {item.slot}
                                                    </td>
                                                ),
                                                'added':
                                                (item)=>(
                                                    <td className='text-center align-middle'>
                                                        {item.added}
                                                    </td>
                                                ),
                                                'response':
                                                (item)=>(
                                                    <td className='text-center align-middle'>
                                                        {item.response}
                                                    </td>
                                                )
                                            }}
                                    />
                                </CCol>
                            </CRow>

                        </CCardBody>
                    </CCard>
                </CCol>
            </CRow>
            <CRow>
                <CCol sm="12">
                    <CCard>
                        <CCardHeader>
                            <CRow>
                                <CCol>
                                    <h6 id="traffic" className="card-title mb-0">ЗАКАЗЫ</h6>
                                </CCol>
                            </CRow>
                        </CCardHeader>

                        <CCardBody>
                            <CRow className="text-left">
                                <CCol>
                                    <CDataTable
                                        items={ordersData}
                                        fields={ordersFields}
                                        hover
                                        // noItemsView={{ noResults: 'no filtering results available', noItems: 'no items available' }}
                                        // https://coreui.io/vue/docs/components/table.html
                                        scopedSlots = {{
                                            'date':
                                            (item)=>(
                                                <td className='text-center align-middle'>
                                                    {item.date}
                                                </td>
                                            ),
                                            'id':
                                            (item)=>(
                                                <td className='text-center align-middle'>
                                                    {item.id}
                                                </td>
                                            ),
                                            'cost':
                                            (item)=>(
                                                <td className='text-center align-middle'>
                                                    {item.cost}
                                                </td>
                                            ),
                                            'reward':
                                            (item)=>(
                                                <td className='text-center align-middle'>
                                                    {item.reward}
                                                </td>
                                            ),
                                            'user':
                                            (item)=>(
                                                <td className='text-center align-middle'>
                                                    {item.user}
                                                </td>
                                            ),
                                            'service':
                                            (item)=>(
                                                <td className='text-center align-middle'>
                                                    {item.service}
                                                </td>
                                            ),
                                            'country':
                                            (item)=>(
                                                <td className='text-center align-middle'>
                                                    {item.country}
                                                </td>
                                            ),
                                            'slot':
                                            (item)=>(
                                                <td className='text-center align-middle'>
                                                    {item.slot}
                                                </td>
                                            ),
                                            'status':
                                            (item)=>(
                                                <td className='text-center align-middle'>
                                                    {item.status}
                                                </td>
                                            ),
                                            'idSMS':
                                            (item)=>(
                                                <td className='text-center align-middle'>
                                                    {item.idSMS}
                                                </td>
                                            )
                                        }}
                                />
                                </CCol>
                            </CRow>
                        </CCardBody>
                    </CCard>
                </CCol>
            </CRow>
            <CRow>
                <CCol sm="12">
                    <CCard>
                        <CCardHeader>
                            <CRow>
                                <CCol>
                                    <h6 id="traffic" className="card-title mb-0">ИСТОРИЯ ПОДКЛЮЧЕНИЙ</h6>
                                </CCol>
                            </CRow>
                        </CCardHeader>

                        <CCardBody>
                            <CRow className="text-left">
                                <CCol>
                                    <CDataTable
                                        items={connectionsHistoryData}
                                        fields={connectionsHistoryFields}
                                        hover
                                        // noItemsView={{ noResults: 'no filtering results available', noItems: 'no items available' }}
                                        // https://coreui.io/vue/docs/components/table.html
                                        scopedSlots = {{
                                            'date':
                                            (item)=>(
                                                <td className='text-center align-middle'>
                                                    {item.date}
                                                </td>
                                            ),
                                            'partner':
                                            (item)=>(
                                                <td className='text-center align-middle'>
                                                    {item.partner}
                                                </td>
                                            ),
                                            'device':
                                            (item)=>(
                                                <td className='text-center align-middle'>
                                                    <Link to={ `/devices/${item.device}`}>{item.device}</Link>
                                                </td>
                                            ),
                                            'slot':
                                            (item)=>(
                                                <td className='text-center align-middle'>
                                                    {item.slot}
                                                </td>
                                            )
                                        }}
                                />
                                </CCol>
                            </CRow>
                        </CCardBody>
                    </CCard>
                </CCol>
            </CRow>
        </>
    );
};

export default CardOfSimcard;
