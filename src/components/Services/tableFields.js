export const servicesFields = [
    {
        key: 'internal_name',
        label: 'Название',
        _style: { textAlign: 'center' },
        filter: false
    },
    {
        key: 'display_priority',
        label: 'Приоритет',
        _style: { textAlign: 'center' },
        filter: false
    },
    {
        key: 'forwardable',
        label: 'Переадресация',
        _style: { textAlign: 'center' },
        filter: false
    },
    {
        key: 'max_reports',
        label: 'Лимит репортов',
        _style: { textAlign: 'center' },
        filter: false
    },
    // {
    //     key: 'period_reset_chs',
    //     label: 'Период сброса ЧС',
    //     filter: false
    // },
    {
        key: 'used_flush_delay',
        label: 'Период использование',
        _style: { textAlign: 'center' },
        filter: false
    },
    {
        key: 'blacklist_flush_dela',
        label: 'Период сброса R',
        _style: { textAlign: 'center' },
        filter: false
    },
    {
        key: 'formats',
        label: 'Создан',
        _style: { textAlign: 'center' },
        filter: false
    }
];
