/* eslint-disable */
import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';
import { FormattedMessage } from 'react-intl';

import {
    CBadge,
    CButton,
    CButtonGroup,
    CCard,
    CCardBody,
    CCardFooter,
    CCardHeader,
    CCol,
    CProgress,
    CRow,
    CCallout,
    CLabel,
    CSelect,
    CDataTable,
    CModal,
    CForm,
    CFormGroup,
    CInputGroup,
    CInput,
    CSwitch,
    CFormText,
    CInvalidFeedback,
    CValidFeedback,
    CModalHeader,
    CModalTitle,
    CModalBody,
    CModalFooter,
    CInputFile,
    CTextarea,
} from '@coreui/react';

import { servicesFields } from './tableFields';
import { getServices, resetCreateServiceForm, createService, editServiceAction, deleteServiceAction } from '../../store/actions/serviceActions';
import BaseSelect from 'react-select';
import FixRequiredSelect from "./FixRequiredSelect";
import makeAnimated from 'react-select/animated';


// const servicesDataTable = [
//     {
//         id: 1,
//         internal_name: 'Instagram',
//         priority: 99,
//         forwardable: false,
//         lim_reports: 30,
//         period_reset_chs: 20,
//         period_usage: 2000,
//         period_reset_r: 24,
//         created: '13:02:48 12.04.2020'
//     },
//     {
//         id: 2,
//         internal_name: 'Facebook',
//         priority: 99,
//         forwardable: false,
//         lim_reports: 30,
//         period_reset_chs: 20,
//         period_usage: 2000,
//         period_reset_r: 24,
//         created: '13:02:48 12.04.2020'
//     }
// ];

const initService = {
    internal_name: '',
    display_priority: '',
    forwardable: false,
    max_reports: '',
    used_flush_delay: '',
    blacklist_flush_dela: '',
    formats: '',
    cancelPeriod: '',
    image: null,
    algoritm: ''
}

const algoritms = [
    {value: 'strict', label: 'Strict'}, 
    {value: 'plain', label: 'Plain'}
];

const Select = props => (
    <FixRequiredSelect
      {...props}
      SelectComponent={BaseSelect}
      options={props.options || options}
    />
  );

const Services = () => {
    const dispatch = useDispatch();
    const animatedComponents = makeAnimated();

    const [openCreateService, setOpenCreateService] = useState(false);
    const [openEditService, setOpenEditService] = useState(false);
    const [small, setSmall] = useState(false);
    const [serviceData, setServiceData] = useState(initService);
    const [editId, setEditId] = useState(null);
    const [selectAlgoritm, setSelectAlgoritm] = useState([]);

    const services = useSelector(state => state.service.services);
    const seccessfullyCreatedService = useSelector(state => state.service.seccessfullyCreatedService);
    const errorMaxReports = useSelector(state => state.service.errorMaxReports);
    const errorInternalName = useSelector(state => state.service.errorInternalName);
    const errorFormats = useSelector(state => state.service.errorFormats);
    const errorImage = useSelector(state => state.service.errorImage);




    useEffect(() => {
        dispatch(getServices());
    }, []);

    const handleService = ({ target: { name, value } }) => {
        setServiceData({ ...serviceData, [name]: value });
        console.log('handleOperator', name, value);
    };

    const handleCheckboxService = (event) => {
        setServiceData({ ...serviceData, forwardable: event });
    };

    const onCreateService = (event) => {
        event.preventDefault();
        console.log('edit', serviceData);
        dispatch(createService(serviceData));

        setEditId(null);

        // if(operatorData.newsim_ussd === ''){
        //     setServiceData({...serviceData, newsim_ussd: null})
        // }
        // dispatch(createICCID(serviceData));
    };

    const onEditService = (event) => {
        event.preventDefault();
        // if(operatorData.newsim_ussd === ''){
        //     setServiceData({...operatorData, newsim_ussd: null})
        // }
        console.log('edit', serviceData);
        dispatch(editServiceAction(serviceData, editId));
        // setEditId(null)
    };
    console.log('seccessfullyCreatedService', seccessfullyCreatedService)

    if(seccessfullyCreatedService){
        setServiceData(initService);
        setSelectAlgoritm([]);
        dispatch(resetCreateServiceForm());
        setEditId(null)
        setOpenCreateService(false);
        setOpenEditService(false);
    }

    const onCancel = () => {
        setServiceData(initService);
        setSelectAlgoritm([]);
        dispatch(resetCreateServiceForm());
        setEditId(null)
        setOpenCreateService(false);
        setOpenEditService(false);
    };

    const editService = (id) => {
        console.log('editService', serviceData, id);

        const service = services.find(item => item.id === id);
        const algoritm = algoritms.find(filter => filter.value === service.algoritm);
        setSelectAlgoritm(algoritm);

        setServiceData({
            internal_name: service.internal_name,
            formats: service.formats,
            forwardable: service.forwardable,
            display_priority: service.display_priority,
            used_flush_delay: service.used_flush_delay,
            blacklist_flush_dela: service.blacklist_flush_dela,
            max_reports: service.max_reports,
            // cancelPeriod: '',
            image: service.image_path ? 'https://dev.sms-port.pro' + service.image_path : null,
            algoritm: ''
        });
        setEditId(id);
    }

    const onDeleteService = () => {
        // console.log('operatorData', operatorData);
        dispatch(deleteServiceAction(serviceData, editId));
        setSmall(false);
        setOpenEditService(false);
        onCancel();
        // onCancel(!openEditService);
    }

    
    const handleSelectAlgoritm = (data) => {
        setServiceData({ ...serviceData, algoritm: data.value });
        setSelectAlgoritm(data);
    }

    const handleImg = (event) => {
        let file = event.target.files[0];
        console.log('handleImg', file);
        setServiceData({ ...serviceData, image: file});
    }


    return (
        <>
            <CRow>
                <CCol sm="12">
                    <CCard>
                        <CCardHeader>
                            <CRow>
                                <CCol>
                                    <h6 id="traffic" className="card-title mb-0">СЕРВИСЫ</h6>
                                </CCol>
                            </CRow>
                        </CCardHeader>
                        <CCardBody>
                            <CRow className="text-left">
                                <CCol xs="12" md="2" style={{ paddingLeft: '1rem', paddingRight: '0.6rem' }}>
                                    <CButton style={{ display: 'block' }} name="select" color="success" onClick={() => setOpenCreateService(!openCreateService)}><i className="cil-plus"></i></CButton>

                                    <CModal
                                        show={openCreateService}
                                        onClose={() => onCancel()}
                                        color="success"
                                    >
                                        <CCard>
                                            <CCardHeader>
                                            НОВЫЙ СЕРВИС
                                            </CCardHeader>
                                            <CCardBody>
                                                <CForm id="onCreateOperator" action="/onCreateOperator" onSubmit={onCreateService}>
                                                    <CFormGroup row>
                                                        <CInputGroup>
                                                            <CCol xs="12" md="12">
                                                                <CLabel htmlFor="input-internal_name-name">Название</CLabel>
                                                                {errorInternalName
                                                                    ? <>
                                                                        <CInput pattern="^$|.*\S+.*" title="Empty field not allowed" invalid value={serviceData.internal_name} onChange={handleService} name="internal_name" id="input-internal_name" placeholder="" required/>
                                                                        <CInvalidFeedback className="help-block">{errorInternalName.error}</CInvalidFeedback>
                                                                    </>
                                                                    : <>
                                                                        <CInput pattern="^$|.*\S+.*" title="Empty field not allowed" value={serviceData.internal_name} onChange={handleService} name="internal_name" id="input-internal_name" placeholder="" required/>
                                                                    </>
                                                                }
                                                            </CCol>
                                                        </CInputGroup>
                                                    </CFormGroup>
                                                    <CFormGroup row>
                                                        <CInputGroup>
                                                            <CCol xs="12" md="4">
                                                                <CLabel htmlFor="display_priority">Приоритет</CLabel>
                                                                <CInput pattern="^$|.*\S+.*" title="Empty field not allowed" value={serviceData.display_priority} onChange={handleService} name="display_priority" id="input-display_priority" required/>

                                                                {/* {errorPrefix
                                                                    ? <>
                                                                        <CInput invalid value={operatorData.prefix} onChange={handleOperator} onInput={maxLengthCheck} name="prefix" id="input-operator-iccid-prefix" minLength="7" maxLength="7" placeholder="" type="number" required/>
                                                                        <CInvalidFeedback className="help-block">{errorPrefix.error}</CInvalidFeedback>
                                                                    </>
                                                                    : <>
                                                                        <CInput value={operatorData.prefix} onChange={handleOperator} onInput={maxLengthCheck} name="prefix" id="input-operator-iccid-prefix" minLength="7" maxLength="7" placeholder="" type="number" required/>
                                                                    </>
                                                                } */}
                                                            </CCol>
                                                            <CCol xs="12" md="4">
                                                                <CLabel htmlFor="max_reports">Лимит репортов</CLabel>
                                                                {errorMaxReports
                                                                    ? <>
                                                                        <CInput pattern="^$|.*\S+.*" title="Empty field not allowed" invalid value={serviceData.max_reports} onChange={handleService} name="max_reports" id="input-max_reports" required/>
                                                                        <CInvalidFeedback className="help-block">{errorMaxReports.error}</CInvalidFeedback>
                                                                    </>
                                                                    : <>
                                                                        <CInput pattern="^$|.*\S+.*" title="Empty field not allowed" value={serviceData.max_reports} onChange={handleService} name="max_reports" id="input-max_reports" required/>
                                                                    </>
                                                                }
                                                            </CCol>
                                                            <CCol xs="12" md="4">
                                                                <CLabel htmlFor="input-forwardable">Переадресация</CLabel>
                                                                <br/>
                                                                <CSwitch name="forwardable" id="input-forwardable" className={'mx-1'} variant={'3d'} color={'primary'} checked={serviceData.forwardable} onChange={(e) => handleCheckboxService(e.target.checked)}/>
                                                            </CCol>
                                                        </CInputGroup>
                                                    </CFormGroup>
                                                    <CFormGroup row>
                                                        <CInputGroup>
                                                            <CCol xs="12" md="4">
                                                                <CLabel htmlFor="input-cancelPeriod">Период сброса ЧС</CLabel>
                                                                <CInput pattern="^$|.*\S+.*" title="Empty field not allowed" value={serviceData.cancelPeriod} onChange={handleService} name="cancelPeriod" id="input-cancelPeriod" placeholder="" />
                                                            </CCol>
                                                            <CCol xs="12" md="4">
                                                                <CLabel htmlFor="input-used_flush_delay">Период исп.</CLabel>
                                                                <CInput pattern="^$|.*\S+.*" title="Empty field not allowed" value={serviceData.used_flush_delay} onChange={handleService} name="used_flush_delay" id="input-used_flush_delay" placeholder=""  />
                                                            </CCol>
                                                            <CCol xs="12" md="4">
                                                                <CLabel htmlFor="input-blacklist_flush_dela">Период сброса R</CLabel>
                                                                <CInput pattern="^$|.*\S+.*" title="Empty field not allowed" value={serviceData.blacklist_flush_dela} onChange={handleService} name="blacklist_flush_dela" id="input-blacklist_flush_dela" placeholder=""  />
                                                            </CCol>
                                                            {/* <CCol xs="12" md="4">
                                                                <CLabel htmlFor="select">USSD новой сим</CLabel>
                                                                <CInput value={operatorData.newsim_ussd} onChange={handleOperator} name="newsim_ussd" id="input-operator-newsim_ussd" placeholder="" />
                                                            </CCol>
                                                            <CCol xs="12" md="4">
                                                                <CInputFile custom id="custom-file-input" onChange={handleImg}/>
                                                                <CLabel htmlFor="custom-file-input" variant="custom-file">
                                                                Логотип...
                                                                </CLabel>
                                                                <hr />
                                                                <img alt="Logo" width="50" height="50" src={operatorData.image}/>
                                                            </CCol> */}
                                                        </CInputGroup>
                                                    </CFormGroup>
                                                    <CFormGroup row>
                                                        <CCol xs="12" md="12">
                                                            <CLabel htmlFor="input-formats">Форматы</CLabel>
                                                            {errorFormats
                                                                    ? <>
                                                                        <CTextarea
                                                                            name="formats"
                                                                            id="formats"
                                                                            rows="5"
                                                                            placeholder=""
                                                                            value={serviceData.formats}
                                                                            onChange={handleService}
                                                                            invalid
                                                                            required
                                                                            pattern="^$|.*\S+.*" title="Empty field not allowed"
                                                                        />
                                                                        <CInvalidFeedback className="help-block">{errorFormats.error}</CInvalidFeedback>
                                                                    </>
                                                                    : <>
                                                                        <CTextarea
                                                                            name="formats"
                                                                            id="formats"
                                                                            rows="5"
                                                                            placeholder=""
                                                                            value={serviceData.formats}
                                                                            onChange={handleService}
                                                                            required
                                                                            pattern="^$|.*\S+.*" title="Empty field not allowed"
                                                                        />
                                                                    </>
                                                                }
                                                        </CCol>
                                                    </CFormGroup>
                                                    <CFormGroup row>
                                                        <CCol xs="12" md="4">
                                                            <CLabel htmlFor="select">Алгоритмы</CLabel>
                                                            <Select
                                                                className="custom__select"
                                                                name="select"
                                                                value={selectAlgoritm}
                                                                closeMenuOnSelect={true}
                                                                components={animatedComponents}
                                                                placeholder="Все"
                                                                options={algoritms}
                                                                onChange={handleSelectAlgoritm}
                                                                name="country_id"
                                                            />
                                                        </CCol>
                                                        <CCol xs="12" md="4" style={{display: 'flex', flexDirection: 'column', alignItems: 'center', justifyContent: 'start', height: '93px', top: '34px'}}>
                                                            <div class="choose_file">
                                                                <span>Choose File</span>
                                                                <input name="Select File" type="file"  onChange={handleImg}/>
                                                            </div>
                                                            {serviceData.image ?
                                                                <img alt="Logo" width="50" height="50" style={{marginTop: '5px'}} src={typeof serviceData.image === 'object' ? URL.createObjectURL(serviceData.image) : serviceData.image}/>
                                                                :
                                                                null
                                                            }
                                                            <CFormText className="help-block" style={{marginTop: '4px'}}><span>Формат jpg, png. Размер до 200px</span></CFormText>
                                                        </CCol>
                                                        <CCol xs="12" md="4">
                                                            {errorImage ? 
                                                                <CFormText className="help-block" style={{paddingTop: '1.7rem', fontSize: '70%'}}><span style={{color: '#f00'}}>{errorImage.error}</span></CFormText>
                                                                : null
                                                            }
                                                        </CCol>

                                                    </CFormGroup>


                                                    <CFormGroup className="form-actions pl-1">
                                                        <CButton type="submit" color="primary"><i className="cil-save"></i></CButton>{' '}
                                                        <CButton color="warning" onClick={() => onCancel()}><i className="cil-x"></i></CButton>
                                                    </CFormGroup>
                                                </CForm>
                                            </CCardBody>
                                        </CCard>
                                    </CModal>
                                </CCol>
                            </CRow>
                            <br/>
                            <CRow className="text-left">
                                <CDataTable
                                    items={services}
                                    fields={servicesFields}
                                    itemsPerPage={50}
                                    hover
                                    sorter
                                    pagination
                                    sorterValue={{ column: 'internal_name', asc: 'true' }}
                                    scopedSlots = {{
                                        'forwardable':
                                        (item) => (
                                            <td className='text-center align-middle' style={{maxWidth: '1rem'}}>
                                                {item.forwardable ? '+' : '-'}
                                            </td>
                                        ),
                                        'display_priority':
                                        (item) => (
                                            <td className='text-center align-middle' style={{maxWidth: '1rem'}}>
                                                {item.display_priority}
                                            </td>
                                        ),
                                        'internal_name':
                                        (item) => (
                                            <td className='text-center align-middle' style={{maxWidth: '1rem'}}>
                                                <Link onClick={() => {editService(item.id); setOpenEditService(!openEditService)}}>{item.internal_name}</Link>
                                            </td>
                                        ),
                                        'max_reports':
                                        (item) => (
                                            <td className='text-center align-middle' style={{maxWidth: '1rem'}}>
                                                {item.max_reports}
                                            </td>
                                        ),
                                        'used_flush_delay':
                                        (item) => (
                                            <td className='text-center align-middle' style={{maxWidth: '1rem'}}>
                                                {item.used_flush_delay}
                                            </td>
                                        ),
                                        'blacklist_flush_dela':
                                        (item) => (
                                            <td className='text-center align-middle' style={{maxWidth: '1rem'}}>
                                                {item.blacklist_flush_dela}
                                            </td>
                                        ),
                                        'formats':
                                        (item) => (
                                            <td className='text-center align-middle' style={{maxWidth: '1rem'}}>
                                                {item.formats}
                                            </td>
                                        ),
                                    
                                    }}
                                />
                                <CModal
                                    show={openEditService}
                                    onClose={() => onCancel()}
                                    color="success"
                                >
                                    <CCard>
                                        <CCardHeader>
                                            РЕДАКТИРОВАТЬ СЕРВИС
                                        </CCardHeader>
                                        <CCardBody>
                                            <CForm id="onEditOperator" action="/onEditService" onSubmit={onEditService}>
                                                <CFormGroup row>
                                                    <CInputGroup>
                                                        <CCol xs="12" md="12">
                                                            <CLabel htmlFor="input-internal_name-name">Название</CLabel>
                                                            {errorInternalName
                                                                ? <>
                                                                    <CInput pattern="^$|.*\S+.*" title="Empty field not allowed" invalid value={serviceData.internal_name} onChange={handleService} name="internal_name" id="input-internal_name" placeholder="" required/>
                                                                    <CInvalidFeedback className="help-block">{errorInternalName.error}</CInvalidFeedback>
                                                                </>
                                                                : <>
                                                                    <CInput pattern="^$|.*\S+.*" title="Empty field not allowed" value={serviceData.internal_name} onChange={handleService} name="internal_name" id="input-internal_name" placeholder="" required/>
                                                                </>
                                                            }
                                                        </CCol>
                                                    </CInputGroup>
                                                </CFormGroup>
                                                <CFormGroup row>
                                                    <CInputGroup>
                                                        <CCol xs="12" md="4">
                                                            <CLabel htmlFor="display_priority">Приоритет</CLabel>
                                                            <CInput pattern="^$|.*\S+.*" title="Empty field not allowed" value={serviceData.display_priority} onChange={handleService} name="display_priority" id="input-display_priority" required/>

                                                            {/* {errorPrefix
                                                                ? <>
                                                                    <CInput invalid value={operatorData.prefix} onChange={handleOperator} onInput={maxLengthCheck} name="prefix" id="input-operator-iccid-prefix" minLength="7" maxLength="7" placeholder="" type="number" required/>
                                                                    <CInvalidFeedback className="help-block">{errorPrefix.error}</CInvalidFeedback>
                                                                </>
                                                                : <>
                                                                    <CInput value={operatorData.prefix} onChange={handleOperator} onInput={maxLengthCheck} name="prefix" id="input-operator-iccid-prefix" minLength="7" maxLength="7" placeholder="" type="number" required/>
                                                                </>
                                                            } */}
                                                        </CCol>
                                                        <CCol xs="12" md="4">
                                                            <CLabel htmlFor="max_reports">Лимит репортов</CLabel>
                                                            {errorMaxReports
                                                                ? <>
                                                                    <CInput pattern="^$|.*\S+.*" title="Empty field not allowed" invalid value={serviceData.max_reports} onChange={handleService} name="max_reports" id="input-max_reports" required/>
                                                                    <CInvalidFeedback className="help-block">{errorMaxReports.error}</CInvalidFeedback>
                                                                </>
                                                                : <>
                                                                    <CInput pattern="^$|.*\S+.*" title="Empty field not allowed" value={serviceData.max_reports} onChange={handleService} name="max_reports" id="input-max_reports" required/>
                                                                </>
                                                            }
                                                        </CCol>
                                                        <CCol xs="12" md="4">
                                                            <CLabel htmlFor="input-forwardable">Переадресация</CLabel>
                                                            <br/>
                                                            <CSwitch name="forwardable" id="input-forwardable" className={'mx-1'} variant={'3d'} color={'primary'} checked={serviceData.forwardable} onChange={(e) => handleCheckboxService(e.target.checked)}/>
                                                        </CCol>
                                                    </CInputGroup>
                                                </CFormGroup>
                                                <CFormGroup row>
                                                    <CInputGroup>
                                                        <CCol xs="12" md="4">
                                                            <CLabel htmlFor="input-cancelPeriod">Период сброса ЧС</CLabel>
                                                            <CInput pattern="^$|.*\S+.*" title="Empty field not allowed" value={serviceData.cancelPeriod} onChange={handleService} name="cancelPeriod" id="input-cancelPeriod" placeholder="" />
                                                        </CCol>
                                                        <CCol xs="12" md="4">
                                                            <CLabel htmlFor="input-used_flush_delay">Период исп.</CLabel>
                                                            <CInput pattern="^$|.*\S+.*" title="Empty field not allowed" value={serviceData.used_flush_delay} onChange={handleService} name="used_flush_delay" id="input-used_flush_delay" placeholder=""  />
                                                        </CCol>
                                                        <CCol xs="12" md="4">
                                                            <CLabel htmlFor="input-blacklist_flush_dela">Период сброса R</CLabel>
                                                            <CInput pattern="^$|.*\S+.*" title="Empty field not allowed" value={serviceData.blacklist_flush_dela} onChange={handleService} name="blacklist_flush_dela" id="input-blacklist_flush_dela" placeholder=""  />
                                                        </CCol>
                                                        {/* <CCol xs="12" md="4">
                                                            <CLabel htmlFor="select">USSD новой сим</CLabel>
                                                            <CInput value={operatorData.newsim_ussd} onChange={handleOperator} name="newsim_ussd" id="input-operator-newsim_ussd" placeholder="" />
                                                        </CCol>
                                                        <CCol xs="12" md="4">
                                                            <CInputFile custom id="custom-file-input" onChange={handleImg}/>
                                                            <CLabel htmlFor="custom-file-input" variant="custom-file">
                                                            Логотип...
                                                            </CLabel>
                                                            <hr />
                                                            <img alt="Logo" width="50" height="50" src={operatorData.image}/>
                                                        </CCol> */}
                                                    </CInputGroup>
                                                </CFormGroup>
                                                <CFormGroup row>
                                                    <CCol xs="12" md="12">
                                                        <CLabel htmlFor="input-formats">Форматы</CLabel>
                                                        {errorFormats
                                                                ? <>
                                                                    <CTextarea
                                                                        name="formats"
                                                                        id="formats"
                                                                        rows="5"
                                                                        placeholder=""
                                                                        value={serviceData.formats}
                                                                        onChange={handleService}
                                                                        invalid
                                                                        required
                                                                        pattern="^$|.*\S+.*" title="Empty field not allowed"
                                                                    />
                                                                    <CInvalidFeedback className="help-block">{errorFormats.error}</CInvalidFeedback>
                                                                </>
                                                                : <>
                                                                    <CTextarea
                                                                        name="formats"
                                                                        id="formats"
                                                                        rows="5"
                                                                        placeholder=""
                                                                        value={serviceData.formats}
                                                                        onChange={handleService}
                                                                        required
                                                                        pattern="^$|.*\S+.*" title="Empty field not allowed"
                                                                    />
                                                                </>
                                                            }
                                                    </CCol>
                                                </CFormGroup>
                                                <CFormGroup row>
                                                    <CCol xs="12" md="4">
                                                        <CLabel htmlFor="select">Алгоритмы</CLabel>
                                                        <Select
                                                            className="custom__select"
                                                            name="select"
                                                            value={selectAlgoritm}
                                                            closeMenuOnSelect={true}
                                                            components={animatedComponents}
                                                            placeholder="Все"
                                                            options={algoritms}
                                                            onChange={handleSelectAlgoritm}
                                                            name="country_id"
                                                        />
                                                    </CCol>
                                                    <CCol xs="12" md="4" style={{display: 'flex', flexDirection: 'column', alignItems: 'center', justifyContent: 'start', height: '93px', top: '34px'}}>
                                                        <div class="choose_file">
                                                            <span>Choose File</span>
                                                            <input name="Select File" type="file"  onChange={handleImg}/>
                                                        </div>
                                                        {serviceData.image ?
                                                            <img alt="Logo" width="50" height="50" style={{marginTop: '5px'}} src={typeof serviceData.image === 'object' ? URL.createObjectURL(serviceData.image) : serviceData.image}/>
                                                            :
                                                            <img alt="Logo" width="50" height="50" style={{marginTop: '5px'}} src={'https://via.placeholder.com/50/FFF/F00?text=N O   LOGO'}/>
                                                        }
                                                        <CFormText className="help-block" style={{marginTop: '4px'}}><span>Формат jpg, png. Размер до 200px</span></CFormText>
                                                    </CCol>
                                                    <CCol xs="12" md="4">
                                                        {errorImage ? 
                                                            <CFormText className="help-block" style={{paddingTop: '1.7rem', fontSize: '70%'}}><span style={{color: '#f00'}}>{errorImage.error}</span></CFormText>
                                                            : null
                                                        }
                                                    </CCol>
                                                </CFormGroup>

                                                <CFormGroup className="form-actions pl-1">
                                                    <CButton type="submit" color="primary"><i className="cil-save"></i></CButton>{' '}
                                                    <CButton color="warning" onClick={() => setSmall(!small)}><i className="cil-trash"></i></CButton>
                                                </CFormGroup>
                                            </CForm>
                                        </CCardBody>
                                    </CCard>
                                </CModal>

                                <CModal
                                    show={small}
                                    onClose={() => setSmall(!small)}
                                    size="sm"
                                    color="danger"
                                >
                                    <CModalHeader closeButton>
                                        <CModalTitle>Внимание!</CModalTitle>
                                    </CModalHeader>
                                    <CModalBody>
                                        Действительно удалить сервис?
                                    </CModalBody>
                                    <CModalFooter>
                                        <CButton color="danger" onClick={() => onDeleteService()}><i className="cil-check-alt"></i></CButton>{' '}
                                        <CButton color="secondary" onClick={() => setSmall(!small)}><i className="cil-x"></i></CButton>
                                    </CModalFooter>
                                </CModal>
                            </CRow>
                        </CCardBody>
                    </CCard>
                </CCol>
            </CRow>
        </>
    );
};

export default Services;
