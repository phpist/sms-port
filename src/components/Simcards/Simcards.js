/* eslint-disable */
import React, { useState } from 'react';
import 'react-date-range/dist/styles.css'; // main style file
import 'react-date-range/dist/theme/default.css'; // theme css file
import { Link } from 'react-router-dom';
import { FormattedMessage } from 'react-intl';


import {
    CBadge,
    CButton,
    CButtonGroup,
    CCard,
    CCardBody,
    CCardFooter,
    CCardHeader,
    CCol,
    CProgress,
    CRow,
    CCallout,
    CLabel,
    CSelect,
    CInput,
    CDropdown,
    CDropdownToggle,
    CDropdownMenu,
    CDataTable
} from '@coreui/react';
import Select from 'react-select';
import makeAnimated from 'react-select/animated';
import CIcon from '@coreui/icons-react';
import { DateRange } from 'react-date-range';
import moment from 'moment';

import './index.css';

import { simCardsCountries, simCardsStatuses, simCardsEarned, simCardsData } from '../../views/users/UsersData';
import SimcardsChart from './SimcardsChart'; 

const animatedComponents = makeAnimated();

const Simcards = () => {
    const [state, setState] = useState([
        {
            startDate: new Date(),
            endDate: new Date(),
            key: 'selection'
        }
    ]);
    const onDateChange = (range) => {
        setState([range.selection]);
        // moment(range.selection).format('DD.MM.YYYY HH:mm');
        console.log('startDate', moment(range.selection.startDate).format('DD/MM/YYYY'));
        console.log('endDate', moment(range.selection.endDate).format('DD/MM/YYYY'));
    };

    console.log('range', state[0].startDate, state[0].endDate);

    const simcardsFields = [
        { 
            key: 'id', 
            label: 'ID сим',
            filter: false
        },
        { 
            key: 'lessTimeOnline', 
            label: 'Крайнее время онлайн',
            filter: false
        },
        {
            key: 'status', 
            label: 'Статус',
            filter: false
        },
        {
            key: 'port', 
            label: 'Порт',
            filter: false
        },
        {
            key: 'device', 
            label: 'Устройство',
            filter: false
        },
        {
            key: 'earned', 
            label: 'Заработано',
            filter: false
        },
        {
            key: 'servives', 
            label: 'Сервисы',
            filter: false
        }
    ];



    return (
        <>
            <CRow>
                <CCol sm="12">
                    <CCard>
                        <CCardHeader>
                            <CRow>
                                <CCol>
                                    <h6 id="traffic" className="card-title mb-0">СИМКАРТЫ</h6>
                                </CCol>
                            </CRow>
                        </CCardHeader>

                        <CCardBody>
                            <CRow className="text-left">
                                <CCol xs="12" md="6" xl className="mb-2 mb-xl-0" style={{ paddingLeft: '0.6rem', paddingRight: '0.6rem' }}>
                                    <CLabel htmlFor="select">Период</CLabel>
                                    <CDropdown className="m-1">
                                        <CDropdownToggle style={{width: '100%'}} name="select" color="white">
                                            <CInput style={{ backgroundColor: '#fff', padding: 0, textAlign: 'center' }} disabled value={moment(state[0].startDate).format('DD/MM/YYYY') + ' - ' + moment(state[0].endDate).format('DD/MM/YYYY')} id="cart-number" placeholder="" required />
                                            {/* {state.startDate} - {state.endDate} */}
                                        </CDropdownToggle>
                                        <CDropdownMenu>
                                            <DateRange
                                                editableDateInputs={true}
                                                onChange={item => onDateChange(item)}
                                                moveRangeOnFirstSelection={false}
                                                ranges={state}
                                            />
                                        </CDropdownMenu>
                                    </CDropdown>

                                </CCol>
                                <CCol xs="12" md="6" xl className="mb-2 mb-xl-0" style={{ paddingLeft: '0.6rem', paddingRight: '0.6rem' }}>
                                    <CLabel htmlFor="select">Страна</CLabel>
                                    <Select
                                        className="custom__select"
                                        name="select"
                                        closeMenuOnSelect={false}
                                        components={animatedComponents}
                                        isMulti
                                        placeholder="Все"
                                        options={simCardsCountries}
                                    />
                                </CCol>
                                <CCol xs="12" md="6" xl className="mb-2 mb-xl-0" style={{ paddingLeft: '0.6rem', paddingRight: '0.6rem' }}>
                                    <CLabel htmlFor="select">Устройство</CLabel>
                                    <CInput defaultValue={'0'} id="cart-number" placeholder="" required />
                                </CCol>
                                <CCol xs="12" md="6" xl className="mb-2 mb-xl-0" style={{ paddingLeft: '0.6rem', paddingRight: '0.6rem' }}>
                                    <CLabel htmlFor="select">Статус</CLabel>
                                    <Select
                                        className="custom__select"
                                        name="select"
                                        closeMenuOnSelect={true}
                                        components={animatedComponents}
                                        placeholder="Все"
                                        options={simCardsStatuses}
                                    />
                                </CCol>
                                <CCol xs="12" md="6" xl className="mb-2 mb-xl-0" style={{ paddingLeft: '0.6rem', paddingRight: '0.6rem' }}>
                                    <CLabel htmlFor="select">Заработано</CLabel>
                                    <Select
                                        className="custom__select"
                                        name="select"
                                        closeMenuOnSelect={true}
                                        components={animatedComponents}
                                        placeholder="Все"
                                        options={simCardsEarned}
                                    />
                                </CCol>
                                <CCol xs="12" md="6" xl className="mb-2 mb-xl-0" style={{ paddingLeft: '0.6rem', paddingRight: '0.6rem' }}>
                                    <CLabel style={{ visibility: 'hidden' }} htmlFor="select">Устройство</CLabel>
                                    <CButton style={{ display: 'block' }} name="select" color="primary"><i className="cil-search"></i></CButton>
                                </CCol>
                            </CRow>
                            <br/>
                            <CRow className="text-left">
                            <CDataTable
                                        items={simCardsData}
                                        fields={simcardsFields}
                                        itemsPerPage={50}
                                        hover
                                        sorter
                                        pagination
                                        scopedSlots = {{
                                            'id':
                                            (item)=>(
                                                <td className='text-center'>
                                                        <Link to={ `/simcards/${item.id}`}>{item.id}</Link>
                                                </td>
                                            ),
                                            
                                            'device':
                                            (item)=>(
                                                <td>
                                                        <Link to={ `/devices/${item.id}`}>{item.device}</Link>
                                                </td>
                                            ),
                                        }}
                                        
                                />
                            </CRow>
                        </CCardBody>
                    </CCard>
                </CCol>
            </CRow>
            <CRow>
                <CCol sm="12">
                    <CCard>
                        <CCardHeader>
                            <CRow>
                                <CCol>
                                    <h6 id="traffic" className="card-title mb-0">СТАТИСТИКА</h6>
                                </CCol>
                            </CRow>
                        </CCardHeader>

                        <CCardBody>
                            <CRow className="text-left">
                                <SimcardsChart />
                            </CRow>
                        </CCardBody>
                    </CCard>
                </CCol>
            </CRow>
        </>
    );
};

export default Simcards;
