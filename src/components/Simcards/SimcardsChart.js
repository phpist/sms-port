import React, { useState } from 'react';
import 'react-date-range/dist/styles.css'; // main style file
import 'react-date-range/dist/theme/default.css'; // theme css file
import { Link } from 'react-router-dom';

import {
    CBadge,
    CButton,
    CButtonGroup,
    CCard,
    CCardBody,
    CCardFooter,
    CCardHeader,
    CCol,
    CProgress,
    CRow,
    CCallout,
    CLabel,
    CSelect,
    CInput,
    CDropdown,
    CDropdownToggle,
    CDropdownMenu,
    CDataTable
} from '@coreui/react';
import { Chart } from 'react-charts';

import { simCardsCountries, simCardsStatuses, simCardsEarned, simCardsData } from '../../views/users/UsersData';

const SimcardsChart = () => {
    const data = [
        {
            label: 'Series 1',
            data: [[0, 1], [1, 2], [2, 4], [3, 2], [4, 7], [5, 1], [6, 2], [7, 4], [8, 2], [9, 7]]
        },
        {
            label: 'Series 2',
            data: [[0, 3], [1, 1], [2, 5], [3, 6], [4, 4], [5, 8], [6, 7], [7, 1], [8, 3], [9, 5]]
        }
    ];

    const axes = [
        { primary: true, type: 'linear', position: 'bottom', show: 'secondaryAxisShow' },
        { type: 'linear', position: 'left', show: 'secondaryAxisShow' }
    ];

    return (
        <div
            style={{
                width: '100%',
                height: '300px'
            }}
        >
            <Chart data={data} axes={axes} tooltip />
        </div>
    );
};

export default SimcardsChart;
