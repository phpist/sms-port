/* eslint-disable */
import React, { lazy } from 'react';
import { Link } from 'react-router-dom';

import {
    CBadge,
    CButton,
    CButtonGroup,
    CCard,
    CCardBody,
    CCardFooter,
    CCardHeader,
    CCol,
    CProgress,
    CRow,
    CCallout,
    CDataTable,
    CLabel,
    CForm,
    CFormGroup,
    CInput,
    CSelect
} from '@coreui/react';
import './index.css';
import { FormattedMessage } from 'react-intl';


import { devicesData, servicesMapData, ordersData, smsData, monitoringPortsData } from '../../views/users/UsersData';

// const fields = ['id', 'Имя', 'Профиль', 'Балланс', 'Дата регистрации', 'Устройства всего/онлайн', ''];


const getColor = status => {
    if(status === 'M') return 'bg-secondary';
    if(status === 'R') return 'bg-warning';
    if(status === 'Y') return 'bg-success';
};

const CardOfDevice = () => {
    const device = devicesData.find(item => item.id === +window.location.href.split('/').reverse()[0]);

    const servicesMapFields = [
        {
            key: 'port',
            label: 'Порт',
            _style: { textAlign: 'center', left: 0 },
            filter: false
        },
        {
            key: 'number',
            label: 'Номер',
            _style: { textAlign: 'center', left: '100px' },
            filter: false
        },
        {
            key: 'operator',
            label: 'Оператор',
            _style: { textAlign: 'center', left: '200px' },
            filter: false
        },
        {
            key: 'currency',
            label: '$',
            _style: { textAlign: 'center', left: '300px' },
            filter: false
        },
        {
            key: 'vkontakte',
            label: 'vkontakte',
            _style: { textAlign: 'center' },
            filter: false
        },
        {
            key: 'odnoklassniki',
            label: 'odnoklassniki',
            _style: { textAlign: 'center'},
            filter: false
        },
        {
            key: 'whatsapp',
            label: 'whatsapp',
            _style: { textAlign: 'center'},
            filter: false
        },
        {
            key: 'viber',
            label: 'viber',
            _style: { textAlign: 'center'},
            filter: false
        },
        {
            key: 'telegram',
            label: 'telegram',
            _style: { textAlign: 'center'},
            filter: false
        },
        {
            key: 'wechat',
            label: 'wechat',
            _style: { textAlign: 'center'},
            filter: false
        },
        {
            key: 'google',
            label: 'google',
            _style: { textAlign: 'center' },
            filter: false
        },
        {
            key: 'avito',
            label: 'avito',
            _style: { textAlign: 'center' },
            filter: false
        },
        {
            key: 'facebook',
            label: 'facebook',
            _style: { textAlign: 'center' },
            filter: false
        },
        {
            key: 'twitter',
            label: 'twitter',
            _style: { textAlign: 'center' },
            filter: false
        },
        {
            key: 'instagram',
            label: 'instagram',
            _style: { textAlign: 'center' },
            filter: false
        },
        {
            key: 'yashas',
            label: 'yashas',
            _style: { textAlign: 'center' },
            filter: false
        },
        {
            key: 'yandex',
            label: 'yandex',
            _style: { textAlign: 'center' },
            filter: false
        },
        {
            key: 'tinder',
            label: 'tinder',
            _style: { textAlign: 'center' },
            filter: false
        },
        {
            key: 's123',
            label: '123',
            _style: { textAlign: 'center' },
            filter: false
        },
        {
            key: 'Skaru',
            label: 'Skaru',
            _style: { textAlign: 'center' },
            filter: false
        },
        {
            key: 'addrs',
            label: 'addrs',
            _style: { textAlign: 'center' },
            filter: false
        },
        {
            key: 'Airbnb',
            label: 'Airbnb',
            _style: { textAlign: 'center' },
            filter: false
        },
        {
            key: 'Alexgro',
            label: 'Alexgro',
            _style: { textAlign: 'center' },
            filter: false
        },
        {
            key: 'aol',
            label: 'aol',
            _style: { textAlign: 'center' },
            filter: false
        },
        {
            key: 'beget',
            label: 'beget',
            _style: { textAlign: 'center' },
            filter: false
        },
        {
            key: 'bizzard',
            label: 'bizzard',
            _style: { textAlign: 'center' },
            filter: false
        },
        {
            key: 'Bolt',
            label: 'Bolt',
            _style: { textAlign: 'center' },
            filter: false
        },
        {
            key: 'Burger_king',
            label: 'Burger_king',
            _style: { textAlign: 'center' },
            filter: false
        },
        {
            key: 'Cdkeyscom',
            label: 'Cdkeyscom',
            _style: { textAlign: 'center' },
            filter: false
        },
        {
            key: 'Chaumarp',
            label: 'Chaumarp',
            _style: { textAlign: 'center' },
            filter: false
        }
    ];

    const ordersFields = [
        {
            key: 'date',
            label: 'Дата',
            _style: { textAlign: 'center' },
            filter: false 
        },
        {
            key: 'id',
            label: 'ID',
            _style: { textAlign: 'center' },
            filter: false 
        },
        {
            key: 'user',
            label: 'Пользователь',
            _style: { textAlign: 'center' },
            filter: false 
        },
        {
            key: 'service',
            label: 'Сервис',
            _style: { textAlign: 'center' },
            filter: false 
        },
        {
            key: 'country',
            label: 'Страна',
            _style: { textAlign: 'center' },
            filter: false 
        },
        {
            key: 'slot',
            label: 'Слот',
            _style: { textAlign: 'center' },
            filter: false 
        },
        {
            key: 'status',
            label: 'Статус',
            _style: { textAlign: 'center' },
            filter: false 
        },
        {
            key: 'idSMS',
            label: 'ID смс',
            _style: { textAlign: 'center' },
            filter: false 
        }
    ]

    const smsFields = [
        {
            key: 'deliveryRreceiveryDate',
            label: 'Дата доставки / получения',
            _style: { textAlign: 'center' },
            filter: false 
        },
        {
            key: 'id',
            label: 'ID',
            _style: { textAlign: 'center' },
            filter: false 
        },
        {
            key: 'slot',
            label: 'Слот',
            _style: { textAlign: 'center' },
            filter: false 
        },
        {
            key: 'sender',
            label: 'Отправитель',
            _style: { textAlign: 'center' },
            filter: false 
        },
        {
            key: 'text',
            label: 'Текст',
            _style: { textAlign: 'center' },
            filter: false 
        },
        {
            key: 'orderId',
            label: 'ID заказа',
            _style: { textAlign: 'center' },
            filter: false 
        },
    ];

    const monitoringPortsFields = [
        {
            key: 'port',
            label: 'Порт',
            _style: { textAlign: 'left' },
            filter: false 
        },
        {
            key: 'status',
            label: 'Статус',
            _style: { textAlign: 'left' },
            filter: false 
        },
        {
            key: 'command',
            label: 'Команда',
            _style: { textAlign: 'left' },
            filter: false 
        },
        {
            key: 'sendingDate',
            label: 'Дата отправки',
            _style: { textAlign: 'left' },
            filter: false 
        },
        {
            key: 'responseDate',
            label: 'Дата ответа устройства',
            _style: { textAlign: 'left' },
            filter: false 
        },
    ]

    const onSaveDeviceData = (event) => {
        event.preventDefault();
    };

    return (
        <>
            <CRow>
                <CCol sm="3">
                    <CCard>
                        <CCardHeader>
                            <CRow>
                                <CCol>
                                    <h6 id="traffic" className="card-title mb-0">МОЙ {device.type}</h6>
                                </CCol>
                            </CRow>
                        </CCardHeader>

                        <CCardBody>
                            <CRow className="text-left">
                                <CCol md="6">
                                    <CLabel>Id</CLabel>
                                </CCol>
                                <CCol xs="12" md="6">
                                    <p className="form-control-static">{device.id}</p>
                                </CCol>
                            </CRow>

                            <CRow className="text-left">
                                <CCol md="6">
                                    <CLabel>Тип</CLabel>
                                </CCol>
                                <CCol xs="12" md="6">
                                    <p className="form-control-static">{device.type}</p>
                                </CCol>
                            </CRow>

                            <CRow className="text-left">
                                <CCol md="6">
                                    <CLabel>Партнер</CLabel>
                                </CCol>
                                <CCol xs="12" md="6">
                                    <p className="form-control-static">{device.partner}</p>
                                </CCol>
                            </CRow>

                            <CRow className="text-left">
                                <CCol md="6">
                                    <CLabel>Лиценция</CLabel>
                                </CCol>
                                <CCol xs="12" md="6">
                                    <p className="form-control-static">{device.deviceLicense}</p>
                                </CCol>
                            </CRow>

                            <CRow className="text-left">
                                <CCol md="6">
                                    <CLabel>Создан</CLabel>
                                </CCol>
                                <CCol xs="12" md="6">
                                    <p className="form-control-static">{device.deviceCreated}</p>
                                </CCol>
                            </CRow>
                            <CRow className="text-left">
                                <CCol md="6">
                                    <CLabel>Дата</CLabel>
                                </CCol>
                                <CCol xs="12" md="6">
                                    <p className="form-control-static">{device.date}</p>
                                </CCol>
                            </CRow>
                        </CCardBody>
                    </CCard>
                </CCol>
                <CCol sm="9">
                    <CCard>
                        <CCardHeader>
                            <CRow>
                                <CCol>
                                    <h6 id="traffic" className="card-title mb-0">ИЗМЕНИТЬ ДАННЫЕ</h6>
                                </CCol>
                            </CRow>
                        </CCardHeader>

                        <CCardBody>
                            <CForm onSubmit={onSaveDeviceData} method="post" row>
                                <CRow className="text-left">
                                    <CCol className="p-1" md="6">
                                        <CFormGroup>
                                            <CLabel htmlFor="name">Лицензия</CLabel>
                                            <CInput defaultValue={device.deviceLicense} id="name" placeholder="" required />
                                        </CFormGroup>
                                    </CCol>
                                    <CCol className="p-1" md="3">
                                        <CFormGroup>
                                            <CLabel htmlFor="emale">Тип</CLabel>
                                            <CInput defaultValue={device.type} id="email" placeholder="" required />
                                        </CFormGroup>
                                    </CCol>
                                    <CCol className="p-1" md="3">
                                        <CFormGroup>
                                            <CLabel htmlFor="balance">Симбанк</CLabel>
                                            <CInput defaultValue={device.simbank} id="balance" placeholder="" required />
                                        </CFormGroup>
                                    </CCol>
                                </CRow>
                                <CRow className="text-left">
                                    <CCol className="p-1" md="3">
                                        <CFormGroup>
                                            <CLabel htmlFor="name">Переадресация</CLabel>
                                            <CInput defaultValue={device.redirection} id="name" placeholder="" required />
                                        </CFormGroup>
                                    </CCol>
                                    <CCol className="p-1" md="3">
                                        <CFormGroup>
                                            <CLabel htmlFor="emale">Партнер</CLabel>
                                            <CInput defaultValue={device.partner} id="email" placeholder="" required />
                                        </CFormGroup>
                                    </CCol>
                                    <CCol className="p-1" md="3">
                                        <CFormGroup>
                                            <CLabel htmlFor="balance">Диапазон слотов</CLabel>
                                            <CInput defaultValue={device.slotsRange} id="balance" placeholder="" required />
                                        </CFormGroup>
                                    </CCol>
                                    <CCol className="p-1" md="3">
                                        <CFormGroup>
                                            <CLabel htmlFor="balance">Лицензия</CLabel>
                                            <CInput defaultValue={device.deviceLicense} id="balance" placeholder="" required />
                                        </CFormGroup>
                                    </CCol>
                                </CRow>
                                <CRow className="text-left">
                                    <CFormGroup className="form-actions pl-1">
                                        <CButton type="submit" color="primary"><i className="cil-save"></i></CButton>{' '}
                                        <CButton color="warning" onClick={onSaveDeviceData}><i className="cil-trash"></i></CButton>
                                    </CFormGroup>
                                </CRow>
                            </CForm>
                        </CCardBody>
                    </CCard>
                </CCol>
            </CRow>
            <CRow>
                <CCol sm="12">
                    <CCard>
                        <CCardHeader>
                            <CRow>
                                <CCol>
                                    <h6 id="traffic" className="card-title mb-0">КАРТА СЕРВИСОВ</h6>
                                </CCol>
                            </CRow>
                        </CCardHeader>

                        <CCardBody>
                            <CRow className="text-left services-map-table">
                            <CDataTable
                                        items={servicesMapData}
                                        fields={servicesMapFields}
                                        itemsPerPage={50}
                                        hover
                                        sorter
                                        pagination
                                        scopedSlots = {{
                                            'port':
                                            (item)=>(
                                                <td style={{left: '0px'}} className='text-center align-middle'>
                                                        {item.port}
                                                        <i style={{color: 'blue', fontWeight: 'bold', fontSize: '0.8rem', cursor: 'pointer', position: 'relative', top: '1px', left: '2px'}} className={'cil-circle'} onClick={()=>{console.log('port', item.port)}}></i>
                                                </td>

                                            ),
                                            'number':
                                            (item)=>(
                                                <td style={{left: '100px'}} className='text-center align-middle'>
                                                        {item.number}
                                                        <br/>
                                                        <i style={{color: 'blue', fontWeight: 'bold', fontSize: '1.0rem', cursor: 'pointer', position: 'relative', top: '1px'}} className={'cil-loop-circular'} onClick={()=>{console.log('number', item.number)}}></i>
                                                </td>
                                            ),
                                            'operator':
                                            (item)=>(
                                                <td style={{left: '200px'}} className='text-center align-middle'>
                                                        {item.operator}
                                                </td>
                                            ),
                                            'currency':
                                            (item)=>(
                                                <td style={{left: '300px'}} className='text-center align-middle'>
                                                        {item.currency}
                                                </td>
                                            ),
                                            'vkontakte':
                                            (item)=>(
                                                <td className={getColor(item.vkontakte) + ' text-center align-middle'}>
                                                        {item.vkontakte}
                                                </td>
                                            ),
                                            'odnoklassniki':
                                            (item)=>(
                                                <td className={getColor(item.odnoklassniki) + ' text-center align-middle'}>
                                                        {item.odnoklassniki}
                                                </td>
                                            ),
                                            'whatsapp':
                                            (item)=>(
                                                <td className={getColor(item.whatsapp) + ' text-center align-middle'}>
                                                        {item.whatsapp}
                                                </td>
                                            ),
                                            'viber':
                                            (item)=>(
                                                <td className={getColor(item.viber) + ' text-center align-middle'}>
                                                        {item.viber}
                                                </td>
                                            ),
                                            'telegram':
                                            (item)=>(
                                                <td className={getColor(item.telegram) + ' text-center align-middle'}>
                                                        {item.telegram}
                                                </td>
                                            ),
                                            'wechat':
                                            (item)=>(
                                                <td className={getColor(item.wechat) + ' text-center align-middle'}>
                                                        {item.wechat}
                                                </td>
                                            ),
                                            'google':
                                            (item)=>(
                                                <td className={getColor(item.google) + ' text-center align-middle'}>
                                                        {item.google}
                                                </td>
                                            ),
                                            'avito':
                                            (item)=>(
                                                <td className={getColor(item.avito) + ' text-center align-middle'}>
                                                        {item.avito}
                                                </td>
                                            ),


                                            'facebook':
                                            (item)=>(
                                                <td className={getColor(item.facebook) + ' text-center align-middle'}>
                                                        {item.facebook}
                                                </td>
                                            ),
                                            'twitter':
                                            (item)=>(
                                                <td className={getColor(item.twitter) + ' text-center align-middle'}>
                                                        {item.twitter}
                                                </td>
                                            ),
                                            'instagram':
                                            (item)=>(
                                                <td className={getColor(item.instagram) + ' text-center align-middle'}>
                                                        {item.instagram}
                                                </td>
                                            ),
                                            'yashas':
                                            (item)=>(
                                                <td className={getColor(item.yashas) + ' text-center align-middle'}>
                                                        {item.yashas}
                                                </td>
                                            ),
                                            'yandex':
                                            (item)=>(
                                                <td className={getColor(item.yandex) + ' text-center align-middle'}>
                                                        {item.yandex}
                                                </td>
                                            ),
                                            'tinder':
                                            (item)=>(
                                                <td className={getColor(item.tinder) + ' text-center align-middle'}>
                                                        {item.tinder}
                                                </td>
                                            ),
                                            's123':
                                            (item)=>(
                                                <td className={getColor(item.s123) + ' text-center align-middle'}>
                                                        {item.s123}
                                                </td>
                                            ),
                                            'Skaru':
                                            (item)=>(
                                                <td className={getColor(item.Skaru) + ' text-center align-middle'}>
                                                        {item.Skaru}
                                                </td>
                                            ),
                                            'addrs':
                                            (item)=>(
                                                <td className={getColor(item.addrs) + ' text-center align-middle'}>
                                                        {item.addrs}
                                                </td>
                                            ),
                                            'Airbnb':
                                            (item)=>(
                                                <td className={getColor(item.Airbnb) + ' text-center align-middle'}>
                                                        {item.Airbnb}
                                                </td>
                                            ),
                                            'Alexgro':
                                            (item)=>(
                                                <td className={getColor(item.Alexgro) + ' text-center align-middle'}>
                                                        {item.Alexgro}
                                                </td>
                                            ),
                                            'aol':
                                            (item)=>(
                                                <td className={getColor(item.aol) + ' text-center align-middle'}>
                                                        {item.aol}
                                                </td>
                                            ),
                                            'beget':
                                            (item)=>(
                                                <td className={getColor(item.beget) + ' text-center align-middle'}>
                                                        {item.beget}
                                                </td>
                                            ),
                                            'bizzard':
                                            (item)=>(
                                                <td className={getColor(item.bizzard) + ' text-center align-middle'}>
                                                        {item.bizzard}
                                                </td>
                                            ),
                                            'Bolt':
                                            (item)=>(
                                                <td className={getColor(item.Bolt) + ' text-center align-middle'}>
                                                        {item.Bolt}
                                                </td>
                                            ),
                                            'Burger_king':
                                            (item)=>(
                                                <td className={getColor(item.Burger_king) + ' text-center align-middle'}>
                                                        {item.Burger_king}
                                                </td>
                                            ),

                                            'Cdkeyscom':
                                            (item)=>(
                                                <td className={getColor(item.Cdkeyscom) + ' text-center align-middle'}>
                                                        {item.Cdkeyscom}
                                                </td>
                                            ),
                                            'Chaumarp':
                                            (item)=>(
                                                <td className={getColor(item.Chaumarp) + ' text-center align-middle'}>
                                                        {item.Chaumarp}
                                                </td>
                                            ),
                                        }}
                                        
                                />
                            </CRow>
                        </CCardBody>
                    </CCard>
                </CCol>
            </CRow>
            <CRow>
                <CCol sm="6">
                    <CCard>
                        <CCardHeader>
                            <CRow>
                                <CCol>
                                    <h6 id="traffic" className="card-title mb-0">ЗАКАЗЫ</h6>
                                </CCol>
                            </CRow>
                        </CCardHeader>

                        <CCardBody>
                            <CRow className="text-left">

                                <CCol xs="12" md="3">
                                    <CLabel htmlFor="select">Слот</CLabel>
                                    <CSelect custom name="select" id="select">

                                        <option value="0">1</option>
                                        <option value="1">Option #1</option>
                                        <option value="2">Option #2</option>
                                        <option value="3">Option #3</option>
                                    </CSelect>
                                </CCol>
                                <CCol xs="12" md="3">
                                    <CLabel htmlFor="select">Сервис</CLabel>
                                    <CSelect custom name="select" id="select">

                                        <option value="0">vk</option>
                                        <option value="1">Option #1</option>
                                        <option value="2">Option #2</option>
                                        <option value="3">Option #3</option>
                                    </CSelect>
                                </CCol>
                                <CCol xs="12" md="3">
                                    <CLabel htmlFor="select">Страна</CLabel>
                                    <CSelect custom name="select" id="select">

                                        <option value="0">ru</option>
                                        <option value="1">Option #1</option>
                                        <option value="2">Option #2</option>
                                        <option value="3">Option #3</option>
                                    </CSelect>
                                </CCol>
                            </CRow>

                            <br />
                            <CRow className="text-left orders-table">
                                <CDataTable
                                    items={ordersData}
                                    fields={ordersFields}
                                    itemsPerPage={50}
                                    hover
                                    sorter
                                    pagination
                                />
                            </CRow>
                        </CCardBody>
                    </CCard>
                </CCol>
                <CCol sm="6">
                    <CCard>
                        <CCardHeader>
                            <CRow>
                                <CCol>
                                    <h6 id="traffic" className="card-title mb-0">СМС</h6>
                                </CCol>
                            </CRow>
                        </CCardHeader>

                        <CCardBody>
                            <CRow className="text-left">
                                <CCol xs="12" md="3">
                                    <CLabel htmlFor="select">Слот</CLabel>
                                    <CSelect custom name="select" id="select">

                                        <option value="0">1</option>
                                        <option value="1">Option #1</option>
                                        <option value="2">Option #2</option>
                                        <option value="3">Option #3</option>
                                    </CSelect>
                                </CCol>
                            </CRow>
                            <br />
                            <CRow className="text-left">
                                <CDataTable
                                    items={smsData}
                                    fields={smsFields}
                                    itemsPerPage={50}
                                    hover
                                    sorter
                                    pagination
                                />
                            </CRow>
                        </CCardBody>
                    </CCard>
                </CCol>
            </CRow>
            <CRow>
                <CCol sm="9">
                    <CCard>
                        <CCardHeader>
                            <CRow>
                                <CCol>
                                    <h6 id="traffic" className="card-title mb-0">МОНИТОРИНГ ПОРТОВ</h6>
                                </CCol>
                            </CRow>
                        </CCardHeader>

                        <CCardBody>
                            <CRow className="text-left">
                                <CDataTable
                                    items={monitoringPortsData}
                                    fields={monitoringPortsFields}
                                    itemsPerPage={50}
                                    hover
                                    sorter
                                    pagination
                                />
                            </CRow>
                        </CCardBody>
                    </CCard>
                </CCol>
                <CCol sm="3">
                    <CCard>
                        <CCardHeader>
                            <CRow>
                                <CCol>
                                    <h6 id="traffic" className="card-title mb-0">НАСТРОЙКИ ПОДКЛЮЧЕНИЯ</h6>
                                </CCol>
                            </CRow>
                        </CCardHeader>

                        <CCardBody>
                            <CRow>
                                <CCol className="text-right" md="6">
                                    <CLabel>1</CLabel>
                                </CCol>
                                <CCol className="text-left" xs="12" md="6">
                                    <p className="form-control-static">100</p>
                                </CCol>
                            </CRow>

                            <CRow>
                                <CCol className="text-right" md="6">
                                    <CLabel>2</CLabel>
                                </CCol>
                                <CCol className="text-left" xs="12" md="6">
                                    <p className="form-control-static">101</p>
                                </CCol>
                            </CRow>

                            <CRow>
                                <CCol className="text-right" md="6">
                                    <CLabel>3</CLabel>
                                </CCol>
                                <CCol className="text-left" xs="12" md="6">
                                    <p className="form-control-static">102</p>
                                </CCol>
                            </CRow>

                            <CRow>
                                <CCol className="text-right" md="6">
                                    <CLabel>4</CLabel>
                                </CCol>
                                <CCol className="text-left" xs="12" md="6">
                                    <p className="form-control-static">103</p>
                                </CCol>
                            </CRow>

                            <CRow>
                                <CCol className="text-right" md="6">
                                    <CLabel>5</CLabel>
                                </CCol>
                                <CCol className="text-left" xs="12" md="6">
                                    <p className="form-control-static">104</p>
                                </CCol>
                            </CRow>
                            <CRow>
                                <CCol className="text-right" md="6">
                                    <CLabel>6</CLabel>
                                </CCol>
                                <CCol className="text-left" xs="12" md="6">
                                    <p className="form-control-static">105</p>
                                </CCol>
                            </CRow>
                            <hr />
                            <CRow className="text-left pl-3">
                                    <CFormGroup className="form-actions pl">
                                        <CButton color="primary"><i className="cil-save"></i></CButton>{' '}
                                        <CButton color="primary" onClick={onSaveDeviceData}>ИМПОРТ</CButton>
                                    </CFormGroup>
                            </CRow>
                        </CCardBody>
                    </CCard>
                </CCol>
            </CRow>
        </>
    );
};

export default CardOfDevice;
