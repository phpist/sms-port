import React from 'react';

const Toaster = React.lazy(() => import('./views/notifications/toaster/Toaster'));
const Tables = React.lazy(() => import('./views/base/tables/Tables'));

const Breadcrumbs = React.lazy(() => import('./views/base/breadcrumbs/Breadcrumbs'));
const Cards = React.lazy(() => import('./views/base/cards/Cards'));
const Carousels = React.lazy(() => import('./views/base/carousels/Carousels'));
const Collapses = React.lazy(() => import('./views/base/collapses/Collapses'));
const BasicForms = React.lazy(() => import('./views/base/forms/BasicForms'));

const Jumbotrons = React.lazy(() => import('./views/base/jumbotrons/Jumbotrons'));
const ListGroups = React.lazy(() => import('./views/base/list-groups/ListGroups'));
const Navbars = React.lazy(() => import('./views/base/navbars/Navbars'));
const Navs = React.lazy(() => import('./views/base/navs/Navs'));
const Paginations = React.lazy(() => import('./views/base/paginations/Pagnations'));
const Popovers = React.lazy(() => import('./views/base/popovers/Popovers'));
const ProgressBar = React.lazy(() => import('./views/base/progress-bar/ProgressBar'));
const Switches = React.lazy(() => import('./views/base/switches/Switches'));

const Tabs = React.lazy(() => import('./views/base/tabs/Tabs'));
const Tooltips = React.lazy(() => import('./views/base/tooltips/Tooltips'));
const BrandButtons = React.lazy(() => import('./views/buttons/brand-buttons/BrandButtons'));
const ButtonDropdowns = React.lazy(() => import('./views/buttons/button-dropdowns/ButtonDropdowns'));
const ButtonGroups = React.lazy(() => import('./views/buttons/button-groups/ButtonGroups'));
const Buttons = React.lazy(() => import('./views/buttons/buttons/Buttons'));
const Charts = React.lazy(() => import('./views/charts/Charts'));
const Dashboard = React.lazy(() => import('./views/dashboard/Dashboard'));
const CoreUIIcons = React.lazy(() => import('./views/icons/coreui-icons/CoreUIIcons'));
const Flags = React.lazy(() => import('./views/icons/flags/Flags'));
const Brands = React.lazy(() => import('./views/icons/brands/Brands'));
const Alerts = React.lazy(() => import('./views/notifications/alerts/Alerts'));
const Badges = React.lazy(() => import('./views/notifications/badges/Badges'));
const Modals = React.lazy(() => import('./views/notifications/modals/Modals'));
const Colors = React.lazy(() => import('./views/theme/colors/Colors'));
const Typography = React.lazy(() => import('./views/theme/typography/Typography'));
const Widgets = React.lazy(() => import('./views/widgets/Widgets'));
const Users = React.lazy(() => import('./views/users/Users'));
const User = React.lazy(() => import('./views/users/User'));

const ApiKeys = React.lazy(() => import('./components/ApiKeys/ApiKeys'));
const Devices = React.lazy(() => import('./components/Devices/Devices'));
const Operators = React.lazy(() => import('./components/Operators/Operators'));
const Orders = React.lazy(() => import('./components/Orders/Orders'));
const Partners = React.lazy(() => import('./components/Partners/Partners'));
const Payments = React.lazy(() => import('./components/Payments/Payments'));
const Profiles = React.lazy(() => import('./components/Profiles/Profiles'));
const Services = React.lazy(() => import('./components/Services/Services'));
const Simcards = React.lazy(() => import('./components/Simcards/Simcards'));
const SimFilters = React.lazy(() => import('./components/SimFilters/SimFilters'));
const Transactions = React.lazy(() => import('./components/Transactions/Transactions'));
const CardOfPartner = React.lazy(() => import('./components/CardOfPartner/CardOfPartner'));

const CardOfDevice = React.lazy(() => import('./components/CardOfDevice/CardOfDevice'));
const CardOfSimcard = React.lazy(() => import('./components/CardOfSimcard/CardOfSimcard'));
const CardOfProfile = React.lazy(() => import('./components/CardOfProfile/CardOfProfile'));
const Price = React.lazy(() => import('./components/Price/Price'));
const CardOfApiKey = React.lazy(() => import('./components/CardOfApiKey/CardOfApiKey'));

// const Main = React.lazy(() => import('./containers/Main'));

const capitalizeFirstLetter = (str) => {
    return str[0].toUpperCase() + str.slice(1);
};

const routes = (val, menuItems) => {
    const routesData = {
        root: {
            path: '/',
            exact: true,
            name: val && capitalizeFirstLetter(val['breadcrumb.main']),
            component: Dashboard
        },
        'menu.apikeys': {
            path: '/apikeys',
            exact: true,
            name: 'API-ключи',
            component: ApiKeys,
            '/:id': {
                path: '/apikeys/:id',
                exact: false,
                name: 'Key',
                component: CardOfApiKey
            }
        },
        'menu.devices': {
            path: '/devices',
            exact: true,
            name: val && capitalizeFirstLetter(val['breadcrumb.devices']),
            component: Devices,
            '/:id': {
                path: '/devices/:id',
                exact: false,
                name: 'Device',
                component: CardOfDevice
            }
        },
        'menu.operators': {
            path: '/operators',
            exact: false,
            name: val && capitalizeFirstLetter(val['breadcrumb.operators']),
            component: Operators
        },
        'menu.orders': {
            path: '/orders',
            exact: false,
            name: val && capitalizeFirstLetter(val['breadcrumb.orders']),
            component: Orders
        },
        'menu.partners': {
            path: '/partners',
            exact: true,
            name: val && capitalizeFirstLetter(val['breadcrumb.partners']),
            component: Partners,
            '/:id': {
                path: '/partners/:id',
                exact: true,
                name: 'Profile',
                component: CardOfPartner
            }
        },
        'menu.payments': {
            path: '/payments',
            exact: false,
            name: val && capitalizeFirstLetter(val['breadcrumb.payments']),
            component: Payments
        },
        'menu.profiles': {
            path: '/profiles',
            exact: true,
            name: val && capitalizeFirstLetter(val['breadcrumb.profiles']),
            component: Profiles,
            '/:id': {
                path: '/profiles/:id',
                name: 'Profile',
                exact: true,
                component: CardOfProfile
            },
            '/:id/:price': {
                path: '/profiles/:id/:price',
                exact: false,
                name: 'Price',
                component: Price
            }
        },
        'menu.services': {
            path: '/services',
            exact: false,
            name: val && capitalizeFirstLetter(val['breadcrumb.services']),
            component: Services
        },
        'menu.simcards': {
            path: '/simcards',
            exact: true,
            name: val && capitalizeFirstLetter(val['breadcrumb.simcards']),
            component: Simcards,
            '/:id': {
                path: '/simcards/:id',
                exact: false,
                name: 'Number',
                component: CardOfSimcard
            }
        },
        'menu.simfilters': {
            path: '/simfilters',
            exact: false,
            name: val && capitalizeFirstLetter(val['breadcrumb.simfilters']),
            component: SimFilters
        },
        'menu.transactions': {
            path: '/transactions',
            exact: false,
            name: val && capitalizeFirstLetter(val['breadcrumb.transactions']),
            component: Transactions
        }
    };

    console.log('routes', menuItems);
    const arr = [];

    console.log('menuItems', menuItems);
    // const menuItemsTransformed = [...menuItems.items];

    for (const index in menuItems) {
        console.log('eee', menuItems[index]);

        console.log('routesData[menuItems[index].name', routesData[menuItems[index].name]);
        arr.push({
            path: routesData[menuItems[index].name].path,
            name: routesData[menuItems[index].name].name,
            exact: routesData[menuItems[index].name].exact,
            component: routesData[menuItems[index].name].component
        });
    }

    for (const index in menuItems) {
        routesData[menuItems[index].name]['/:id'] &&
        arr.push({
            path: routesData[menuItems[index].name]['/:id'].path,
            name: routesData[menuItems[index].name]['/:id'].name,
            exact: routesData[menuItems[index].name]['/:id'].exact,
            component: routesData[menuItems[index].name]['/:id'].component
        });
    }

    for (const index in menuItems) {
        routesData[menuItems[index].name]['/:id/:price'] &&
        arr.push({
            path: routesData[menuItems[index].name]['/:id/:price'].path,
            name: routesData[menuItems[index].name]['/:id/:price'].name,
            exact: routesData[menuItems[index].name]['/:id/:price'].exact,
            component: routesData[menuItems[index].name]['/:id/:price'].component
        });
    }

    arr.push({
        path: routesData.root.path,
        name: routesData.root.name,
        exact: routesData.root.exact,
        component: routesData.root.component
    });

    console.log('arr', arr);

    return arr;
};

export default routes;

// const routes = (val) => {
//     return [
//         { path: '/', exact: true, name: val && capitalizeFirstLetter(val['breadcrumb.main']), component: Dashboard },
//         // { path: '/dashboard', name: 'Dashboard', component: Dashboard },
//         { path: '/theme', name: 'Theme', component: Colors, exact: true },
//         { path: '/theme/colors', name: 'Colors', component: Colors },
//         { path: '/theme/typography', name: 'Typography', component: Typography },
//         { path: '/base', name: 'Base', component: Cards, exact: true },
//         { path: '/base/breadcrumbs', name: 'Breadcrumbs', component: Breadcrumbs },
//         { path: '/base/cards', name: 'Cards', component: Cards },
//         { path: '/base/carousels', name: 'Carousel', component: Carousels },
//         { path: '/base/collapses', name: 'Collapse', component: Collapses },
//         { path: '/base/forms', name: 'Forms', component: BasicForms },
//         { path: '/base/jumbotrons', name: 'Jumbotrons', component: Jumbotrons },
//         { path: '/base/list-groups', name: 'List Groups', component: ListGroups },
//         { path: '/base/navbars', name: 'Navbars', component: Navbars },
//         { path: '/base/navs', name: 'Navs', component: Navs },
//         { path: '/base/paginations', name: 'Paginations', component: Paginations },
//         { path: '/base/popovers', name: 'Popovers', component: Popovers },
//         { path: '/base/progress-bar', name: 'Progress Bar', component: ProgressBar },
//         { path: '/base/switches', name: 'Switches', component: Switches },
//         { path: '/base/tables', name: 'Tables', component: Tables },
//         { path: '/base/tabs', name: 'Tabs', component: Tabs },
//         { path: '/base/tooltips', name: 'Tooltips', component: Tooltips },
//         { path: '/buttons', name: 'Buttons', component: Buttons, exact: true },
//         { path: '/buttons/buttons', name: 'Buttons', component: Buttons },
//         { path: '/buttons/button-dropdowns', name: 'Dropdowns', component: ButtonDropdowns },
//         { path: '/buttons/button-groups', name: 'Button Groups', component: ButtonGroups },
//         { path: '/buttons/brand-buttons', name: 'Brand Buttons', component: BrandButtons },
//         { path: '/charts', name: 'Charts', component: Charts },
//         { path: '/icons', exact: true, name: 'Icons', component: CoreUIIcons },
//         { path: '/icons/coreui-icons', name: 'CoreUI Icons', component: CoreUIIcons },
//         { path: '/icons/flags', name: 'Flags', component: Flags },
//         { path: '/icons/brands', name: 'Brands', component: Brands },
//         { path: '/notifications', name: 'Notifications', component: Alerts, exact: true },
//         { path: '/notifications/alerts', name: 'Alerts', component: Alerts },
//         { path: '/notifications/badges', name: 'Badges', component: Badges },
//         { path: '/notifications/modals', name: 'Modals', component: Modals },
//         { path: '/notifications/toaster', name: 'Toaster', component: Toaster },
//         { path: '/widgets', name: 'Widgets', component: Widgets },
//         { path: '/users', exact: true, name: 'Users', component: Users },
//         { path: '/users/:id', exact: true, name: 'User Details', component: User },

//         { path: '/apikeys', exact: true, name: 'API-ключи', component: ApiKeys },
//         { path: '/devices', exact: true, name: val && capitalizeFirstLetter(val['breadcrumb.devices']), component: Devices },
//         { path: '/operators', name: val && capitalizeFirstLetter(val['breadcrumb.operators']), component: Operators },
//         { path: '/orders', name: val && capitalizeFirstLetter(val['breadcrumb.orders']), component: Orders },
//         { path: '/partners', exact: true, name: val && capitalizeFirstLetter(val['breadcrumb.partners']), component: Partners },
//         { path: '/payments', name: val && capitalizeFirstLetter(val['breadcrumb.payments']), component: Payments },
//         { path: '/profiles', exact: true, name: val && capitalizeFirstLetter(val['breadcrumb.profiles']), component: Profiles },
//         { path: '/services', name: val && capitalizeFirstLetter(val['breadcrumb.services']), component: Services },
//         { path: '/simcards', exact: true, name: val && capitalizeFirstLetter(val['breadcrumb.simcards']), component: Simcards },
//         { path: '/simfilters', name: val && capitalizeFirstLetter(val['breadcrumb.simfilters']), component: SimFilters },
//         { path: '/transactions', name: val && capitalizeFirstLetter(val['breadcrumb.transactions']), component: Transactions },
//         {
//             path: '/partners/:id',
//             exact: true,
//             name: 'Profile',
//             component: CardOfPartner
//         },
//         {
//             path: '/devices/:id',
//             name: 'Device',
//             component: CardOfDevice
//         },
//         {
//             path: '/simcards/:id',
//             name: 'Number',
//             component: CardOfSimcard
//         },
//         {
//             path: '/profiles/:id',
//             name: 'Profile',
//             exact: true,
//             component: CardOfProfile
//         },
//         {
//             path: '/profiles/:id/:price',
//             name: 'Price',
//             component: Price
//         },
//         {
//             path: '/apikeys/:id',
//             name: 'Key',
//             component: CardOfApiKey
//         }

//     ];
// };
