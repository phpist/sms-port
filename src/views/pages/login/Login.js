import React, { Suspense, useState, useEffect, useRef } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { useHistory, Link } from 'react-router-dom';

import {
    CButton,
    CCard,
    CCardBody,
    CCardGroup,
    CCol,
    CContainer,
    CForm,
    CInput,
    CInputGroup,
    CInputGroupPrepend,
    CInputGroupText,
    CRow
} from '@coreui/react';
import CIcon from '@coreui/icons-react';

import { login } from '../../../store/actions/authActions';

const Login = () => {
    const dispatch = useDispatch();
    const history = useHistory();

    const authorized = useSelector(state => state.authorization.authorized);

    if (authorized) {
        history.push('/partners');
    }

    const [loginData, setLoginData] = useState({
        username: '',
        password: ''
    });

    const onLogin = (event) => {
        event.preventDefault();
        console.log('onLogin');

        dispatch(login(loginData));

        if (authorized) {
            history.push('/partners');
        }
    };

    const handleInputChange = ({ target: { name, value } }) => {
        setLoginData({ ...loginData, [name]: value });
    };

    return (
        <div className="c-app c-default-layout flex-row align-items-center">
            <CContainer>
                <CRow className="justify-content-center">
                    <CCol md="5">
                        <CCardGroup>
                            <CCard className="p-4">
                                <CCardBody>
                                    <CForm onSubmit={onLogin}>
                                        <h1>Login</h1>
                                        <p className="text-muted">Sign In to your account</p>
                                        <CInputGroup className="mb-3">
                                            <CInputGroupPrepend>
                                                <CInputGroupText>
                                                    <CIcon name="cil-user" />
                                                </CInputGroupText>
                                            </CInputGroupPrepend>
                                            <CInput onChange={handleInputChange} value={loginData.username} name="username" type="email" placeholder="Username" autoComplete="username" required/>
                                        </CInputGroup>
                                        <CInputGroup className="mb-4">
                                            <CInputGroupPrepend>
                                                <CInputGroupText>
                                                    <CIcon name="cil-lock-locked" />
                                                </CInputGroupText>
                                            </CInputGroupPrepend>
                                            <CInput onChange={handleInputChange} value={loginData.password} name="password" type="password" placeholder="Password" autoComplete="current-password" required/>
                                        </CInputGroup>
                                        <CRow>
                                            <CCol>
                                                <CButton type="submit" color="primary" className="px-4">Login</CButton>
                                            </CCol>
                                            <CCol>
                                                <CButton to="/" color="secondary" className="px-4">Cancel</CButton>
                                            </CCol>
                                            {/* <CCol xs="6" className="text-right">
                                                <CButton color="link" className="px-0">Forgot password?</CButton>
                                            </CCol> */}
                                        </CRow>
                                    </CForm>
                                </CCardBody>
                            </CCard>
                            {/* <CCard className="text-white bg-primary py-5 d-md-down-none" style={{ width: '44%' }}>
                                <CCardBody className="text-center">
                                    <div>
                                        <h2>Sign up</h2>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut
                      labore et dolore magna aliqua.</p>
                                        <Link to="/register">
                                            <CButton color="primary" className="mt-3" active tabIndex={-1}>Register Now!</CButton>
                                        </Link>
                                    </div>
                                </CCardBody>
                            </CCard> */}
                        </CCardGroup>
                    </CCol>
                </CRow>
            </CContainer>
        </div>
    );
};

export default Login;
