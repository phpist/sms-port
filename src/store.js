import { createStore } from 'redux';

const initialState = {
    sidebarShow: 'responsive'
};

const rootReducer = (state = initialState, { type, ...rest }) => {
    console.log('rest', rest);
    switch (type) {
    case 'set':
        return { ...state, ...rest };
    default:
        return state;
    }
};

const store = createStore(rootReducer);
export default store;
