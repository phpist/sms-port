/* eslint-disable */
import React, { Component } from 'react';
import { HashRouter, Route, Switch, Redirect } from 'react-router-dom';
import { useSelector } from 'react-redux';

import ReactNotification from 'react-notifications-component';
import 'react-notifications-component/dist/theme.css';

import './scss/style.scss';
// import 'animate.css';

const loading = (
    <div className="pt-3 text-center">
        <div className="sk-spinner sk-spinner-pulse"></div>
    </div>
);

// Containers
const TheLayout = React.lazy(() => import('./containers/TheLayout'));

const Main = React.lazy(() => import('./containers/Main'));

// Pages
const Login = React.lazy(() => import('./views/pages/login/Login'));
const Register = React.lazy(() => import('./views/pages/register/Register'));
const Page404 = React.lazy(() => import('./views/pages/page404/Page404'));
const Page500 = React.lazy(() => import('./views/pages/page500/Page500'));

// const isLogged = !!sessionStorage.getItem('token');


const App = () => {
    const authorized = useSelector(state => state.authorization.authorized);
    return (
        <>
            <ReactNotification />
            <HashRouter>
                <React.Suspense fallback={loading}>
                    <Switch>
                        <Route exact path="/" name="Main Page" render={props => <Main {...props}/>} />
                        <Route exact path="/login" name="Login Page" render={props => <Login {...props}/>} />
                        <Route exact path="/register" name="Register Page" render={props => <Register {...props}/>} />
                        <Route exact path="/404" name="Page 404" render={props => <Page404 {...props}/>} />
                        <Route exact path="/500" name="Page 500" render={props => <Page500 {...props}/>} />
                        <Route path="/" name="Home" render={props => (authorized === true ? <TheLayout {...props}/> : <Redirect to="/" />)} />
                    </Switch>
                </React.Suspense>
            </HashRouter>
        </>
    );
}

export default App;
